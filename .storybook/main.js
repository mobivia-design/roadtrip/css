module.exports = {
  "stories": ["../stories/**/*.stories.mdx", "../stories/**/*.stories.@(js|jsx|ts|tsx)"],
  "addons": ["@storybook/addon-a11y", {
    name: '@storybook/addon-essentials',
    options: {
      measure: false,
      outline: false,
      controls: false,
      actions: false
    }
  }, "storybook-addon-themes", "@geometricpanda/storybook-addon-badges", "@storybook/addon-mdx-gfm"],
  staticDirs: ['../dist', 'favicon'],
  framework: {
    name: "@storybook/html-webpack5",
    options: {}
  },
  docs: {
    autodocs: true
  }
};