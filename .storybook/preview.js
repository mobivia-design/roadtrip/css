import React from 'react';
import { Title, Subtitle, Description, Primary, Controls, Stories } from '@storybook/blocks';

const SOURCE_REGEX = /^\(\) => `((.|\n)*)`$/;

const DocWrapper = ({ children }) => {
  return (
    <body className="norauto-theme">
      {children}
    </body>
  );
};

export const parameters = {
  themes: [
    { name: 'Mobivia', class: 'mobivia-theme', color: '#B40063' },
    { name: 'Norauto', class: 'norauto-theme', color: '#002B6F', default: true },
    { name: 'Auto5', class: 'auto5-theme', color: '#E00008' },
    { name: 'ATU', class: 'atu-theme', color: '#bb1e10' },
    { name: 'Midas', class: 'midas-theme', color: '#FFD300' }
  ],
  docs: {
    page: () => (
      <>
      <DocWrapper>
        <Stories />
      </DocWrapper>
      </>
    ),
    source: { state: 'open' },
    transformSource: (src, storyContext) => {
      const match = SOURCE_REGEX.exec(src);
      return match ? match[1] : src;
    },
  },
  options: {
    storySort: (a, b) => a.kind === b.kind ? 0 : a.id.localeCompare(b.id, undefined, { numeric: true }),
  },
  badges: ['stable'],
  backgrounds: {
    default: 'white',
    values: [
      { 
          name: 'white', 
          value: '#ffffff'
      },
      { 
          name: 'grey', 
          value: '#f4f5fa' 
      },
    ],
    grid: {
      disable: true
    }
  },
  viewport: { 
    viewports: {
      mobile: {
        name: 'Mobile',
        styles: {
          width: '360px',
          height: '640px',
        },
      },
      tablet: {
        name: 'Tablet',
        styles: {
          width: '768px',
          height: '1024px',
        },
      },
      desktop: {
        name: 'Desktop',
        styles: {
          width: '1600px',
          height: '864px',
        },
      },
    },
    defaultViewport: 'mobile',
  },
}
