module.exports = {
  plugins: [
    require('postcss-import')({
      plugins: [
        require('stylelint'),
        require('postcss-reporter')({ 
          clearReportedMessages: true,
        })
      ]
    }),
    require('autoprefixer'),
    require('../../banner'),
    require('cssnano')({
      preset: 'default',
  })
  ]
}
