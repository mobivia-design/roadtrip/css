const pkg = require('../../package.json');
const year = new Date().getFullYear();

module.exports = () => {
  return {
    postcssPlugin: 'banner',
    Once(css) {
        css.prepend(`/*!
 * Roadtrip CSS ${pkg.version} (https://design.mobivia.com/)
 * Copyright 2020-${year} Mobivia
 * Licensed under Apache-2.0 (https://gitlab.com/mobivia-design/roadtrip/css/-/blob/master/LICENSE)
 */
`);
    },
  };
};
module.exports.postcss = true;