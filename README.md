<div align="center">
  <img width="124" height="124" align="center" src="illustration/roadtrip-css.png">

  <h1 align="center">Roadtrip CSS</h1>

  <p align="center">CSS framework for Roadtrip Design System</p>
</div>
​

## :blue_book: Documentation
​
Our documentation site lives at [design.mobivia.com](https://design.mobivia.com/03cde1f3f/p/62224e-using-css-framework).
You'll be able to find detailed documentation on getting started, all of the components, our themes, our guidelines, and more.
​

## :package: Installation


### With NPM
​
Before moving further, please make sure you have [Node.js](https://nodejs.org) installed on your machine. You can install the latest version through their website.

If you’re planning on using our CSS framework in a project that doesn’t yet use [Node Package Manager](https://www.npmjs.com/), you’ll have to first create a [package.json](https://docs.npmjs.com/cli/v7/configuring-npm/package-json) file. To do this, run `npm init` and follow the steps provided.

Run in your project or website root directory (where package.json lives):
​
```bash
npm install @roadtrip/css --save
```

In the main file of you app,
you can now import the CSS to load it in you app.

```javascript
import "@roadtrip/css/dist/roadtrip.min.css";
```


### with CDN
​
CSS can also be served from a CDN by adding the code in the `head` of you html file. Just specify a version in the URL like the following.

```html
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@roadtrip/css@2.29.0/dist/roadtrip.min.css" />
```
​

## :rocket: Usage

When the framework is loaded, you can finally use the prebuild classes.
​
```html
<button class="btn btn-secondary">Click me!</button>
```
​
You can find the [list of all our components](https://design.mobivia.com/03cde1f3f/p/97f8bd-overview) with their markup already available in our documentation.

## :computer: Contributing
Please take a look at our [contributing](CONTRIBUTING.md) guidelines if you're interested in helping!
​
## :newspaper: Changelog
Find all the latest changes in our [changelog](CHANGELOG.md).

## :page_with_curl: License
​
Copyright © 2021 [Mobivia](https://www.mobivia.com/).

Distributed under [Apache-2.0](LICENSE) license.