# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [4.7.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.7.0...4.7.1) (2025-01-24)

### Bug Fixes

* **variables:** fix variable midas ([f731bbd](https://gitlab.com/mobivia-design/roadtrip/css/commit/f731bbd597ac297c95b3edcdf54777cc9fb0f32b))
* **variables:** refactor color midas header ([bcaaceb](https://gitlab.com/mobivia-design/roadtrip/css/commit/bcaaceb573799e47c395d6feda1f4a6ee02e264b))
* **drawer:** add background on footer ([49d630c](https://gitlab.com/mobivia-design/roadtrip/css/commit/49d630c31eb6c76fc2e0bc1c7f85644ac15da621))


## [4.7.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.6.0...4.7.0) (2024-12-24)

### Features

* **utilities:** add media queries flex and margin ([011dafc](https://gitlab.com/mobivia-design/roadtrip/css/commit/011dafc346fc3e3d87de2fc95f89bfccf9b128b9))


## [4.6.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.5.0...4.6.0) (2024-12-19)

### Features

* **button:** add variant destructive ([e06b646](https://gitlab.com/mobivia-design/roadtrip/css/commit/e06b64654600e5aebeec811f7b79bc5812d80f50))


### Bug Fixes

* **button:** update variable css ghost button ([4175905](https://gitlab.com/mobivia-design/roadtrip/css/commit/4175905a2ece64e6ca7cc3d6c6e6ebab22064579))
* **tabs:** add disabled ([ed1a875](https://gitlab.com/mobivia-design/roadtrip/css/commit/ed1a8756a4f06550e9cdeaf259f997d0693c19d0))
* **tabs:** add border for button ([6bec2bb](https://gitlab.com/mobivia-design/roadtrip/css/commit/6bec2bbc9f0a406730925b58875fae3b052a0da4))
* **drawer:** add padding media querie min 768 ([fc2e18d](https://gitlab.com/mobivia-design/roadtrip/css/commit/fc2e18d3288b0fd7595306acea85afd8fb9777ee))


## [4.5.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.4.0...4.5.0) (2024-12-05)

### Features

* **tabs:** add disabled ([ed1a875](https://gitlab.com/mobivia-design/roadtrip/css/commit/ed1a8756a4f06550e9cdeaf259f997d0693c19d0))

### Bug Fixes

* **token:** token grey by default ([f95dd6a](https://gitlab.com/mobivia-design/roadtrip/css/commit/f95dd6ace16643a0618af646fbf13e2eda419440))

### Refactor

* **accessibility:** optimize focus ([f95dd6a](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/ecc79cee98e30909cec7ae148ed1d416b21d1a3e))


## [4.4.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.3.0...4.4.0) (2024-11-22)

### Features

* **checkbox-radio:** add option secondary label ([da50f86](https://gitlab.com/mobivia-design/roadtrip/css/commit/da50f86340c801bab7922c51e7783c03f50a89f6))
* **progress-indicator-horizontal:** add in progress ([651bf0e](https://gitlab.com/mobivia-design/roadtrip/css/commit/651bf0e1226811e55194e602797c0f7e61019248))


### Bug Fixes

* **button:** fix color outline disabled ([5fab64d](https://gitlab.com/mobivia-design/roadtrip/css/commit/5fab64d6071914f91130f449a9580b4139c750a2))


## [4.3.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.2.2...4.3.0) (2024-11-05)

### Features

* **badge:** add variant inverse ([d04db3c](https://gitlab.com/mobivia-design/roadtrip/css/commit/d04db3cd9a5a85af9fc98f1aa85cfa047b9d585d))
* **rating:** add extra-small ([37ba9bf](https://gitlab.com/mobivia-design/roadtrip/css/commit/37ba9bf74b4f68fcdb118b97d8fa235c70d921db))
* **badge:** add variant inverse ([d04db3c](https://gitlab.com/mobivia-design/roadtrip/css/commit/d04db3cd9a5a85af9fc98f1aa85cfa047b9d585d))


### Refactor

* **accessibility:**  update token outline ([aafef26](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/aafef26cd6bbaf61ba9e61100b6dc6cc0e3e60ee))


### [4.2.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.2.1...4.2.2) (2024-10-30)

### Bug Fixes

* **accessibility:** update token on-surface-weak, on-surface-extra-weak, outline and outline-weak ([6bd11c1](https://gitlab.com/mobivia-design/roadtrip/css/commit/6bd11c1cf4d5ded65fa12e5602b6858b4a8bfc8b))


### [4.2.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.2.0...4.2.1) (2024-10-17)

### Bug Fixes

* **accessibility:** update token and focus ([5a92b5f](https://gitlab.com/mobivia-design/roadtrip/css/commit/5a92b5f21a1794c6c5372b9480c1e234a04cfaeb))
* **button:** remove bg outline disabled ([6fe56fc](https://gitlab.com/mobivia-design/roadtrip/css/commit/6fe56fcb454d3c0d9190bf2008b2269ad0d6cf64))


## [4.2.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.1.0...4.2.0) (2024-09-05)

### Features

* **range:** add prop disabled ([ec0fedf](https://gitlab.com/mobivia-design/roadtrip/css/commit/ec0fedf1062176b8f00e13fdf5107b60de988f42))


### Refactor

* **fab:** update color fab button midas theme ([b9e5606](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/b9e56064ff2da3fce0be0514a264bf42492b0c49))


## [4.1.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.0.2...4.1.0) (2024-08-13)

### Features

* **drawer:** add option footer ([c3dee17](https://gitlab.com/mobivia-design/roadtrip/css/commit/c3dee172dd856b52abe0eccc24d288acb7ca2904))


### [4.0.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.0.1...4.0.2) (2024-08-01)

### Refactor

* **modal:** update padding bottom
 ([c72639c](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/c72639c568cb41b8ba1a9e8c527f7bab8380c8f4))
* **accordion:** update margin light
 ([73c9f49](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/73c9f49abe70c1b14fc8fc0f54fa91d2aff913b3))


### [4.0.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/4.0.0...4.0.1) (2024-07-26)

### Bug Fixes

* **docs:** add norauto-theme by default ([324a116](https://gitlab.com/mobivia-design/roadtrip/css/commit/324a116b6b6f409fe43da1874086fc7a699c4fb6))
* **accordion:** remove bg is-light ([ece6acd](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/ece6acd9a9dfdce05d188d0732fc3cf6d6f865b6))


## [4.0.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.15.1...4.0.0) (2024-07-18)

### Features

* **dist:** split css and add theme-norauto ([26263a9](https://gitlab.com/mobivia-design/roadtrip/css/commit/26263a9d1032faca02f68f20a8eb30387e676c61))
* **dist:**  roadtrip.min.css ([dca0880](https://gitlab.com/mobivia-design/roadtrip/css/commit/dca088002240a56cc1d6439b1726ae713a291f28))
* **dist:**  files dist ([dd9b70f](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/dd9b70f15d6177c7d1f4af301126fbccb52db63d))
* **rating:** add class rating-star-active ([0221f46](https://gitlab.com/mobivia-design/roadtrip/css/commit/0221f46d30cd7e37398cc1e897f0b8df0d709f12))


### [3.15.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.15.0...3.15.1) (2024-06-26)

### Refactor

* **breadcrumb:** color last item ([5be2ac3](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/5be2ac3d7b690cd6422c7f184659a873775f8c28))

### Bug Fixes

* **alert:** update token link ([34e9a0a](https://gitlab.com/mobivia-design/roadtrip/css/commit/34e9a0a8a49d284af4d6a36eaa8660215048f54c))


## [3.15.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.14.3...3.15.0) (2024-06-13)

### Features

* **brand:** add bythjul ([022d109](https://gitlab.com/mobivia-design/roadtrip/css/commit/022d109a4768a430c342708b38eb5368109d2933))
* **brand:** add skruvat brand ([c88f31d](https://gitlab.com/mobivia-design/roadtrip/css/commit/c88f31de0775c679227465ea28bc798a2e349faa))


### Refactor

* **modal:** update padding
 ([25cc231](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/25cc231c32140bf381da4e16d6a3ce3166cdccbc))


### [3.14.3](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.14.2...3.14.3) (2024-06-06)

### Refactor

* **carousel:** token accessibility bullets ([090e703](https://gitlab.com/mobivia-design/roadtrip/css/commit/090e703428a77dbfa82ceea4baa88280d0e0f1ab))
* **items-per-page:** refactor accessibility([eaba2ec](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/eaba2ec1b59e9ccefcd765ebe0903f8df662e932))


### [3.14.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.14.1...3.14.2) (2024-03-26)

### Refactor 

* **carousel:** update img url ([51fd627](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/51fd6271eb17022b66565839f7c0cbba21a5db3f))
* **token:** change token auto5 fab-button ([a9fff9c](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/a9fff9c62e695387c9df95ba5306f2e46dab827f))


### [3.14.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.14.0...3.14.1) (2024-02-20)

### Bug Fixes

* **variables:** fix token blur ([c8b4f90](https://gitlab.com/mobivia-design/roadtrip/css/commit/c8b4f9009746894f8a3c2ba4852369ca87dca54c))

## [3.14.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.13.1...3.14.0) (2024-02-16)

### Features

* **content-card:** add new component ([86507b3](https://gitlab.com/mobivia-design/roadtrip/css/commit/86507b3c6defac840de618e7d33e5dbc49f9a978))
* **token:** add new token fab-button ([e2ec6a0](https://gitlab.com/mobivia-design/roadtrip/css/commit/e2ec6a0336cfcff9ae31f3f527eb38313b54c3bd))

### Bug Fixes

* **floating-button:** icon color token ([fc71603](https://gitlab.com/mobivia-design/roadtrip/css/commit/fc716039d5124d68c554ca79d8b79a2cce183d60))
* **dropdown:** position absolute ([b19a136](https://gitlab.com/mobivia-design/roadtrip/css/commit/b19a136c5a8eff27a01fe4c6de701c729bacb832))

### Refactor 

* **refactor:** utilities add class -12 spacing and color tokens ([34b0e2c](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/34b0e2cad5efc13c31e34e6931f0472095f7ca20))


### [3.13.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.13.0...3.13.1) (2024-02-02)

### Bug Fixes

* **button:** hover ([42c62d2](https://gitlab.com/mobivia-design/roadtrip/css/commit/42c62d254cf2a76d832060a0ed9157e488a8d9af))

## [3.13.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.12.1...3.13.0) (2024-01-17)

### Features

* **card:** add class elevation ([a4eee32](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/a4eee32431a95e8c640dde1c18293d92dd922f9d))
* **counter:** add readonly ([f1ab2c3](https://gitlab.com/mobivia-design/roadtrip/css/commit/f1ab2c357d73e7186db3da5e92b9ab280273f89a))

### Bug Fixes

* **spacing:** fix spacing ([e7d3793](https://gitlab.com/mobivia-design/roadtrip/css/commit/e7d37939654ae62a69858ab5f63838a444bfe772))
* **card:** optim padding ([4e6d16b](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/4e6d16be38ec1a7e6c1824e68f5725c782419d7b))
* **fab:** fix display position center ([e4ef473](https://gitlab.com/mobivia-design/roadtrip/css/commit/e4ef4736a54187aea26592d8d80036a1fc53e822))


### [3.12.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.12.0...3.12.1) (2023-12-22)

### Bug Fixes

* **button:** fix outline disabled ([1f2770c](https://gitlab.com/mobivia-design/roadtrip/css/commit/1f2770c020f00d05521e084d53fcd327ba04e1d2))
* **token:** tag-exclusivity on yellow-gold-50 ([ccf1a23](https://gitlab.com/mobivia-design/roadtrip/css/commit/ccf1a23f43ad4c44a6965b4a921904876b80feed))


## [3.12.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.11.1...3.12.0) (2023-12-01)

### Features

* **token:** add token blur ([45fac22](https://gitlab.com/mobivia-design/roadtrip/css/commit/45fac220cb9980b39fa61fb386aeafa4222f00dd))
* **rating:** add option readOnly / fix color ([f43a9c6](https://gitlab.com/mobivia-design/roadtrip/css/commit/f43a9c601f7439031514063608c01118503df66b))


### [3.11.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.11.0...3.11.1) (2023-11-20)

### Bug Fixes

* **select:** optim css ([dc94823](https://gitlab.com/mobivia-design/roadtrip/css/commit/dc94823ef00fa16be2843ee938b5609c24f3b33f))


## [3.11.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.10.0...3.11.0) (2023-11-10)

### Features

* **select:** add option md ([fb5e316](https://gitlab.com/mobivia-design/roadtrip/css/commit/fb5e316fbd227bffe2b456844252cbef4a24182f))


### Bug Fixes

* **drawer:** add bold tille ([da48acb](https://gitlab.com/mobivia-design/roadtrip/css/commit/da48acb8efd112e39c85f4c7e376152cc8d6167d))


## [3.10.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.9.2...3.10.0) (2023-10-31)

### Features

* **token:** add token label extra small ([2ebb972](https://gitlab.com/mobivia-design/roadtrip/css/commit/2ebb9729b050b5542d63685ff4100c6476e346b3))

### Bug Fixes
* **button:** add storie only icon ([346785b](https://gitlab.com/mobivia-design/roadtrip/css/commit/346785b04da395f80a9778c982cbc0daaee55da2))


### [3.9.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.9.1...3.9.2) (2023-10-17)

### Bug Fixes

* **auto5:** refacto token header badge ([7039a6e](https://gitlab.com/mobivia-design/roadtrip/css/commit/7039a6efb2638b3385b7abc3762432d39c8657eb))
* **banner:** refacto token ([19fa01f](https://gitlab.com/mobivia-design/roadtrip/css/commit/19fa01f0df5b8b53aae063cab9c371dfbae987dd))


## [3.9.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.9.0...3.9.1) (2023-10-10)

### Bug Fixes

* **tokens:** outline weak ([f9cea08](https://gitlab.com/mobivia-design/roadtrip/css/commit/f9cea086384d7caa124b4a433dfc557c642a702e))
* **button:** fix disabled ([f56b3c7](https://gitlab.com/mobivia-design/roadtrip/css/commit/f56b3c72e4aa8857eed6bd065fbe761be47555b8))

## [3.9.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.8.0...3.9.0) (2023-09-01)

### Features

* **flap:** add filled ([ab5e8c0](https://gitlab.com/mobivia-design/roadtrip/css/commit/ab5e8c0b2081886bec3dfafa10cc8744cafdebf1))
* **alert:** add option button ([b097274](https://gitlab.com/mobivia-design/roadtrip/css/commit/b0972748660c072500a0eb64f82c5e0ef17b8f94))

### Bug Fixes

* **range:** update icon solid ([467c19b](https://gitlab.com/mobivia-design/roadtrip/css/commit/467c19b0de36cbd7ff89d59e0e5746a064480d3f))

## [3.8.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.7.0...3.8.0) (2023-08-18)

### Features

* **rating:** add rating without reviews ([ab0f708](https://gitlab.com/mobivia-design/roadtrip/css/commit/ab0f7084c226d998536de64675aa8c67a783858b))
* **tokens:** add spacing tokens ([b353af7](https://gitlab.com/mobivia-design/roadtrip/css/commit/b353af7142e8480ac10907d83c63b8cdc04e7679))

### Refactor

* **breadcrumb:** replace arrow by slash ([22e5e0e](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/22e5e0eb07e3cb695bf1fd648ebf5a7bf42f7040))


## [3.7.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.6.0...3.7.0) (2023-07-07)

### Features

* **progress:** add color rating ([5a2c22a](https://gitlab.com/mobivia-design/roadtrip/css/commit/5a2c22a47d155ddfdf40f953951a036f584fa0cf))
* **tag:** add contrast tag ([5add9b7](https://gitlab.com/mobivia-design/roadtrip/css/commit/5add9b70c686a6fb1664d6dacf6fb19c72670b40))
* **textarea:** add option noresize ([084d01c](https://gitlab.com/mobivia-design/roadtrip/css/commit/084d01cdba11b23b686582f3d775f98c8ab8b0d5))

### Bug Fixes

* **tag:** fix color contrast ([8007aaa](https://gitlab.com/mobivia-design/roadtrip/css/commit/8007aaa938fe78f43e39716acfca00c8e5f1413e))

### Refactor

* **global-nav:** update size label ([74665c5](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/74665c52a17f27c0a39d904427d5eaa69cdf592f))

## [3.6.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.5.0...3.6.0) (2023-06-08)

### Features

* **tag:** add new component ([9087498](https://gitlab.com/mobivia-design/roadtrip/css/commit/908749854d2bf452c752ebcf932f2aec64f72733))

### Bug Fixes

* **toast:** add pointer-event ([68403d3](https://gitlab.com/mobivia-design/roadtrip/css/commit/68403d39df39a7fccbf3a7e237d700c0e1b3297d))
* **button:** fix color secondary button ([efd7f80](https://gitlab.com/mobivia-design/roadtrip/css/commit/efd7f80ee28cdb0fcbef2d04c4203a5e9a052c1b))

### Refactor

* **progress-indicator-vertical:** rename stepper-vertical by progress-indicator-vertical ([4784804](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/4784804828c30f9a8837316136b9e7c4c145e1ee))
* **progress-indicator-vertical:** fix substep ([78e0b8d](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/78e0b8d981ada6308b5b112096ae7bae51d202ca))


## [3.5.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.4.0...3.5.0) (2023-06-01)

### Features

* **tokens:** add tokens violet ([b5627df](https://gitlab.com/mobivia-design/roadtrip/css/commit/b5627dfe0efacbc25a28e8b581541153b0c36002))
* **tokens:** add neutral grey ([69a109b](https://gitlab.com/mobivia-design/roadtrip/css/commit/69a109bd04b4183e153c38462e0ec879ae95bd1b))
* **tokens:** add neutral grey ([d6cf7ab](https://gitlab.com/mobivia-design/roadtrip/css/commit/d6cf7ab4db66781c2ece922a279f4eee5ed859f7))

### Bug Fixes

* **title:** remove color title ([49d252c](https://gitlab.com/mobivia-design/roadtrip/css/commit/49d252cefba225cdac120387b4e1bb5a250dba4a))

### Refactor

* **progress-indicator-horizontal:** color ([8137885](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/8137885a90f4e4bde6fd243fc004a11dccce6031))
* **progress-indicator-horizontal:** size ([a04676e](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/a04676e890a4de18963483ff4a135a63c5b99747))


## [3.4.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.3.0...3.4.0) (2023-05-12)

### Features

* **button-floating:** add new component ([7a89014](https://gitlab.com/mobivia-design/roadtrip/css/commit/7a890141f6ac3dd9505bb9d941c72ef3fe4b08ff))

### Bug Fixes

* **button-floating:** remove zindex ([330e067](https://gitlab.com/mobivia-design/roadtrip/css/commit/330e06784cffaef22533914be0731aec46d8a38f))

## [3.3.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.2.1...3.3.0) (2023-04-24)

### Features

* **progress-tracker:** add new component ([ec7dc2b](https://gitlab.com/mobivia-design/roadtrip/css/commit/ec7dc2bd8c26c995398397e69c59e6421a7aa8a5))
* **progress-tracker:** optim css ([89b863d](https://gitlab.com/mobivia-design/roadtrip/css/commit/89b863d41c8416745be1aa9e898007b38e9eecba))
* **tokens:** add color header disabled ([7132e0b](https://gitlab.com/mobivia-design/roadtrip/css/commit/7132e0b9e06ad13f4c8b8fe3faed164a87bf8ccd))

### Refactor

* **stepper:** progress-indicator-horizontal ([b1c1dd8](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/b1c1dd8e1b948be119d2c958097cf0502ec75984))


### Bug Fixes

* **tokens:** atu midas color header disabled ([db2e679](https://gitlab.com/mobivia-design/roadtrip/css/commit/db2e679cc06069aa8e17970b2738597902c191b1))
* **tokens:** color on header badge mobiva ([e0eec10](https://gitlab.com/mobivia-design/roadtrip/css/commit/e0eec10914dea8c067a2330c57c432bacb802f7e))


### [3.2.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.2.0...3.2.1) (2023-04-13)

### Bug Fixes

* **alert:** color outline warning ([d6f8c51](https://gitlab.com/mobivia-design/roadtrip/css/commit/d6f8c51ea90c637cdf372c29353e18dde55932fd))

### Refactor

* **global-navbar:** update token ([bdd025b](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/bdd025b4a8b2898ba2f545df806e081eedb121a8))


## [3.2.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.1.2...3.2.0) (2023-04-04)

### Features

* **tokens:** add decorative color tokens ([e566f90](https://gitlab.com/mobivia-design/roadtrip/css/commit/e566f9048bac7fe1b8e3c9ebbec98bfe9e3a4057))
* **tokens:** add tag - banner- rating tokens ([9b1f4a5](https://gitlab.com/mobivia-design/roadtrip/css/commit/9b1f4a5ffc7ceade4cf17346b717f0c6b5558bb5))
* **dropdown:** add medium version ([d3bb75c](https://gitlab.com/mobivia-design/roadtrip/css/commit/d3bb75c033c70bfe054ea5acfa4e338fa2f3a567))


### Refactor

* **plate-number:** bg- cursor readonly ([efb1e63](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/efb1e63811799578c304ca004424840a85a84287))
* **tokens:** apply new tokens ([ad0ed02](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/ad0ed02cf065bb1e7d337a3b68f8b0ae995be16a))


### [3.1.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.1.1...3.1.2) (2023-03-24)

### Bug Fixes

* **utilities:** add stories ([65239b5](https://gitlab.com/mobivia-design/roadtrip/css/commit/65239b56348b1d8177ae1976d93043997593a73c))

### [3.1.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.0.1...3.1.1) (2023-03-23)

### Features

* **utilities:** add new class ([8c47e5b](https://gitlab.com/mobivia-design/roadtrip/css/commit/8c47e5b81cf48a0094602527f11a5ca5d4fb8e21))

### Refactor

* **toast:** toast less invasive ([65aa89b](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/65aa89b915796de7adb8ce58b4b90853aed0cc36))
* **progress:** add light version ([2d07c40](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/2d07c403801c54ab5177f4523f555ce61bba3365))
* **toast:** add progressbar ([411451f](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/411451f1064d944658ac190e1257c1cf9c7a6a2e))

### Bug Fixes

* **token:** update color token ([c5fc473](https://gitlab.com/mobivia-design/roadtrip/css/commit/c5fc473ffa8301adfc20435c8f6c08e8a71099e2))


### [3.0.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/3.0.0...3.0.1) (2023-03-03)

### Bug Fixes

* **navbar:** add tooltip ([c5e9ff4](https://gitlab.com/mobivia-design/roadtrip/css/commit/c5e9ff4175d05ae843406a68c96879d67d768938))
* **profil-dropdown:** add z-index ([090e49e](https://gitlab.com/mobivia-design/roadtrip/css/commit/090e49e3a320d447811f2bec08c940bf20c0cd5a))

## [3.0.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.51.3...3.0.0) (2023-02-27)

### Features

* **Tokens:** add Tokens on components ([ae1059b](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/ae1059b6e4e7071f03620bd60d555f49b803d94b))

### [2.51.3](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.51.2...2.51.3) (2023-02-23)

### Bug Fixes

* **toast:** fix color toast danger ([fe6083f](https://gitlab.com/mobivia-design/roadtrip/css/commit/fe6083faa6db4610fe65cebcc02b181a2998accc))

### [2.51.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.51.1...2.51.2) (2023-02-17)

### Bug Fixes

* **tokens:** rename tokens on badge header ([6a73473](https://gitlab.com/mobivia-design/roadtrip/css/commit/6a7347360fdfc1f41281664783d5bf847b80c0f3))

### [2.51.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.51.0...2.51.1) (2023-02-16)

### Bug Fixes

* **progress:** add padding full width ([2990542](https://gitlab.com/mobivia-design/roadtrip/css/commit/29905427d72bea917a4039b5ea503ad1f958e05d))


## [2.51.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.50.1...2.51.0) (2023-02-08)

### Features

* **tokens:** update token status ([aa4f3e9](https://gitlab.com/mobivia-design/roadtrip/css/commit/aa4f3e9db953171490943ac83ccf189c0609e2d7))
* **token:** add token decorative + midas + auto5 ([09f233a](https://gitlab.com/mobivia-design/roadtrip/css/commit/09f233a2cf88bc7718b0e7ce12c23173f79fe326))
* **token:** fix token icon midas mobivia ([926b5b0](https://gitlab.com/mobivia-design/roadtrip/css/commit/926b5b07d8140e61c26849e49de2325c621692df))

### Bug Fixes

* **global-navigation:** size title header & border ([e79a542](https://gitlab.com/mobivia-design/roadtrip/css/commit/e79a54283b1840034800f0db57a58b75d50c88fd))


### [2.50.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.50.0...2.50.1) (2023-01-25)

### Bug Fixes

* **tokens:** update token secondary ([4b594a1](https://gitlab.com/mobivia-design/roadtrip/css/commit/4b594a1b28f955f0a58b33163b3209f5da7768a6))


## [2.50.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.49.0...2.50.0) (2023-01-25)

### Features

* **global-nav:** add new components ([ab3c4b3](https://gitlab.com/mobivia-design/roadtrip/css/commit/ab3c4b3986b9516c052323ce629064d703a210db))

### Bug Fixes

* **radio:** fix alignment ([efff666](https://gitlab.com/mobivia-design/roadtrip/css/commit/efff6667510998a6b8faf1c90d76a18bb81dc618))


## [2.49.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.48.2...2.49.0) (2023-01-17)

### Features

* **navbar:** add avatar ([e63eeda](https://gitlab.com/mobivia-design/roadtrip/css/commit/e63eeda3a6ce65a5d29ff0935e7ca5b52bb31f01))

### Bug Fixes

* **toolbar:** fix with button ([456d4a1](https://gitlab.com/mobivia-design/roadtrip/css/commit/456d4a1cb2ec59f27843a9a36fe352f14da28547))
* **toolbar:** fix back button ([52ddbb8](https://gitlab.com/mobivia-design/roadtrip/css/commit/52ddbb8fe10f303176fd580930f863a70ea9adaf))
* **variables:** fix tokens ([b333586](https://gitlab.com/mobivia-design/roadtrip/css/commit/b3335861e36716c918d2ef3c7c1389a2d7010102))


### [2.48.3](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.48.2...2.48.3) (2023-01-11)

### Bug Fixes

* name token header ([34638b3](https://gitlab.com/mobivia-design/roadtrip/css/commit/34638b31c4bd7850453c7495f8d8a9d5b93b37e7))

### [2.48.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.48.1...2.48.2) (2023-01-11)

### Bug Fixes
* add token TNS ([2abe723](https://gitlab.com/mobivia-design/roadtrip/css/commit/2abe7235e43cd505075447ee430a8a5ed1107f35))
* **skeleton:** fix animation ([9f7426d](https://gitlab.com/mobivia-design/roadtrip/css/commit/9f7426d917069545b91518774af6697071701727))


### [2.48.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.48.0...2.48.1) (2022-12-23)

### Bug Fixes

* **color:** update variable surface header ([8359664](https://gitlab.com/mobivia-design/roadtrip/css/commit/8359664cf09b20c0c1d9495de92091c07446cba5))


## [2.48.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.47.0...2.48.0) (2022-12-23)

### Features

* **avatar:** add avatar ([e732264](https://gitlab.com/mobivia-design/roadtrip/css/commit/e7322648f18eb273ee4c518fb93ba736004cbf1c))
* **profil-dropdown:** add component ([4b325ac](https://gitlab.com/mobivia-design/roadtrip/css/commit/4b325ac3d4906751e7fc5cf613aef8e58ace722f))

## [2.47.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.46.0...2.47.0) (2022-12-14)

### Features

* **verticalStepper:** add new component ([20961a1](https://gitlab.com/mobivia-design/roadtrip/css/commit/20961a17c73dae0389e2b6cf7d7717034ede08a9))
* **segmented-button:** add small version ([60a562a](https://gitlab.com/mobivia-design/roadtrip/css/commit/60a562aea069341ab08b2a10ecab4a9c1433dd0f))
* **color:** add variable badge header ([5f7acf1](https://gitlab.com/mobivia-design/roadtrip/css/commit/5f7acf1b8a2be26b26eba8c5bc28b0e5fcbed69c))

### Bug Fixes

* **button:** add -webkit for ios ([5c54109](https://gitlab.com/mobivia-design/roadtrip/css/commit/5c541092efd2a8166326372ec6decbe46a692c36))
refactor(counter): change color : ([a23cce9](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/a23cce98341ad3c524f36b89028c6abbba31ccc5))


## [2.46.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.45.0...2.46.0) (2022-11-30)

### Features

* **button:** add ghost button ([290999a](https://gitlab.com/mobivia-design/roadtrip/css/commit/290999af758a3cfb06dbc48c7c8e7440f6d11e95))
* **progress:** change position label and add step ([dfcf04f](https://gitlab.com/mobivia-design/roadtrip/css/commit/dfcf04f09c168610bd15a35c4b3b9942208988b0))

### Bug Fixes

* **segmentedbutton:** fix stories ([0e83f82](https://gitlab.com/mobivia-design/roadtrip/css/commit/0e83f82f1ec2b9ac0d477ad8adb90ec3d2d8a25a))
* **buttons:** add outline default button ([a300fec](https://gitlab.com/mobivia-design/roadtrip/css/commit/a300fecf128b8c602552ba30337956013980f720))


## [2.45.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.44.4...2.45.0) (2022-11-14)

### Features

* **color:** add variable color for header ([133f2e9](https://gitlab.com/mobivia-design/roadtrip/css/commit/133f2e9471fb929c0b2c4e99ac23e47565a546e4))
* **flaps:** add color black for black friday ([37506f1](https://gitlab.com/mobivia-design/roadtrip/css/commit/37506f1177c35ea927a6b075f18649eada04f97d))
* **segmentedButtons:** add new component ([2421bf3](https://gitlab.com/mobivia-design/roadtrip/css/commit/2421bf358669d26377596ef4ad610fd7a9abebb2))

### [2.44.4](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.44.3...2.44.4) (2022-11-04)

### Bug Fixes
Fix build - maj node version

### [2.44.3](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.44.2...2.44.3) (2022-11-04)

### Bug Fixes
Fix build - maj node version

### [2.44.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.44.1...2.44.2) (2022-11-04)

### Bug Fixes
Fix build - maj node version

### [2.44.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.44.0...2.44.1) (2022-11-04)

### Bug Fixes

* **build:** fix build ([e389394](https://gitlab.com/mobivia-design/roadtrip/css/commit/e389394bf1c5cd4260e4cc51386d9f85038e6276))

## [2.44.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.43.0...2.44.0) (2022-11-04)

### Features

* **range:** add labels option ([1ac14d9](https://gitlab.com/mobivia-design/roadtrip/css/commit/1ac14d9b5b73f5fa0f94256d2d57b1bfba7c29b0))
* **carousel:** update color pagers ([1bdce9e](https://gitlab.com/mobivia-design/roadtrip/css/commit/1bdce9edf2bdc4f6877a5c60eb88e411423fcea0))
* **link:** add variant white ([3bdfa59](https://gitlab.com/mobivia-design/roadtrip/css/commit/3bdfa59dc8c5d02c4abff9c1faafecff7d7bfe8c))
* **accordion:** add icon left ([21ebb2c](https://gitlab.com/mobivia-design/roadtrip/css/commit/21ebb2ca809cefa46d07f1e87698c457bbfdea3b))


## [2.43.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.42.0...2.43.0) (2022-10-06)

### Features

* **grid:** update padding ([6ad8990](https://gitlab.com/mobivia-design/roadtrip/css/commit/6ad89904d3e0ed2fb0e53cf19711be3d0d1ef1e6))
* **accordion:** add small version - update light ([4d97a14](https://gitlab.com/mobivia-design/roadtrip/css/commit/4d97a143bc36e4fd82e8170ae14827968a594c9b))

### Bug Fixes

* **toast:** fix color toast succes ([9c88cf1](https://gitlab.com/mobivia-design/roadtrip/css/commit/9c88cf1f4c1aed5f559cec106a34543ff1746a82))



## [2.42.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.41.0...2.42.0) (2022-09-14)

### Bug Fixes

* **accordion:** remove max-height ([56b3885](https://gitlab.com/mobivia-design/roadtrip/css/commit/56b3885b154415b1313b3adc8834db1337188688))

### Features

* **accordion:** add accordion light stories ([f6504fe](https://gitlab.com/mobivia-design/roadtrip/css/commit/f6504fec8f42f75ee5e0a0383497c16b79e35a56))
* **accordion:** add opening animation ([763476c](https://gitlab.com/mobivia-design/roadtrip/css/commit/763476ca21438b76d44a7db1d00b946bf4bad054))
* add border utilities ([00188fd](https://gitlab.com/mobivia-design/roadtrip/css/commit/00188fd93391abe6cbb4e58aea59c33540737234))
* add brand color ([1e160b0](https://gitlab.com/mobivia-design/roadtrip/css/commit/1e160b05f5834824318cbd59c5c396aa8d65126d))
* add breadcrumb pattern ([f2a1a8c](https://gitlab.com/mobivia-design/roadtrip/css/commit/f2a1a8c06ce914431e2a44bc13e05d7de3232dcd))
* add card component ([837a063](https://gitlab.com/mobivia-design/roadtrip/css/commit/837a0632276641a2c9f1254e10477d67f0d85e7e))
* add carousel component ([271b1e9](https://gitlab.com/mobivia-design/roadtrip/css/commit/271b1e9841d89f3eac7e74f2bc2c4ce04b3e0394))
* add chip component ([f850c54](https://gitlab.com/mobivia-design/roadtrip/css/commit/f850c54f69a5d56651f3d29805f1cba30ab8e701))
* add collapse component ([85bb3a8](https://gitlab.com/mobivia-design/roadtrip/css/commit/85bb3a8f0f2f598d4411cc277ffec6d7216cb4d8))
* add flap component ([4faa952](https://gitlab.com/mobivia-design/roadtrip/css/commit/4faa952bd148dc14b26b43c46076fee54b3c2e6f))
* add link component ([e37a3ed](https://gitlab.com/mobivia-design/roadtrip/css/commit/e37a3ed90366926f66d604ab02002eeeb5ca0add))
* add missing in-page components to essential bundle ([9f55709](https://gitlab.com/mobivia-design/roadtrip/css/commit/9f55709d21f08ffc7e28dc0f4d4dbcf4f8bc4eb9))
* add plate number component ([f218f90](https://gitlab.com/mobivia-design/roadtrip/css/commit/f218f904b8d0aff7bf1fee5f677927cc04584e47))
* add range component ([a6af901](https://gitlab.com/mobivia-design/roadtrip/css/commit/a6af901c7fc5ea05efa934516ae7c9d5e0e08f0a))
* add rating component ([d3e26d9](https://gitlab.com/mobivia-design/roadtrip/css/commit/d3e26d96203d8a7baffb358f8155e1d2cb31e655))
* add toggle component ([76584f8](https://gitlab.com/mobivia-design/roadtrip/css/commit/76584f80040bbbb807b0f801c051ae854c821e0c))
* add toolbar component ([36d2b56](https://gitlab.com/mobivia-design/roadtrip/css/commit/36d2b566437e75fe5cba3fea6856d17798240faf))
* add version number in dist files ([07c4360](https://gitlab.com/mobivia-design/roadtrip/css/commit/07c43600e1980c90337d71eb1ea1dc890412380d))
* **alert:** add link and title ([8bba45d](https://gitlab.com/mobivia-design/roadtrip/css/commit/8bba45de5db57473f4beb18b97eaa9f82888c381))
* **alert:** update colors and text size ([ca96a33](https://gitlab.com/mobivia-design/roadtrip/css/commit/ca96a33cd8ff605e58af9edbfd410fc3149970e3))
* **alert:** update design ([75ac33d](https://gitlab.com/mobivia-design/roadtrip/css/commit/75ac33ddd0a71e586702a310f1e4e045f5da74da))
* **alert:** update design ([13bbee3](https://gitlab.com/mobivia-design/roadtrip/css/commit/13bbee37ab2f62505f234d75f900f32e54cd4015))
* **alert:** update icon color to match text color ([9a40cab](https://gitlab.com/mobivia-design/roadtrip/css/commit/9a40cab8f91cc35bce0ef891ba7a443a7b88991d))
* **badge:** update colors and padding ([24a2dc6](https://gitlab.com/mobivia-design/roadtrip/css/commit/24a2dc60cbcbbc1b8233fce1cfee9a40d313a022))
* **banner:** add banner stories ([84c1aa3](https://gitlab.com/mobivia-design/roadtrip/css/commit/84c1aa3f991b16bd0d47cd337c2f7f481fe85262))
* **breakpoint:** add breakpoint 1200px for desktop users ([b2b99ab](https://gitlab.com/mobivia-design/roadtrip/css/commit/b2b99abc1b3fab14ce48b714fa8172a131028c15))
* **button:** add xl size ([b2291dc](https://gitlab.com/mobivia-design/roadtrip/css/commit/b2291dc0501db7bfa6960929c2d2cdc241460b77))
* **button:** update colors ([c930dd8](https://gitlab.com/mobivia-design/roadtrip/css/commit/c930dd85a9b50176191d4f9b133af0c060943926))
* **button:** update disabled state ([81c825a](https://gitlab.com/mobivia-design/roadtrip/css/commit/81c825a825db913951c7a277aa8cde5c02afd506))
* **checkbox:** add hover state ([8a44e10](https://gitlab.com/mobivia-design/roadtrip/css/commit/8a44e1018cd22205e77333719d0dba0dc8af06fb))
* **checkbox:** add indeterminate state ([7bee667](https://gitlab.com/mobivia-design/roadtrip/css/commit/7bee667503299d0466fb124bed799961b6011221))
* **checkbox:** Add inverse radio and label order feature ([0dba3a4](https://gitlab.com/mobivia-design/roadtrip/css/commit/0dba3a4e561a186a702b8be48b9d0710838a3ee4))
* **checkbox:** update colors and font-size ([015642c](https://gitlab.com/mobivia-design/roadtrip/css/commit/015642c68aa76bd4850b6c53dddde712fc14ebdd))
* **checkbox:** update focus design ([b269233](https://gitlab.com/mobivia-design/roadtrip/css/commit/b26923355e8320f234c7bb98be7a8698759dc4f9))
* **chip:** update colors and paddings ([ca3252e](https://gitlab.com/mobivia-design/roadtrip/css/commit/ca3252efd416acbfdfee343df296f4e166c866c3))
* **color:** update theme variable color ([833e825](https://gitlab.com/mobivia-design/roadtrip/css/commit/833e82535753111a57982861960d04478fa13c0b))
* **counter:** add icon version ([db7749a](https://gitlab.com/mobivia-design/roadtrip/css/commit/db7749aa9f924b90e4c6b859b88fd96ef96cef0a))
* **counter:** add other sizes ([91bc86b](https://gitlab.com/mobivia-design/roadtrip/css/commit/91bc86b8e37bdc5e46814ede3e27aac7dd368dfb))
* **counter:** increase size minus and more ([f48c327](https://gitlab.com/mobivia-design/roadtrip/css/commit/f48c327e75cda822eff2c6dfe98ed87840fdc714))
* **counter:** update button color ([8e2a542](https://gitlab.com/mobivia-design/roadtrip/css/commit/8e2a54240e7eb7bdf994e3cffe240f8a570e5d2f))
* **custom font:** update font path ([c08f787](https://gitlab.com/mobivia-design/roadtrip/css/commit/c08f78777b84eb81a1548def4d95cb924aed13b4))
* **deps:** update postcss & autoprefixer ([8b0b43e](https://gitlab.com/mobivia-design/roadtrip/css/commit/8b0b43e0723fd4d1dc17b5c7fcea449ff06e76e9))
* **dialog:** update colors, margins and font-size ([bf7572c](https://gitlab.com/mobivia-design/roadtrip/css/commit/bf7572c66051096b37b7b056d3e6c831e790a784))
* drop IE support ([327c55b](https://gitlab.com/mobivia-design/roadtrip/css/commit/327c55bd9eef65ef95d04d6c58a4597264bb339c))
* **dropdown:** add icon ([c13accc](https://gitlab.com/mobivia-design/roadtrip/css/commit/c13acccbd1fc1c92840d3b703ff6373cd3137375))
* **dropdown:** add position and opening direction ([6e9c7ff](https://gitlab.com/mobivia-design/roadtrip/css/commit/6e9c7ff56063b24990f1a3d2d73c02d905ae13ad))
* **dropdown:** refactor position ([1b5d4b6](https://gitlab.com/mobivia-design/roadtrip/css/commit/1b5d4b608f3e4654cb8492e2464d877188cac919))
* **dropdown:** update design ([bf09b71](https://gitlab.com/mobivia-design/roadtrip/css/commit/bf09b714ea37e1bb6b7a2d1c54967de0b5ab4185))
* **dropdown:** update width ([c4d17f2](https://gitlab.com/mobivia-design/roadtrip/css/commit/c4d17f2a8fb105ebe0823eb829672385bc397d59))
* **flap:** add ecology color ([1d9b5f6](https://gitlab.com/mobivia-design/roadtrip/css/commit/1d9b5f69898924a9abac5d6aaa9ec8d28c160565))
* **flap:** update colors ([c9fb3c8](https://gitlab.com/mobivia-design/roadtrip/css/commit/c9fb3c8cbb9ba05af95f8f5f6b1d24867c606c26))
* **forms:** add hover state ([5105f2f](https://gitlab.com/mobivia-design/roadtrip/css/commit/5105f2f5faf226715a928b4eaec84d531c8c8885))
* **form:** update font size for input and select ([b914636](https://gitlab.com/mobivia-design/roadtrip/css/commit/b914636417398078885e1876663c1024213de3fa))
* **icon:** add 3x and 4x sizes ([4ca321d](https://gitlab.com/mobivia-design/roadtrip/css/commit/4ca321d0197ca30454fce1deed42da88e252d9c0))
* **icons:** add rotation utilities ([cef2803](https://gitlab.com/mobivia-design/roadtrip/css/commit/cef2803762aa6b5b5d202596efb1209096064e75))
* **icons:** move arrows to chevron ([01b723e](https://gitlab.com/mobivia-design/roadtrip/css/commit/01b723e3e3805dfbc54d64d357873c9b7059873b))
* **icons:** update alter icons ([a6a5376](https://gitlab.com/mobivia-design/roadtrip/css/commit/a6a5376049be1f9277c0b7c5c2a67b513123025c))
* **input:** add input type time ([dfbb1d9](https://gitlab.com/mobivia-design/roadtrip/css/commit/dfbb1d96d7f9e427ae80f661631ed5d817a86dd1))
* **input:** add password ([eecebf0](https://gitlab.com/mobivia-design/roadtrip/css/commit/eecebf0e4061da961f81d4fb50a9a8fbcd3abd22))
* **input:** add required on input and select ([002a74f](https://gitlab.com/mobivia-design/roadtrip/css/commit/002a74f8385e8b889064af08cd4f3ba7873def43))
* **input:** add xl size ([1f26ebb](https://gitlab.com/mobivia-design/roadtrip/css/commit/1f26ebb0031d83d912064c5f34e7828fa0038c47))
* **items-per-page:** add items-per-page stories ([873d7ec](https://gitlab.com/mobivia-design/roadtrip/css/commit/873d7ecc662c74c2fbf387a013bbcd415cdf41d3))
* **modals:** mover layer color value to the CSS variable ([19fe403](https://gitlab.com/mobivia-design/roadtrip/css/commit/19fe403fe2f078381e9da00322cc998395b6e515))
* move fonts declaration in its own file ([42cf2ae](https://gitlab.com/mobivia-design/roadtrip/css/commit/42cf2aefda99094d066400ac467acf9f62a5c42a))
* **navbar-item:** update colors and label text size ([5de418f](https://gitlab.com/mobivia-design/roadtrip/css/commit/5de418f5106dc0932608381d65e9c4cdf32c40d6))
* **navbar:** update design ([5e33dfa](https://gitlab.com/mobivia-design/roadtrip/css/commit/5e33dfabe069090cc23f6f7291e756dc1c410339))
* **pagination:** update colors and margins ([6ba2f44](https://gitlab.com/mobivia-design/roadtrip/css/commit/6ba2f44b737e2858fe8bf8e334f63cc44cb9f442))
* **pagination:** update design ([daaec4f](https://gitlab.com/mobivia-design/roadtrip/css/commit/daaec4f3f86bd5a8db03a2ba7d6b496a3a36254e))
* **plate-number:** add Austria and German format ([1609bc7](https://gitlab.com/mobivia-design/roadtrip/css/commit/1609bc745724ad98a685d3c47cdddec9b676d931))
* **plateNumber:** add plate number for motorbike ([e3cf141](https://gitlab.com/mobivia-design/roadtrip/css/commit/e3cf1410eb15bce89904e87b234c7e0acc38d68f))
* **plateNumber:** update Belgian plate to be closer to real one ([a6276a0](https://gitlab.com/mobivia-design/roadtrip/css/commit/a6276a0d3193e1ad4f546e0b854c9932aaf0831f))
* **plateNumber:** update design ([f1fb32d](https://gitlab.com/mobivia-design/roadtrip/css/commit/f1fb32d9f1d701825a0e7e31fe36b12b385e0d88))
* **postcss:** update dependencies ([4649883](https://gitlab.com/mobivia-design/roadtrip/css/commit/464988335c56989d03a087ba0f4b126ade0ed18c))
* **progress:** add label ([acf5907](https://gitlab.com/mobivia-design/roadtrip/css/commit/acf5907045a8ecbd88956f194b09cc716beed24a))
* **progress:** add label ([068419b](https://gitlab.com/mobivia-design/roadtrip/css/commit/068419b122e75c333c888202da533df022b8a7d0))
* **radio:** add hover state ([c3de2d5](https://gitlab.com/mobivia-design/roadtrip/css/commit/c3de2d5e99aefef76f23aa28702b41873c16ba6e))
* **radio:** Add inverse radio and label order feature ([7212157](https://gitlab.com/mobivia-design/roadtrip/css/commit/7212157027adac865343be12bf970bc4d75fe032))
* **radio:** update colors and font-size ([082efef](https://gitlab.com/mobivia-design/roadtrip/css/commit/082efef94404d46df79adb047be3e522a23beade))
* **radio:** update focus design ([8321283](https://gitlab.com/mobivia-design/roadtrip/css/commit/8321283b3db27d0ed1cf5574a773f3203609f56e))
* **rating:** add small version ([8dbf627](https://gitlab.com/mobivia-design/roadtrip/css/commit/8dbf6275bc391bf115014f2300383927c0e8ebbd))
* **rating:** update colors and font size ([6b2903b](https://gitlab.com/mobivia-design/roadtrip/css/commit/6b2903b04dfa40ce21d73b7699b808589baf06ca))
* remove tap highlight on mobile ([b8c48c0](https://gitlab.com/mobivia-design/roadtrip/css/commit/b8c48c09663166f33fffebafe6d95623b5f4bef6))
* **select:** add xl size ([4c6d38f](https://gitlab.com/mobivia-design/roadtrip/css/commit/4c6d38fb5bb431598173060ce3fafcab04799315))
* **skeleton:** add 50 50 size and update color ([fc2ca04](https://gitlab.com/mobivia-design/roadtrip/css/commit/fc2ca049468811e8cd727b1093dbf5449cfb145c))
* **skeleton:** update grey values ([3b9b1bc](https://gitlab.com/mobivia-design/roadtrip/css/commit/3b9b1bc37a6a78dc4ef34e94d25a6e601ab4bf39))
* **skeleton:** update margin and height ([bb16636](https://gitlab.com/mobivia-design/roadtrip/css/commit/bb1663671b0295e02739d47fedc736bd248f17ed))
* **spacing:** add 4px padding and margin utilities ([8983c5c](https://gitlab.com/mobivia-design/roadtrip/css/commit/8983c5cd0334e48a3396e6d4b273733871902a73))
* **spinner:** add size spinner ([1021d3c](https://gitlab.com/mobivia-design/roadtrip/css/commit/1021d3cbed41ad48e9f31d1442b4fe27041b5da1))
* **spinner:** add xl size and color ([f5d23e7](https://gitlab.com/mobivia-design/roadtrip/css/commit/f5d23e7d957cb584d106840d506f06485f67480d))
* **stepper:** add variant ([5a8ddbe](https://gitlab.com/mobivia-design/roadtrip/css/commit/5a8ddbea4948929c3a2ac4923e34cdd13426edaf))
* **stepper:** update design ([6110b50](https://gitlab.com/mobivia-design/roadtrip/css/commit/6110b50b05415791a55a0b82fcc6797cdeb1850f))
* **storybook:** code source panel before accessibility ([8c975a4](https://gitlab.com/mobivia-design/roadtrip/css/commit/8c975a4536ab2500fcfce0dd8dcb84cf978dd4ea))
* **storybook:** missing `aria-label` on dialog component ([c07309f](https://gitlab.com/mobivia-design/roadtrip/css/commit/c07309f7616091a99db15da28fa06c6ddcf6168f))
* **storybook:** missing `aria-label` on drawer component ([b0d000f](https://gitlab.com/mobivia-design/roadtrip/css/commit/b0d000f46267d3080a10c5d2b9c0201bc8d7b839))
* **storybook:** missing `aria-label` on modal component ([243b8e0](https://gitlab.com/mobivia-design/roadtrip/css/commit/243b8e06798ce7e6477ef96a50a0d9f24af7cf1c))
* **storybook:** missing `aria-label` on progress component ([3d6f52c](https://gitlab.com/mobivia-design/roadtrip/css/commit/3d6f52c70db3c60b79ff0f9c1f44db132be422a2))
* **storybook:** move dropdown to expand section ([64bd1bb](https://gitlab.com/mobivia-design/roadtrip/css/commit/64bd1bbfe53fcb62d8dcf4fcee91d28e5fa73f18))
* **storybook:** replace storybook-addon-paddings by native implementation ([7cc4b3e](https://gitlab.com/mobivia-design/roadtrip/css/commit/7cc4b3ea8c0b3760f60af25789fe2b608b2a12e4))
* **storybook:** update dependencies ([e9e8713](https://gitlab.com/mobivia-design/roadtrip/css/commit/e9e8713c4e67d0a0f91537ffede6b4a14f98e923))
* **storybook:** update top widget order ([d8e5deb](https://gitlab.com/mobivia-design/roadtrip/css/commit/d8e5debba417a1376baf9ef8352f50b1169a95ca))
* **storybook:** update utility stories ([a7a63e5](https://gitlab.com/mobivia-design/roadtrip/css/commit/a7a63e5454004a014a32fd3ab8d46440e237a35c))
* **switch:** update focus state ([3b914a0](https://gitlab.com/mobivia-design/roadtrip/css/commit/3b914a0a2664011c9afa1f18bab2c8c98e5c4dd7))
* **tabs:** add color secondary ([79d1be9](https://gitlab.com/mobivia-design/roadtrip/css/commit/79d1be941558fe1a8897d6e966a1127c9f79d0d9))
* **tabs:** update design ([04cb54c](https://gitlab.com/mobivia-design/roadtrip/css/commit/04cb54c28d5ed1608ac894b607f0e93799d4b781))
* **tabs:** update hover tabs ([da91db2](https://gitlab.com/mobivia-design/roadtrip/css/commit/da91db2a42266a9ff3e449a263ab9b4b09cd51f8))
* **tabs:** update typo size and topicon ([f7a0acd](https://gitlab.com/mobivia-design/roadtrip/css/commit/f7a0acd42226bcbc41a4a196915990a5f448ae82))
* **theme:** add mobivia theme ([fa7e424](https://gitlab.com/mobivia-design/roadtrip/css/commit/fa7e424deba2df4003e54cca32d3bf9898dfd8cf))
* **themes:** update themes and add Auto5 theme ([a2840fb](https://gitlab.com/mobivia-design/roadtrip/css/commit/a2840fb5bb31d5eb9335b0c413df60ba7308af1c))
* **toast:** add top position ([21054ff](https://gitlab.com/mobivia-design/roadtrip/css/commit/21054ff5bd8a84be5b6c9f4dcde8b18d373de7ba))
* **toast:** update icons color for warning state ([ee13bf4](https://gitlab.com/mobivia-design/roadtrip/css/commit/ee13bf4d7acf0a62718e87257658aa334c38f71d))
* **toast:** update mobile design ([9adf1f1](https://gitlab.com/mobivia-design/roadtrip/css/commit/9adf1f181b446443e225e960ff6fa9d23af540e4))
* **toggle:** update colors and font-size ([8b5ce6b](https://gitlab.com/mobivia-design/roadtrip/css/commit/8b5ce6ba547a423679ca3205390aeafe75426182))
* **tooltip:** update size text and position ([a138d0d](https://gitlab.com/mobivia-design/roadtrip/css/commit/a138d0df208e447e99b82dac1f65440afde75de2))
* **typography:** add class text ([d33cbbf](https://gitlab.com/mobivia-design/roadtrip/css/commit/d33cbbf5d12d5f8cac4e670c91d8b97e2f960644))
* **typography:** add color title and class text ([281372f](https://gitlab.com/mobivia-design/roadtrip/css/commit/281372f513cc2fcb52db915cb4597beb0b8729fc))
* **typography:** add underline heading title ([6245ada](https://gitlab.com/mobivia-design/roadtrip/css/commit/6245ada0df8a2c6e1ea8d4a2a979013508bee0f4))
* **typography:** increase headings font weight ([acc2996](https://gitlab.com/mobivia-design/roadtrip/css/commit/acc2996c8dafbbd5d7784dcb2df2a3cd82dabcfa))
* **typography:** update font-weight for h7, h8, h9 ([3fa5b32](https://gitlab.com/mobivia-design/roadtrip/css/commit/3fa5b3280f7b139c83f3ff4726798114d1442466))
* update ATU theme ([b5351fe](https://gitlab.com/mobivia-design/roadtrip/css/commit/b5351fe09dee848bc4941b26993018111178651b))
* update ATU theme to reflect brand changes ([e6ab25e](https://gitlab.com/mobivia-design/roadtrip/css/commit/e6ab25e4dcac96ef21b4c7b34fd267c6fc3d2cba))
* update Auto5 theme ([01e4ec9](https://gitlab.com/mobivia-design/roadtrip/css/commit/01e4ec9e495f17cc83629d839d78c88df6b15b04))
* update color system ([6a39d3c](https://gitlab.com/mobivia-design/roadtrip/css/commit/6a39d3c29835f37f1ca30b7b1c2accf0def91074))
* update colours ([33ec5ca](https://gitlab.com/mobivia-design/roadtrip/css/commit/33ec5caa8f0cb1f3b75c23fe51affa8ce0394899))
* update components colors ([69ac6c7](https://gitlab.com/mobivia-design/roadtrip/css/commit/69ac6c7b4096ad2c64ea44ba7be0686002fa5ff6))
* update Midas theme ([cd5ca9f](https://gitlab.com/mobivia-design/roadtrip/css/commit/cd5ca9f1f9e5ccef259e1c525d9f261636d9b83a))
* update theme architecture ([9c5a07d](https://gitlab.com/mobivia-design/roadtrip/css/commit/9c5a07d07839f092c1aefed4fa998a11ee3565ee))
* **variables:** add hover colors ([61eb4f1](https://gitlab.com/mobivia-design/roadtrip/css/commit/61eb4f1f6f4c61bebdc46c83c6cd44a571c32476))


### Bug Fixes

* **accordion:** remove max-height ([56b3885](https://gitlab.com/mobivia-design/roadtrip/css/commit/56b3885b154415b1313b3adc8834db1337188688))
* **accordion:** remove max-height ([9b05931](https://gitlab.com/mobivia-design/roadtrip/css/commit/9b05931837f074f2ca6f88dd8ea115de7f3bef0c))
* **accordion:** update color border ([c6236b4](https://gitlab.com/mobivia-design/roadtrip/css/commit/c6236b4790ee3fd79b39874059fbfb19ffc1f6f4))
* **colors:** add color auto5 ([e14b6c5](https://gitlab.com/mobivia-design/roadtrip/css/commit/e14b6c51284315a42c00ed64baa24bc917e62d10))
* **colors:** map old background value to new one ([9ccc353](https://gitlab.com/mobivia-design/roadtrip/css/commit/9ccc353a3f7fb71cb0aac3877e84e02297848a58))
* **colors:** regeneration css ([2ef5171](https://gitlab.com/mobivia-design/roadtrip/css/commit/2ef5171479314087d8b86f6d60d267ebbcd2cb20))
* **colors:** version dist ([6611c43](https://gitlab.com/mobivia-design/roadtrip/css/commit/6611c4361d80dc4312e12cc0c8752f351801ce79))
* **counter:** add `aria-label` attribute for screen readers ([312bf7e](https://gitlab.com/mobivia-design/roadtrip/css/commit/312bf7ea7393068691e22f26d189f7296d5f66eb))
* **input:** fix opacity for Safari iOS 15+ ([530e13c](https://gitlab.com/mobivia-design/roadtrip/css/commit/530e13cf5af7c7870f7e39dae911451e77821dc0))
* **input:** fix placeholder opacity for firefox ([b8b84f9](https://gitlab.com/mobivia-design/roadtrip/css/commit/b8b84f91ce005239f319054f68cd5537a2f810b1))
* **input:** label overlapping for type date and time ([b4a0a04](https://gitlab.com/mobivia-design/roadtrip/css/commit/b4a0a042805550cd9f7a9150e65e60f2c53b295d))
* **Nuxt:** error with postcss-custom-properties ([b10cd0f](https://gitlab.com/mobivia-design/roadtrip/css/commit/b10cd0fe3ba4dc2873dc8b91bfe4a0b92b59d6c3))
* **radio:** radiobox shrink when long label ([fd91c49](https://gitlab.com/mobivia-design/roadtrip/css/commit/fd91c49d4d83f6a3b0ee2f84cf1f8b811a695c64))
* **range:** fix invisible range progress ([2b99742](https://gitlab.com/mobivia-design/roadtrip/css/commit/2b99742015c7ea039353cd3ae6b3da370b0df98d))
* **storybook:** fix click on dropdown story ([b9cb235](https://gitlab.com/mobivia-design/roadtrip/css/commit/b9cb235290b82e7cde496d4fbe13afa276b3c9bf))
* **storybook:** remove duplicate counter example ([91efd04](https://gitlab.com/mobivia-design/roadtrip/css/commit/91efd042a6bc7668a422e8172e3a018c2bf7584b))
* **storybook:** update name of gray classes in color utility examples ([d6c9ba6](https://gitlab.com/mobivia-design/roadtrip/css/commit/d6c9ba6f98a078bd1c7abd9c134e0bb7481d584a))
* **switch:** prevent lever from shrinking with long label ([082f640](https://gitlab.com/mobivia-design/roadtrip/css/commit/082f6409848511de5d39e8605fae1c43db60f02d))
* **themes:** fix themes with new color system ([6e54480](https://gitlab.com/mobivia-design/roadtrip/css/commit/6e54480a181eabfe994b5725d57f38cdc699a54f))
* update color for banner for atu and auto ([a841408](https://gitlab.com/mobivia-design/roadtrip/css/commit/a8414082d11da5fb1920912c5559392c45eed0f8))

## [2.41.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.40.0...2.41.0) (2022-08-30)
### Features

* **input:** add input type time ([dfbb1d9](https://gitlab.com/mobivia-design/roadtrip/css/commit/dfbb1d96d7f9e427ae80f661631ed5d817a86dd1))
* **counter:** add icon version ([db7749a](https://gitlab.com/mobivia-design/roadtrip/css/commit/db7749aa9f924b90e4c6b859b88fd96ef96cef0a))

### Bug Fixes

* update color for banner for atu and auto ([a841408](https://gitlab.com/mobivia-design/roadtrip/css/commit/a8414082d11da5fb1920912c5559392c45eed0f8))



## [2.40.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.39.0...2.40.0) (2022-08-02)

### Features

* **alert:** add link and title ([8bba45d](https://gitlab.com/mobivia-design/roadtrip/css/commit/8bba45de5db57473f4beb18b97eaa9f82888c381))

## [2.39.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.38.0...2.39.0) (2022-07-22)

### Features

* **input:** add password ([eecebf0](https://gitlab.com/mobivia-design/roadtrip/css/commit/eecebf0e4061da961f81d4fb50a9a8fbcd3abd22))
* **progress:** add label ([acf5907](https://gitlab.com/mobivia-design/roadtrip/css/commit/acf5907045a8ecbd88956f194b09cc716beed24a))
* **progress:** add label ([068419b](https://gitlab.com/mobivia-design/roadtrip/css/commit/068419b122e75c333c888202da533df022b8a7d0))
* **counter:** increase size minus and more ([f48c327](https://gitlab.com/mobivia-design/roadtrip/css/commit/f48c327e75cda822eff2c6dfe98ed87840fdc714))
* **rating:** add small version ([8dbf627](https://gitlab.com/mobivia-design/roadtrip/css/commit/8dbf6275bc391bf115014f2300383927c0e8ebbd))


## [2.38.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.37.0...2.38.0) (2022-07-07)


### Features

* **color:** update theme variable color ([833e825](https://gitlab.com/mobivia-design/roadtrip/css/commit/833e82535753111a57982861960d04478fa13c0b))
* **stepper:** add variant ([5a8ddbe](https://gitlab.com/mobivia-design/roadtrip/css/commit/5a8ddbea4948929c3a2ac4923e34cdd13426edaf))


## [2.37.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.36.0...2.37.0) (2022-06-23)


### Features

* **tooltip:** update size text and position ([a138d0d](https://gitlab.com/mobivia-design/roadtrip/css/commit/a138d0df208e447e99b82dac1f65440afde75de2))
* **skeleton:** add 50 50 size and update color ([fc2ca04](https://gitlab.com/mobivia-design/roadtrip/css/commit/fc2ca049468811e8cd727b1093dbf5449cfb145c))
* **skeleton:** update margin and height ([bb16636](https://gitlab.com/mobivia-design/roadtrip/css/commit/bb1663671b0295e02739d47fedc736bd248f17ed))
* **spinner:** add xl size and color ([f5d23e7](https://gitlab.com/mobivia-design/roadtrip/css/commit/f5d23e7d957cb584d106840d506f06485f67480d))

## [2.36.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.35.0...2.36.0) (2022-06-15)

### Features

* **input:** add required on input and select ([002a74f](https://gitlab.com/mobivia-design/roadtrip/css/commit/002a74f8385e8b889064af08cd4f3ba7873def43))
* **dropdown:** add position and opening direction ([6e9c7ff](https://gitlab.com/mobivia-design/roadtrip/css/commit/6e9c7ff56063b24990f1a3d2d73c02d905ae13ad))
* **dropdown:** add icon ([c13accc](https://gitlab.com/mobivia-design/roadtrip/css/commit/c13acccbd1fc1c92840d3b703ff6373cd3137375))
* **dropdown:** update width ([c4d17f2](https://gitlab.com/mobivia-design/roadtrip/css/commit/c4d17f2a8fb105ebe0823eb829672385bc397d59))
* **dropdown:** refactor position ([1b5d4b6](https://gitlab.com/mobivia-design/roadtrip/css/commit/1b5d4b608f3e4654cb8492e2464d877188cac919))
* **spinner:** add size spinner ([1021d3c](https://gitlab.com/mobivia-design/roadtrip/css/commit/1021d3cbed41ad48e9f31d1442b4fe27041b5da1))
* **plate-number:** add Austria and German format ([1609bc7](https://gitlab.com/mobivia-design/roadtrip/css/commit/1609bc745724ad98a685d3c47cdddec9b676d931))

### Bug Fixes

* **accordion:** remove max-height ([9b05931](https://gitlab.com/mobivia-design/roadtrip/css/commit/9b05931837f074f2ca6f88dd8ea115de7f3bef0c))
* **accordion:** update color border ([c6236b4](https://gitlab.com/mobivia-design/roadtrip/css/commit/c6236b4790ee3fd79b39874059fbfb19ffc1f6f4))


## [2.35.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.34.0...2.35.0) (2022-06-08)

### Refactor

* **accordion:** update bold header ([d46a340](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/d46a340136f637d1e58b2688912ac96e85a8ee17))
* **chip:** update color close ([6716c4c](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/6716c4c6a469d0e73cd168ea34f6425878f0a1ee))
* **typography:** update size h5 ([9f6c947](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/9f6c947e6b0aee4139b81422b591bdf98b118fc1))
* **form:** update color border([aee1357](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/aee1357f1a93c1286ac35cf95491d8cd11e06893))
* **progress-bar** update color secondary([49ed9f1](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/49ed9f1cd88db7d9756e4b4e3a7a9b26175e7b64))
* **tabs** delete padding([774fd09](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/774fd099356eeb7fdcf850d890fb7c459930d2d8))
* **flap** update color([9f2c3ad](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/9f2c3ada0f81d783d363abfef5f6912bd6c9218c))

### Features

* **tabs** add color secondary([79d1be9](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/79d1be941558fe1a8897d6e966a1127c9f79d0d9))

## [2.34.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.33.0...2.34.0) (2022-06-01)


### Features

* **typography:** add class text ([d33cbbf](https://gitlab.com/mobivia-design/roadtrip/css/commit/d33cbbf5d12d5f8cac4e670c91d8b97e2f960644))
* **typography:** add color title and class text ([281372f](https://gitlab.com/mobivia-design/roadtrip/css/commit/281372f513cc2fcb52db915cb4597beb0b8729fc))

### Bug Fixes

* **colors:** add color auto5 ([e14b6c5](https://gitlab.com/mobivia-design/roadtrip/css/commit/e14b6c51284315a42c00ed64baa24bc917e62d10))

## [2.33.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.32.1...v2.33.0) (2022-05-19)


### Features

* **range:** update color active and tick ([869b8a8](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/869b8a8fd47417f2e5f9614cd8db1d4f3127d569))
### [2.32.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.32.0...v2.32.1) (2022-05-12)


### Features


* add brand color ([1e160b0](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/1e160b05f5834824318cbd59c5c396aa8d65126d))

### Bug Fixes

* **themes:** fix themes with new color system ([6e54480](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/6e54480a181eabfe994b5725d57f38cdc699a54f))
* **colors:** map old background value to new one ([9ccc353](https://gitlab.com/mobivia-design/roadtrip/css/-/commit/9ccc353a3f7fb71cb0aac3877e84e02297848a58))

## [2.31.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.30.0...2.31.0) (2022-04-21)


### Features


* **typography:** add underline heading title ([6245ada](https://gitlab.com/mobivia-design/roadtrip/css/commit/6245ada0df8a2c6e1ea8d4a2a979013508bee0f4))
* **checkbox:** add indeterminate state ([7bee667](https://gitlab.com/mobivia-design/roadtrip/css/commit/7bee667503299d0466fb124bed799961b6011221))
* **counter:** update button color ([8e2a542](https://gitlab.com/mobivia-design/roadtrip/css/commit/8e2a54240e7eb7bdf994e3cffe240f8a570e5d2f))
* **button:** add xl size ([b2291dc](https://gitlab.com/mobivia-design/roadtrip/css/commit/b2291dc0501db7bfa6960929c2d2cdc241460b77))
* **alert:** update design ([75ac33d](https://gitlab.com/mobivia-design/roadtrip/css/commit/75ac33ddd0a71e586702a310f1e4e045f5da74da))
* **alert:** update design ([13bbee3](https://gitlab.com/mobivia-design/roadtrip/css/commit/13bbee37ab2f62505f234d75f900f32e54cd4015))
* **plateNumber:** update Belgian plate to be closer to real one ([a6276a0](https://gitlab.com/mobivia-design/roadtrip/css/commit/a6276a0d3193e1ad4f546e0b854c9932aaf0831f))
* **button:** add xl size ([b2291dc](https://gitlab.com/mobivia-design/roadtrip/css/commit/b2291dc0501db7bfa6960929c2d2cdc241460b77))
* **checkbox:** add hover state ([8a44e10](https://gitlab.com/mobivia-design/roadtrip/css/commit/8a44e1018cd22205e77333719d0dba0dc8af06fb))
* **skeleton:** update grey values ([3b9b1bc](https://gitlab.com/mobivia-design/roadtrip/css/commit/3b9b1bc37a6a78dc4ef34e94d25a6e601ab4bf39))
* update color system ([6a39d3c](https://gitlab.com/mobivia-design/roadtrip/css/commit/6a39d3c29835f37f1ca30b7b1c2accf0def91074))
* **alert:** update colors and text size ([ca96a33](https://gitlab.com/mobivia-design/roadtrip/css/commit/ca96a33cd8ff605e58af9edbfd410fc3149970e3))
* **badge:** update colors and padding ([24a2dc6](https://gitlab.com/mobivia-design/roadtrip/css/commit/24a2dc60cbcbbc1b8233fce1cfee9a40d313a022))
* **button:** update colors ([c930dd8](https://gitlab.com/mobivia-design/roadtrip/css/commit/c930dd85a9b50176191d4f9b133af0c060943926))
* **theme:** add mobivia theme ([fa7e424](https://gitlab.com/mobivia-design/roadtrip/css/commit/fa7e424deba2df4003e54cca32d3bf9898dfd8cf))
* **flap:** update colors ([c9fb3c8](https://gitlab.com/mobivia-design/roadtrip/css/commit/c9fb3c8cbb9ba05af95f8f5f6b1d24867c606c26))
* **chip:** update colors and paddings ([ca3252e](https://gitlab.com/mobivia-design/roadtrip/css/commit/ca3252efd416acbfdfee343df296f4e166c866c3))
* update components colors ([69ac6c7](https://gitlab.com/mobivia-design/roadtrip/css/commit/69ac6c7b4096ad2c64ea44ba7be0686002fa5ff6))
* **colors:** update color utilities ([a3679af](https://gitlab.com/mobivia-design/roadtrip/css/commit/a3679afeda03cb3b5ee0bb41f8f2febc9949584e))
* **rating:** update colors and font size ([6b2903b](https://gitlab.com/mobivia-design/roadtrip/css/commit/6b2903b04dfa40ce21d73b7699b808589baf06ca))
* add link component ([e37a3ed](https://gitlab.com/mobivia-design/roadtrip/css/commit/e37a3ed90366926f66d604ab02002eeeb5ca0add))
* update Auto5 theme ([01e4ec9](https://gitlab.com/mobivia-design/roadtrip/css/commit/01e4ec9e495f17cc83629d839d78c88df6b15b04))
* update ATU theme ([b5351fe](https://gitlab.com/mobivia-design/roadtrip/css/commit/b5351fe09dee848bc4941b26993018111178651b))
* update Midas theme ([cd5ca9f](https://gitlab.com/mobivia-design/roadtrip/css/commit/cd5ca9f1f9e5ccef259e1c525d9f261636d9b83a))
* add breadcrumb pattern ([f2a1a8c](https://gitlab.com/mobivia-design/roadtrip/css/commit/f2a1a8c06ce914431e2a44bc13e05d7de3232dcd))
* **navbar-item:** update colors and label text size ([5de418f](https://gitlab.com/mobivia-design/roadtrip/css/commit/5de418f5106dc0932608381d65e9c4cdf32c40d6))
* **pagination:** update colors and margins ([6ba2f44](https://gitlab.com/mobivia-design/roadtrip/css/commit/6ba2f44b737e2858fe8bf8e334f63cc44cb9f442))
* **dialog:** update colors, margins and font-size ([bf7572c](https://gitlab.com/mobivia-design/roadtrip/css/commit/bf7572c66051096b37b7b056d3e6c831e790a784))
* **modals:** mover layer color value to the CSS variable ([19fe403](https://gitlab.com/mobivia-design/roadtrip/css/commit/19fe403fe2f078381e9da00322cc998395b6e515))
* **checkbox:** update colors and font-size ([015642c](https://gitlab.com/mobivia-design/roadtrip/css/commit/015642c68aa76bd4850b6c53dddde712fc14ebdd))
* **radio:** update colors and font-size ([082efef](https://gitlab.com/mobivia-design/roadtrip/css/commit/082efef94404d46df79adb047be3e522a23beade))
* add toggle component ([76584f8](https://gitlab.com/mobivia-design/roadtrip/css/commit/76584f80040bbbb807b0f801c051ae854c821e0c))
* **toggle:** update colors and font-size ([8b5ce6b](https://gitlab.com/mobivia-design/roadtrip/css/commit/8b5ce6ba547a423679ca3205390aeafe75426182))


## [2.30.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.29.0...2.30.0) (2021-12-30)


### Features

* add version number in dist files ([07c4360](https://gitlab.com/mobivia-design/roadtrip/css/commit/07c43600e1980c90337d71eb1ea1dc890412380d))
* update theme architecture ([9c5a07d](https://gitlab.com/mobivia-design/roadtrip/css/commit/9c5a07d07839f092c1aefed4fa998a11ee3565ee))
* move fonts declaration in its own file ([42cf2ae](https://gitlab.com/mobivia-design/roadtrip/css/commit/42cf2aefda99094d066400ac467acf9f62a5c42a))


## [2.29.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.28.0...2.29.0) (2021-11-25)


### Features

* add card component ([837a063](https://gitlab.com/mobivia-design/roadtrip/css/commit/837a0632276641a2c9f1254e10477d67f0d85e7e))
* add carousel component ([271b1e9](https://gitlab.com/mobivia-design/roadtrip/css/commit/271b1e9841d89f3eac7e74f2bc2c4ce04b3e0394))
* add collapse component ([85bb3a8](https://gitlab.com/mobivia-design/roadtrip/css/commit/85bb3a8f0f2f598d4411cc277ffec6d7216cb4d8))
* add rating component ([d3e26d9](https://gitlab.com/mobivia-design/roadtrip/css/commit/d3e26d96203d8a7baffb358f8155e1d2cb31e655))
* add toolbar component ([36d2b56](https://gitlab.com/mobivia-design/roadtrip/css/commit/36d2b566437e75fe5cba3fea6856d17798240faf))
* update ATU theme to reflect brand changes ([e6ab25e](https://gitlab.com/mobivia-design/roadtrip/css/commit/e6ab25e4dcac96ef21b4c7b34fd267c6fc3d2cba))
* drop IE support ([327c55b](https://gitlab.com/mobivia-design/roadtrip/css/commit/327c55bd9eef65ef95d04d6c58a4597264bb339c))
* remove tap highlight on mobile ([b8c48c0](https://gitlab.com/mobivia-design/roadtrip/css/commit/b8c48c09663166f33fffebafe6d95623b5f4bef6))
* **plateNumber:** add plate number for motorbike ([e3cf141](https://gitlab.com/mobivia-design/roadtrip/css/commit/e3cf1410eb15bce89904e87b234c7e0acc38d68f))
* **dropdown:** update design ([bf09b71](https://gitlab.com/mobivia-design/roadtrip/css/commit/bf09b714ea37e1bb6b7a2d1c54967de0b5ab4185))
* **stepper:** update design ([6110b50](https://gitlab.com/mobivia-design/roadtrip/css/commit/6110b50b05415791a55a0b82fcc6797cdeb1850f))
* **accordion:** add opening animation ([763476c](https://gitlab.com/mobivia-design/roadtrip/css/commit/763476ca21438b76d44a7db1d00b946bf4bad054))
* **icon:** add rotation utilities ([cef2803](https://gitlab.com/mobivia-design/roadtrip/css/commit/cef2803762aa6b5b5d202596efb1209096064e75))
* **icon:** add 3x and 4x sizes ([4ca321d](https://gitlab.com/mobivia-design/roadtrip/css/commit/4ca321d0197ca30454fce1deed42da88e252d9c0))
* **flap:** add ecology color ([1d9b5f6](https://gitlab.com/mobivia-design/roadtrip/css/commit/1d9b5f69898924a9abac5d6aaa9ec8d28c160565))

### Bug Fixes

* **input:** fix opacity for Safari iOS 15+ ([530e13c](https://gitlab.com/mobivia-design/roadtrip/css/commit/530e13cf5af7c7870f7e39dae911451e77821dc0))


## [2.28.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.27.0...2.28.0) (2021-10-08)


### Features

* **counter:** add other sizes ([91bc86b](https://gitlab.com/mobivia-design/roadtrip/css/commit/91bc86b8e37bdc5e46814ede3e27aac7dd368dfb))



## [2.27.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.26.0...2.27.0) (2021-09-28)


### Bug Fixes

* **switch:** prevent lever from shrinking with long label ([082f640](https://gitlab.com/mobivia-design/roadtrip/css/commit/082f6409848511de5d39e8605fae1c43db60f02d))

## [2.26.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.25.0...2.26.0) (2021-07-22)


### Features

* **themes:** update themes and add Auto5 theme ([a2840fb](https://gitlab.com/mobivia-design/roadtrip/css/commit/a2840fb5bb31d5eb9335b0c413df60ba7308af1c))


### Bug Fixes

* **range:** fix invisible range progress ([2b99742](https://gitlab.com/mobivia-design/roadtrip/css/commit/2b99742015c7ea039353cd3ae6b3da370b0df98d))



## [2.25.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.24.1...2.25.0) (2021-07-13)


### Features

* add plate number component ([f218f90](https://gitlab.com/mobivia-design/roadtrip/css/commit/f218f904b8d0aff7bf1fee5f677927cc04584e47))



## [2.24.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.24.0...2.24.1) (2021-06-02)


### Bug Fixes

* **Nuxt:** error with postcss-custom-properties ([b10cd0f](https://gitlab.com/mobivia-design/roadtrip/css/commit/b10cd0fe3ba4dc2873dc8b91bfe4a0b92b59d6c3))



## [2.24.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.23.0...2.24.0) (2021-05-06)


### Features

* add range component ([a6af901](https://gitlab.com/mobivia-design/roadtrip/css/commit/a6af901c7fc5ea05efa934516ae7c9d5e0e08f0a))
* **variables:** add hover colors ([61eb4f1](https://gitlab.com/mobivia-design/roadtrip/css/commit/61eb4f1f6f4c61bebdc46c83c6cd44a571c32476))


### Bug Fixes

* **input:** fix placeholder opacity for firefox ([b8b84f9](https://gitlab.com/mobivia-design/roadtrip/css/commit/b8b84f91ce005239f319054f68cd5537a2f810b1))



## [2.23.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.22.0...2.23.0) (2021-03-31)


### Features

* **alert:** update icon color to match text color ([9a40cab](https://gitlab.com/mobivia-design/roadtrip/css/commit/9a40cab8f91cc35bce0ef891ba7a443a7b88991d))
* **alert:** update alert icons ([a6a5376](https://gitlab.com/mobivia-design/roadtrip/css/commit/a6a5376049be1f9277c0b7c5c2a67b513123025c))
* **toast:** update icons color for warning state ([ee13bf4](https://gitlab.com/mobivia-design/roadtrip/css/commit/ee13bf4d7acf0a62718e87257658aa334c38f71d))



## [2.22.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.21.0...2.22.0) (2021-03-04)


### Features

* add chip component ([f850c54](https://gitlab.com/mobivia-design/roadtrip/css/commit/f850c54f69a5d56651f3d29805f1cba30ab8e701))
* **checkbox:** Add inverse radio and label order feature ([0dba3a4](https://gitlab.com/mobivia-design/roadtrip/css/commit/0dba3a4e561a186a702b8be48b9d0710838a3ee4))
* **radio:** Add inverse radio and label order feature ([7212157](https://gitlab.com/mobivia-design/roadtrip/css/commit/7212157027adac865343be12bf970bc4d75fe032))



## [2.21.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.20.2...2.21.0) (2021-02-24)


### Features

* add border utilities ([00188fd](https://gitlab.com/mobivia-design/roadtrip/css/commit/00188fd93391abe6cbb4e58aea59c33540737234))
* **breakpoint:** add breakpoint 1200px for desktop users ([b2b99ab](https://gitlab.com/mobivia-design/roadtrip/css/commit/b2b99abc1b3fab14ce48b714fa8172a131028c15))
* **spacing:** add 4px padding and margin utilities ([8983c5c](https://gitlab.com/mobivia-design/roadtrip/css/commit/8983c5cd0334e48a3396e6d4b273733871902a73))
* **typography:** update font-weight for h7, h8, h9 ([3fa5b32](https://gitlab.com/mobivia-design/roadtrip/css/commit/3fa5b3280f7b139c83f3ff4726798114d1442466))



## [2.20.2](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.20.1...2.20.2) (2021-02-10)


### Bug Fixes

* **form:** update font size for input and select ([b914636](https://gitlab.com/mobivia-design/roadtrip/css/commit/b914636417398078885e1876663c1024213de3fa))



## [2.20.1](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.20.0...2.20.1) (2021-02-04)


### Bug Fixes

* **radio:** radiobox shrink when long label ([fd91c49](https://gitlab.com/mobivia-design/roadtrip/css/commit/fd91c49d4d83f6a3b0ee2f84cf1f8b811a695c64))



## [2.20.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.19.0...2.20.0) (2021-02-03)


### Features

* **icons:** move arrows to chevron ([01b723e](https://gitlab.com/mobivia-design/roadtrip/css/commit/01b723e3e3805dfbc54d64d357873c9b7059873b))



### Bug Fixes

* **input:** label overlapping for type date and time ([b4a0a04](https://gitlab.com/mobivia-design/roadtrip/css/commit/b4a0a042805550cd9f7a9150e65e60f2c53b295d))



## [2.19.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.18.0...2.19.0) (2021-01-26)


### Features

* **alert:** update design ([75ac33d](https://gitlab.com/mobivia-design/roadtrip/css/commit/75ac33ddd0a71e586702a310f1e4e045f5da74da))
* **pagination:** update design ([daaec4f](https://gitlab.com/mobivia-design/roadtrip/css/commit/daaec4f3f86bd5a8db03a2ba7d6b496a3a36254e))
* **toast:** add top position ([21054ff](https://gitlab.com/mobivia-design/roadtrip/css/commit/21054ff5bd8a84be5b6c9f4dcde8b18d373de7ba))


### Bug Fixes

* **counter:** add `aria-label` attribute for screen readers ([312bf7e](https://gitlab.com/mobivia-design/roadtrip/css/commit/312bf7ea7393068691e22f26d189f7296d5f66eb))



## [2.18.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.17.0...2.18.0) (2020-12-08)


### Features

* **alert:** update design ([13bbee3](https://gitlab.com/mobivia-design/roadtrip/css/commit/13bbee37ab2f62505f234d75f900f32e54cd4015))
* **button:** update disabled state ([81c825a](https://gitlab.com/mobivia-design/roadtrip/css/commit/81c825a825db913951c7a277aa8cde5c02afd506))
* **checkbox:** add hover state ([8a44e10](https://gitlab.com/mobivia-design/roadtrip/css/commit/8a44e1018cd22205e77333719d0dba0dc8af06fb))
* **checkbox:** update focus design ([b269233](https://gitlab.com/mobivia-design/roadtrip/css/commit/b26923355e8320f234c7bb98be7a8698759dc4f9))
* **custom font:** update font path ([c08f787](https://gitlab.com/mobivia-design/roadtrip/css/commit/c08f78777b84eb81a1548def4d95cb924aed13b4))
* **radio:** add hover state ([c3de2d5](https://gitlab.com/mobivia-design/roadtrip/css/commit/c3de2d5e99aefef76f23aa28702b41873c16ba6e))
* **radio:** update focus design ([8321283](https://gitlab.com/mobivia-design/roadtrip/css/commit/8321283b3db27d0ed1cf5574a773f3203609f56e))
* **switch:** update focus state ([3b914a0](https://gitlab.com/mobivia-design/roadtrip/css/commit/3b914a0a2664011c9afa1f18bab2c8c98e5c4dd7))
* **toast:** update mobile design ([9adf1f1](https://gitlab.com/mobivia-design/roadtrip/css/commit/9adf1f181b446443e225e960ff6fa9d23af540e4))
* **typography:** increase headings font weight ([acc2996](https://gitlab.com/mobivia-design/roadtrip/css/commit/acc2996c8dafbbd5d7784dcb2df2a3cd82dabcfa))



## [2.17.0](https://gitlab.com/mobivia-design/roadtrip/css/compare/2.16.0...2.17.0) (2020-11-19)


### Features

* add flap component ([4faa952b](https://gitlab.com/mobivia-design/roadtrip/css/commit/4faa952bd148dc14b26b43c46076fee54b3c2e6f))