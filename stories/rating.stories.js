export default {
  title: 'Listing/Rating',
};

export const Default = () => `
<div class="rating d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
  </div>
  <a class="rating-number" href="#">(5 reviews)</a>
</div>

<div class="rating rating-small d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
  </div>
  <a class="rating-number" href="#">(5 reviews)</a>
</div>

<div class="rating rating-extra-small d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
  </div>
  <a class="rating-number" href="#">(5 reviews)</a>
</div>


<div class="rating d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
  </div>
  
</div>

<div class="rating rating-small d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star"></svg>
  </div>
  
</div>

`;

export const Rating = () => `
<div class="rating d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star"><use href="#star-half-color"></svg>
    <svg class="rating-star"><use href="#star"></svg>
  </div>
  <a class="rating-number" href="#">(4 reviews)</a>
</div>
`;

export const MaxRating = () => `
<div class="rating d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
  </div>
  <a class="rating-number" href="#">(50 reviews)</a>
</div>
`;


export const ReadOnly = () => `
<div class="rating d-inline-flex align-items-center">
  <div class="rating-stars">
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
    <svg class="rating-star rating-star-active"><use href="#star"></svg>
  </div>
  <label class="rating-number read-only">(50 reviews)</label>
</div>
`;