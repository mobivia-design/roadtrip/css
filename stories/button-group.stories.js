export default {
  title: 'Forms/Button group'
}

export const Primary = () => `
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-outline-primary">Left</button>
  <button type="button" class="btn btn-outline-primary">Middle</button>
  <button type="button" class="btn btn-outline-primary">Right</button>
</div>
`;

export const Secondary = () => `
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-outline-secondary">Left</button>
  <button type="button" class="btn btn-outline-secondary">Middle</button>
  <button type="button" class="btn btn-outline-secondary">Right</button>
</div>
`;

export const Default = () => `
<div class="btn-group" role="group" aria-label="Basic example">
  <button type="button" class="btn btn-outline-default">Left</button>
  <button type="button" class="btn btn-outline-default">Middle</button>
  <button type="button" class="btn btn-outline-default">Right</button>
</div>
`;

export const ButtonSizes = () => `
<div class="btn-group btn-group-md" role="group" aria-label="Default button group">
  <button type="button" class="btn btn-outline-primary">Left</button>
  <button type="button" class="btn btn-outline-primary">Middle</button>
  <button type="button" class="btn btn-outline-primary">Right</button>
</div>
<br><br>
<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
  <button type="button" class="btn btn-outline-primary">Left</button>
  <button type="button" class="btn btn-outline-primary">Middle</button>
  <button type="button" class="btn btn-outline-primary">Right</button>
</div>
`;
