export default {
  title: 'Listing/List',
  parameters: {
    layout: 'fullscreen',
  },
}

export const Links = () => `
<div class="list-group">
  <a href="#" class="list-group-item list-group-item-action list-group-item-arrow active">
    Drawer link
  </a>
  <a href="#" class="list-group-item list-group-item-action list-group-item-arrow">Drawer link</a>
  <a href="#" class="list-group-item list-group-item-action list-group-item-arrow">Drawer link</a>
  <a href="#" class="list-group-item list-group-item-action list-group-item-arrow">Drawer link</a>
  <a href="#" class="list-group-item list-group-item-action list-group-item-arrow">Drawer link</a>
</div>
`;

export const Control = () => `
<div class="list-group" role="radiogroup" aria-label="label of this fields">
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="one" name="listControl"/>
    <label class="list-group-label" for="one">
      list control one
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="two" name="listControl"/>
    <label class="list-group-label" for="two">
      list control two
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="three" name="listControl"/>
    <label class="list-group-label" for="three">
      list control three
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
</div>
`;

export const ControlMultipleChoices = () => `
<div class="list-group list-group-inverse" role="group" aria-label="label of this fields">
  <div class="list-group-item">
    <input class="list-group-input" type="checkbox" id="first" name="listControl"/>
    <label class="list-group-label" for="first">
      list control first
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="checkbox" id="second" name="listControl"/>
    <label class="list-group-label" for="second">
      list control second
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="checkbox" id="third" name="listControl"/>
    <label class="list-group-label" for="third">
      list control third
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
</div>
`;

export const WithSegmentation = () => `
<div class="list-group">
  <h2 class="list-group-title">Title</h2>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="segmentOneFirst" name="listControl"/>
    <label class="list-group-label" for="segmentOneFirst">
      control
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="segmentOneSecond" name="listControl"/>
    <label class="list-group-label" for="segmentOneSecond">
      control
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="segmentOneThird" name="listControl"/>
    <label class="list-group-label" for="segmentOneThird">
      control
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <h2 class="list-group-title">Title</h2>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="segmentTwoFirst" name="listControl"/>
    <label class="list-group-label" for="segmentTwoFirst">
      control
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="segmentTwoSecond" name="listControl"/>
    <label class="list-group-label" for="segmentTwoSecond">
      control
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <div class="list-group-item">
    <input class="list-group-input" type="radio" id="segmentTwoThird" name="listControl"/>
    <label class="list-group-label" for="segmentTwoThird">
      control
      <svg xmlns="http://www.w3.org/2000/svg" class="list-icon" viewBox="0 0 64 64"><use xlink:href="#check-wide"/></svg>
    </label>
  </div>
  <button class="btn btn-link btn-block">
    More
  </button>
</div>
`;
