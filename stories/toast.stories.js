export default {
  title: 'Indicators/Toast',
  parameters: {
    docs: {
      inlineStories: false,
      iframeHeight: '300px',
    },
  },
}

export const Info = () => `
<div class="toast-container toast-open" role="alert">
  <div class="toast">
    <svg xmlns="http://www.w3.org/2000/svg" class="toast-icon" width="32px" height="32px" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
    <p class="toast-label">Information sticky toast</p>
    <button type="button" class="toast-close" aria-label="Close">
      <svg xmlns="http://www.w3.org/2000/svg" class="toast-close-icon" width="24px" height="24px" viewBox="0 0 64 64"><use xlink:href="#navigation-close"/></svg>
    </button>
    <div class="progress progress-light mt-8">
      <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>
</div>


<script>
setTimeout(() => {
  document.querySelector('.toast-open').style.display = 'none';
}, 5000);

</script>
`;

export const Success = () => `
<div class="toast-container toast-open" role="alert">
  <div class="toast toast-success">
    <svg xmlns="http://www.w3.org/2000/svg" class="toast-icon" width="32px" height="32px" viewBox="0 0 64 64"><use xlink:href="#alert-success-outline"/></svg>
    <p class="toast-label">Success sticky toast</p>
    <button type="button" class="toast-close" aria-label="Close">
      <svg xmlns="http://www.w3.org/2000/svg" class="toast-close-icon" width="24px" height="24px" viewBox="0 0 64 64"><use xlink:href="#navigation-close"/></svg>
    </button>
    <div class="progress progress-light mt-8">
      <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>
</div>

<script>
setTimeout(() => {
  document.querySelector('.toast-open').style.display = 'none';
}, 5000);

</script>
`;

export const Warning = () => `
<div class="toast-container toast-open" role="alert">
  <div class="toast toast-warning">
    <svg xmlns="http://www.w3.org/2000/svg" class="toast-icon" width="32px" height="32px" viewBox="0 0 64 64"><use xlink:href="#alert-warning-outline"/></svg>
    <p class="toast-label">Warning sticky toast</p>
    <button type="button" class="toast-close" aria-label="Close">
      <svg xmlns="http://www.w3.org/2000/svg" class="toast-close-icon" width="24px" height="24px" viewBox="0 0 64 64"><use xlink:href="#navigation-close"/></svg>
    </button>
    <div class="progress progress-light mt-8">
      <div class="progress-bar bg-warning" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>
</div>

<script>
setTimeout(() => {
  document.querySelector('.toast-open').style.display = 'none';
}, 5000);

</script>
`;

export const Danger = () => `
<div class="toast-container toast-open" role="alert">
  <div class="toast toast-danger">
    <svg xmlns="http://www.w3.org/2000/svg" class="toast-icon" width="32px" height="32px" viewBox="0 0 64 64"><use xlink:href="#alert-danger-outline"/></svg>
    <p class="toast-label">Danger sticky toast</p>
    <button type="button" class="toast-close" aria-label="Close">
      <svg xmlns="http://www.w3.org/2000/svg" class="toast-close-icon" width="24px" height="24px" viewBox="0 0 64 64"><use xlink:href="#navigation-close"/></svg>
    </button>
  </div>
</div>

<script>
document.querySelector('.toast-close').onclick = function() {
  document.querySelector('.toast-open').style.display = 'none';
}
</script>
`;
