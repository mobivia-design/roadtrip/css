export default {
  title: 'Forms/Button floating'
}

export const Playground = () => `
<button class="btn-floating">
  <svg xmlns="http://www.w3.org/2000/svg" class="btn-floating-icon" viewBox="0 0 64 64">
    <use xlink:href="#shopping-cart-add"/>
  </svg>
  <span class="btn-floating-label">Add to cart</span>
</button>

<script>
document.addEventListener("scroll", (event) => {
  if (window.scrollY < 10) {
    document.querySelector('.btn-floating').style.maxWidth = "max-content";
    document.querySelector('.btn-floating').style.display = "flex";
  }
  else{
    document.querySelector('.btn-floating').style.maxWidth = "3.5rem";
    document.querySelector('.btn-floating').style.display = "block";
  }
});
</script>

`;

export const Left = () => `
<button class="btn-floating btn-floating-left">
  <svg xmlns="http://www.w3.org/2000/svg" class="btn-icon" viewBox="0 0 64 64">
    <use xlink:href="#shopping-cart-add"/>
  </svg>
  <span class="btn-floating-label">Add to cart</span>
</button>

<script>
document.addEventListener("scroll", (event) => {
  if (window.scrollY < 10) {
    document.querySelector('.btn-floating.btn-floating-left').style.maxWidth = "max-content";
    document.querySelector('.btn-floating.btn-floating-left').style.display = "flex";
  }
  else{
    document.querySelector('.btn-floating.btn-floating-left').style.maxWidth = "3.5rem";
    document.querySelector('.btn-floating.btn-floating-left').style.display = "block";
  }
});
</script>
`;

export const Center = () => `
<button class="btn-floating btn-floating-center">
  <svg xmlns="http://www.w3.org/2000/svg" class="btn-icon" viewBox="0 0 64 64">
    <use xlink:href="#shopping-cart-add"/>
  </svg>
  <span class="btn-floating-label">Add to cart</span>
</button>
`;
