export default {
  title: 'Expand/Accordion',
};

export const Playground = () => `

<details class="accordion">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

<details class="accordion accordion-light">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

<details class="accordion accordion-light accordion-light--border">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="accordion-icon-left">
        <use xlink:href="#alert-info-outline"/>
      </svg>
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

<details class="accordion accordion-light accordion-light--border">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>


<details class="accordion accordion-light accordion-light--small">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>


<details class="accordion accordion-light accordion-light--small accordion-light--border">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

<details class="accordion accordion-light accordion-light--small accordion-light--border">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header"> 
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="accordion-icon-left">
        <use xlink:href="#alert-info-outline"/>
      </svg>
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

`;

export const Closed = () => `
<details class="accordion">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

<details class="accordion accordion-light">
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>
`;

export const Open = () => `
<details class="accordion" open>
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>

<details class="accordion accordion-light" open>
  <summary class="accordion-trigger" tabindex="0" role="button">
    <div class="accordion-header accordion-light-header">
      Accordion
      <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow" viewBox="0 0 64 64">
        <use xlink:href="#navigation-chevron"/>
      </svg>
    </div>
  </summary>
  <div class="accordion-content accordion-light-content">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
  </div>
</details>
`;