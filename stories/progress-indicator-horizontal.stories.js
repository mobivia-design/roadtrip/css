export default {
  title: 'Navigation/Progress Indicator Horizontal'
}

export const Default = () => `
<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal">  
    <li class="progress-indicator-horizontal-item current">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">1</span>
        <span class="progress-indicator-horizontal-title">First step</span>
      </a>
    </li>
    <li class="progress-indicator-horizontal-item">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">2</span>
        <span class="progress-indicator-horizontal-title">Second step<span>Second ligne label</span></span>
      </a>
    </li>
    <li class="progress-indicator-horizontal-item">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">3</span>
        <span class="progress-indicator-horizontal-title">Third step</span>
      </a>
    </li>
  </ul>
</nav>

<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal progress-indicator-horizontal-header">  
    <li class="progress-indicator-horizontal-item current">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">1</span>
        <span class="progress-indicator-horizontal-title">First step</span>
      </a>
    </li>
    <li class="progress-indicator-horizontal-item">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">2</span>
        <span class="progress-indicator-horizontal-title">Second step</span>
      </a>
    </li>
    <li class="progress-indicator-horizontal-item">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">3</span>
        <span class="progress-indicator-horizontal-title">Third step</span>
      </a>
    </li>
  </ul>
</nav>
`;

export const InProgress = () => `
<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal">  
    <li class="progress-indicator-horizontal-item current in-progress">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">1</span>
        <span class="progress-indicator-horizontal-title">First step</span>
      </a>
    </li>
    <li class="progress-indicator-horizontal-item">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">2</span>
        <span class="progress-indicator-horizontal-title">Second step<span>Second ligne label</span></span>
      </a>
    </li>
    <li class="progress-indicator-horizontal-item">
      <a class="progress-indicator-horizontal-link">
        <span class="progress-indicator-horizontal-icon">3</span>
        <span class="progress-indicator-horizontal-title">Third step</span>
      </a>
    </li>
  </ul>
</nav>
`;

export const OneCompletedStep = () => `
<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal">  
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">First step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item current">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">2</span>
          <span class="progress-indicator-horizontal-title">Second step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">3</span>
          <span class="progress-indicator-horizontal-title">Third step</span>
        </a>
      </li>
  </ul>
</nav>

<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal progress-indicator-horizontal-header">  
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">First step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item current">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">2</span>
          <span class="progress-indicator-horizontal-title">Second step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">3</span>
          <span class="progress-indicator-horizontal-title">Third step</span>
        </a>
      </li>
  </ul>
</nav>
`;

export const TwoCompletedSteps = () => `
<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal">  
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">First step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">Second step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item current">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">3</span>
          <span class="progress-indicator-horizontal-title">Third step</span>
        </a>
      </li>
  </ul>
</nav>

<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal progress-indicator-horizontal-secondary">  
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">First step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">Second step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item current">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">3</span>
          <span class="progress-indicator-horizontal-title">Third step</span>
        </a>
      </li>
  </ul>
</nav>

<nav aria-label="Stepper example">
  <ul class="progress-indicator-horizontal progress-indicator-horizontal-header">  
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">First step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item completed">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-horizontal-title">Second step</span>
        </a>
      </li>
      <li class="progress-indicator-horizontal-item current">
        <a class="progress-indicator-horizontal-link">
          <span class="progress-indicator-horizontal-icon">3</span>
          <span class="progress-indicator-horizontal-title">Third step</span>
        </a>
      </li>
  </ul>
</nav>
`;
