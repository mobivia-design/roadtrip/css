export default {
  title: 'Forms/Textarea'
}

export const Default = () => `
<div class="form-group">
  <textarea class="form-control textarea-control form-control-xl" type="text" id="inputClassic" required rows="3"></textarea>
  <label class="form-label" for="inputClassic">Label</label>
</div>
`;

export const WithValue = () => `
<div class="form-group">
  <textarea class="form-control textarea-control form-control-xl" type="text" id="inputClassic" required rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium pharetra semper. Fusce lacus metus, facilisis sit amet lacus ut, facilisis dapibus urna.</textarea>
  <label class="form-label" for="inputClassic">Label</label>
</div>
`;

export const Disabled = () => `
<div class="form-group">
  <textarea class="form-control textarea-control form-control-xl" type="text" id="inputDisabled" disabled rows="3"></textarea>
  <label class="form-label" for="inputDisabled">Label</label>
</div>
`;

export const Error = () => `
<div class="form-group was-validated">
  <textarea class="form-control textarea-control form-control-xl" type="text" id="inputError" required rows="3"></textarea>
  <label class="form-label" for="inputError">Label</label>
  <p class="invalid-feedback">This field is required</p>
</div>
`;

export const NoResize = () => `
<div class="form-group">
  <textarea class="form-control textarea-control no-resize form-control-xl" type="text" id="inputClassic" required rows="3"></textarea>
  <label class="form-label" for="inputClassic">Label</label>
</div>
`;