export default {
  title: 'Navigation/Link'
}

export const Playground = () => `
<a class="link" href="#">Link</a>

<br><br>

<a class="link d-inline-flex align-items-center" href="#">
  <svg class="icon-sm mr-4"><use href="#edit-pen"/></svg>
  Link
</a>

<br><br>

<a class="link d-inline-flex align-items-center" href="#">
  Link
  <svg class="icon-sm ml-4"><use href="#arrow"/></svg>
</a>
`;

export const Default = () => `
<a class="link link-default" href="#">Link</a>
`;

export const White = () => `
<div class="bg-secondary p-24">
  <a class="link link-white" href="#">Link</a>
</div>
`;


export const Sizes = () => `
<a class="link link-md" href="#">Link</a>

<br><br>

<a class="link link-sm" href="#">Link</a>
`;
