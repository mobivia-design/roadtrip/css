export default {
  title: 'Listing/Card',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
};

export const Value = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card" value="125">
        125
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card" value="135">
        135
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card" value="145">
        145
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card" value="155">
        155
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card" value="165">
        165
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card" value="175">
        175
      </button>
    </div>
  </road-row>
</div>
`;

export const ElevationLowest = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-lowest" value="125">
        125
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-lowest" value="135">
        135
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-lowest" value="145">
        145
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-lowest" value="155">
        155
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-lowest" value="165">
        165
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-lowest" value="175">
        175
      </button>
    </div>
  </road-row>
</div>
`;

export const ElevationAverage = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-average" value="125">
        125
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-average" value="135">
        135
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-average" value="145">
        145
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-average" value="155">
        155
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-average" value="165">
        165
      </button>
    </div>
    <div class="col-4 col-md-3 col-lg-2">
      <button class="card card-elevation-average" value="175">
        175
      </button>
    </div>
  </road-row>
</div>
`;

export const vehicle = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col-6 col-md-4 col-lg-3">
      <button class="card" value="car">
        <svg class="icon-3x icon-gray" aria-hidden="true"><use href="#vehicle-car"></svg>
        <div class="text-gray text-truncate">Car</div>
      </button>
    </div>
    <div class="col-6 col-md-4 col-lg-3">
      <button class="card" value="4x4">
        <svg class="icon-3x icon-gray" aria-hidden="true"><use href="#vehicle-suv"></svg>
        <div class="text-gray text-truncate">4x4</div>
      </button>
    </div>
    <div class="col-6 col-md-4 col-lg-3">
      <button class="card" value="truck">
        <svg class="icon-3x icon-gray" aria-hidden="true"><use href="#vehicle-pickup-van"></svg>
        <div class="text-gray text-truncate">Truck</div>
      </button>
    </div>
    <div class="col-6 col-md-4 col-lg-3">
      <button class="card" value="bike">
        <svg class="icon-3x icon-gray" aria-hidden="true"><use href="#vehicle-moto"></svg>
        <div class="text-gray text-truncate">Bike</div>
      </button>
    </div>
  </div>
</div>
`;
