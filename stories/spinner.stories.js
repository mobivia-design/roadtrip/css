export default {
  title: 'Indicators/Spinner'
}

export const Default = () => `
<svg class="spinner spinner--small" viewBox="25 25 50 50">
  <circle class="spinner-circle" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--medium" viewBox="25 25 50 50">
  <circle class="spinner-circle" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--large" viewBox="25 25 50 50">
  <circle class="spinner-circle" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--xl" viewBox="25 25 50 50">
  <circle class="spinner-circle" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--small" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--light" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--medium" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--light" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--large" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--light" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--xl" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--light" cx="50" cy="50" r="20"></circle>
</svg>


<svg class="spinner spinner--small" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--dark" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--medium" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--dark" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--large" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--dark" cx="50" cy="50" r="20"></circle>
</svg>

<svg class="spinner spinner--xl" viewBox="25 25 50 50">
  <circle class="spinner-circle spinner-circle--dark" cx="50" cy="50" r="20"></circle>
</svg>
`;
