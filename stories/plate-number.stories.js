export default {
  title: 'Forms/PlateNumber',
};

export const Car = () => `
<div class="input-group plate-number">
  <input class="plate-number-input mb-0" id="plate-number-id" placeholder="AT-857-YY"/>
  <div class="input-group-prepend">
    <label class="input-group-text p-0" for="plate-number-id">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          F
        </div>
      </div>
    </label>
  </div>
  <div class="input-group-append">
    <label class="input-group-text p-0" for="plate-number-id">
      <div class="plate-number-end">
        &nbsp;
      </div>
    </label>
  </div>
</div>

<div class="input-group plate-number">
  <input class="plate-number-input mb-0" id="plate-number-id" placeholder="FF-10-FF"/>
  <div class="input-group-prepend">
    <label class="input-group-text p-0" for="plate-number-id">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          A
        </div>
      </div>
    </label>
  </div>
  <div class="input-group-append">
    <label class="input-group-text p-0" for="plate-number-id">
      <div class="plate-number-end">
        &nbsp;
      </div>
    </label>
  </div>
</div>

<div class="input-group plate-number">
  <input class="plate-number-input mb-0" id="plate-number-id" placeholder="RA-KL-8136"/>
  <div class="input-group-prepend">
    <label class="input-group-text p-0" for="plate-number-id">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          D
        </div>
      </div>
    </label>
  </div>
  <div class="input-group-append">
    <label class="input-group-text p-0" for="plate-number-id">
      <div class="plate-number-end">
        &nbsp;
      </div>
    </label>
  </div>
</div>

<div class="input-group plate-number plate-number-be">
  <input class="plate-number-input mb-0" id="plate-number-be-id" placeholder="1-AAA-001" value="2-ABC-123"/>
  <div class="input-group-prepend">
    <label class="input-group-text p-0" for="plate-number-be-id">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          B
        </div>
      </div>
    </label>
  </div>
  <div class="input-group-append">
    <label class="input-group-text" for="plate-number-be-id">
      &nbsp;
    </label>
  </div>
</div>
`;

export const Disabled = () => `
<div class="input-group plate-number">
  <input class="plate-number-input mb-0" placeholder="AT-857-YY" value="AA-123-AA" disabled/>
  <div class="input-group-prepend">
    <label class="input-group-text p-0">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          F
        </div>
      </div>
    </label>
  </div>
  <div class="input-group-append">
    <label class="input-group-text p-0">
      <div class="plate-number-end">
        &nbsp;
      </div>
    </label>
  </div>
</div>
`;

export const ReadOnly = () => `
<div class="input-group plate-number">
  <input class="plate-number-input mb-0" placeholder="AT-857-YY" value="AA-123-AA" readonly/>
  <div class="input-group-prepend">
    <label class="input-group-text p-0">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          F
        </div>
      </div>
    </label>
  </div>
  <div class="input-group-append">
    <label class="input-group-text p-0">
      <div class="plate-number-end">
        &nbsp;
      </div>
    </label>
  </div>
</div>
`;

export const Motorbike = () => `
<div class="input-group plate-number motorbike-plate" style="width:200px;">
  <textarea class="form-control plate-number-input mb-0" placeholder="AT-\n857-YY" maxlength="9" rows="2"></textarea>
  <div class="input-group-prepend">
    <div class="input-group-text">
      <div class="plate-number-start">
        <svg class="plate-number-icon" viewBox="0 0 64 64"><use xlink:href="#license-plate-star-eu"/></svg>
        <div class="plate-number-location">
          F
        </div>
      </div>
    </div>
  </div>
  <div class="input-group-append">
    <div class="input-group-text">
      <div class="plate-number-end">&nbsp;</div>
    </div>
  </div>
</div>
`;
