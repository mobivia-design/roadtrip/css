export default {
  title: 'Indicators/Tag',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
};

export const Playground = () => `
<span class="tag tag-grey">
  Label
</span>
<span class="tag tag-yellow">
  Label
</span>
<span class="tag tag-red">
  Label
</span>
<span class="tag tag-violet">
  Label
</span>
<span class="tag tag-blue">
  Label
</span>
<span class="tag tag-green">
  Label
</span>
`;

export const Contrast = () => `
<span class="tag tag-grey tag-grey-contrast">
  Label
</span>
<span class="tag tag-yellow tag-yellow-contrast">
  Label
</span>
<span class="tag tag-red tag-red-contrast">
  Label
</span>
<span class="tag tag-violet tag-violet-contrast">
  Label
</span>
<span class="tag tag-blue tag-blue-contrast">
  Label
</span>
<span class="tag tag-green tag-green-contrast">
  Label
</span>
`;

