export default {
    title: 'Navigation/SegmentedButton'
  }
  
  export const Playground = () => `
  <div class="segmented-button-bar">
    <button class="btn btn-secondary btn-md active" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-md" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-md" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-md" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-md" type="button" role="tab">
        Label
    </button>
  </div>
  `;

  export const Small = () => `
  <div class="segmented-button-bar">
    <button class="btn btn-secondary btn-sm active" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-sm" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-sm" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-sm" type="button" role="tab">
        Label
    </button>
    <button class="btn btn-outline-secondary btn-sm" type="button" role="tab">
        Label
    </button>
  </div>
  `;
  