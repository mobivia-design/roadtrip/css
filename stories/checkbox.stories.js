export default {
  title: 'Forms/Checkbox'
}

export const Default = () => `
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="checkboxDefault"/>
  <label class="form-check-label" for="checkboxDefault">Label</label>
</div>
`;

export const Checked = () => `
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="checkboxChecked" checked/>
  <label class="form-check-label" for="checkboxChecked">Label</label>
</div>
`;

export const Indeterminate = () => `
<script>
  document.querySelector('#checkboxIndeterminate').indeterminate = true;
</script>
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="checkboxIndeterminate"/>
  <label class="form-check-label" for="checkboxIndeterminate">Label</label>
</div>
`;

export const Disabled = () => `
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="checkboxDisabled" disabled/>
  <label class="form-check-label" for="checkboxDisabled">Label</label>
</div>
`;

export const Error = () => `
<div class="form-check was-validated">
  <input class="form-check-input" type="checkbox" id="checkboxError" required/>
  <label class="form-check-label" for="checkboxError">Label</label>
  <p class="invalid-feedback">Check the checkbox to continue</p>
</div>
`;

export const Inverse = () => `
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="checkboxInverse"/>
  <label class="form-check-label form-checkbox-inverse" for="checkboxInverse">Label</label>
</div>
`;

export const SecondaryLabel = () => `
<div class="form-check">
  <input class="form-check-input" type="checkbox" id="checkboxDefault"/>
  <label class="form-check-label" for="checkboxDefault">Label <span class="form-check-label-span">(1)</span></label>
</div>
`;