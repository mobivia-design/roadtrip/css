export default {
  title: 'Forms/Radio'
}

export const Inline = () => `
<div class="form-group" role="radiogroup" aria-label="label of this fields">
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioInline1" name="radioInline" checked/>
    <label class="form-radio-label" for="radioInline1">Label1</label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioInline2" name="radioInline"/>
    <label class="form-radio-label" for="radioInline2">Label2</label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioInline3" name="radioInline"/>
    <label class="form-radio-label" for="radioInline3">Label3</label>
  </div>
</div>
`;


export const Stacked = () => `
<div class="form-group" role="radiogroup" aria-label="label of this fields">
  <div class="form-radio">
    <input class="form-radio-input" type="radio" id="radioClassic1" name="radioClassic" checked/>
    <label class="form-radio-label" for="radioClassic1">Label1</label>
  </div>
  <div class="form-radio">
    <input class="form-radio-input" type="radio" id="radioClassic2" name="radioClassic"/>
    <label class="form-radio-label" for="radioClassic2">Label2</label>
  </div>
  <div class="form-radio">
    <input class="form-radio-input" type="radio" id="radioClassic3" name="radioClassic"/>
    <label class="form-radio-label" for="radioClassic3">Label3</label>
  </div>
</div>
`;

export const Inverse = () => `
<div class="form-group" role="radiogroup" aria-label="label of this fields">
  <div class="form-radio">
    <input class="form-radio-input" type="radio" id="radioInverse1" name="radioInverse" checked/>
    <label class="form-radio-label form-radio-inverse" for="radioInverse1">Label1</label>
  </div>
  <div class="form-radio">
    <input class="form-radio-input" type="radio" id="radioInverse2" name="radioInverse"/>
    <label class="form-radio-label form-radio-inverse" for="radioInverse2">Label2</label>
  </div>
  <div class="form-radio">
    <input class="form-radio-input" type="radio" id="radioInverse3" name="radioInverse"/>
    <label class="form-radio-label form-radio-inverse" for="radioInverse3">Label3</label>
  </div>
</div>
`;

export const Disabled = () => `
<div class="form-group" role="radiogroup" aria-label="label of this fields">
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioDisabled1" name="radioDisabled" disabled/>
    <label class="form-radio-label" for="radioDisabled1">Label1</label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioDisabled2" name="radioDisabled" disabled/>
    <label class="form-radio-label" for="radioDisabled2">Label2</label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioDisabled3" name="radioDisabled" disabled/>
    <label class="form-radio-label" for="radioDisabled3">Label3</label>
  </div>
</div>
`;

export const Error = () => `
<div class="form-group is-invalid form-radio" role="radiogroup" aria-label="label of this fields">
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioError1" name="type" required/>
    <label class="form-radio-label" for="radioError1">Label1</label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioError2" name="type" required/>
    <label class="form-radio-label" for="radioError2">Label2</label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioError3" name="type" required/>
    <label class="form-radio-label" for="radioError3">Label3</label>
  </div>
  <p class="invalid-feedback">This field is required.</p>
</div>
`;

export const SecondaryLabel = () => `
<div class="form-group" role="radiogroup" aria-label="label of this fields">
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioInline1" name="radioInline" checked/>
    <label class="form-radio-label" for="radioInline1">Label1 <span class="form-radio-label-span">(1)</span></label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioInline2" name="radioInline"/>
    <label class="form-radio-label" for="radioInline2">Label2 <span class="form-radio-label-span">(1)</span></label>
  </div>
  <div class="form-radio form-radio-inline">
    <input class="form-radio-input" type="radio" id="radioInline3" name="radioInline"/>
    <label class="form-radio-label" for="radioInline3">Label3 <span class="form-radio-label-span">(1)</span></label>
  </div>
</div>
`;
