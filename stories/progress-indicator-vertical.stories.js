export default {
    title: 'Navigation/Progress Indicator Vertical'
  }
  
  export const Default = () => `
  <nav aria-label="Vertical Stepper example">
    <ul class="progress-indicator-vertical">  
      <li class="progress-indicator-vertical-item current">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">1</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">First step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">2</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Second step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">3</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Third step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">4</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Fourth step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">5</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Fifth step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
    </ul>
  </nav>
  `;
  
  export const OneCompletedStep = () => `
  <nav aria-label="Vertical Stepper example">
    <ul class="progress-indicator-vertical">  
      <li class="progress-indicator-vertical-item completed">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
          </span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">First step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item current">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">2</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Second step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">3</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Third step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">4</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Fourth step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-indicator-vertical-item">
        <a class="progress-indicator-vertical-link">
          <span class="progress-indicator-vertical-icon">5</span>
          <span class="progress-indicator-vertical-line"></span>
        </a>
        <div class="progress-indicator-vertical-item-content">
            <span class="progress-indicator-vertical-title">Fifth step</span>
            <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
    </ul>
  </nav>
  `;
  
  export const AllCompletedSteps = () => `
  <nav aria-label="Vertical Stepper example">
  <ul class="progress-indicator-vertical">  
    <li class="progress-indicator-vertical-item completed">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">First step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item completed">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Second step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item completed">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Third step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item completed">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Fourth step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item current">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">5</span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Fifth step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
  </ul>
</nav>
  `;

  export const SubSteps = () => `
  <nav aria-label="Vertical Stepper example">
  <ul class="progress-indicator-vertical">  
    <li class="progress-indicator-vertical-item completed">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">First step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item current">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Second step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item">
      <a class="progress-indicator-vertical-substep-link">
        <span class="progress-indicator-vertical-substep-beforeline"></span>
        <span class="progress-indicator-vertical-substep-icon"></span>
        <span class="progress-indicator-vertical-substep-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Third step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-indicator-vertical-item">
      <a class="progress-indicator-vertical-link">
        <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
        </span>
        <span class="progress-indicator-vertical-line"></span>
      </a>
      <div class="progress-indicator-vertical-item-content">
          <span class="progress-indicator-vertical-title">Fourth step</span>
          <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
  </ul>
</nav> 

<nav aria-label="Vertical Stepper example">
<ul class="progress-indicator-vertical">  
  <li class="progress-indicator-vertical-item completed">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">First step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
  <li class="progress-indicator-vertical-item in-progress">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">Second step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
  <li class="progress-indicator-vertical-item current">
    <a class="progress-indicator-vertical-substep-link">
      <span class="progress-indicator-vertical-substep-beforeline"></span>
      <span class="progress-indicator-vertical-substep-icon"></span>
      <span class="progress-indicator-vertical-substep-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">Third step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
  <li class="progress-indicator-vertical-item">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">Fourth step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
</ul>
</nav>

<nav aria-label="Vertical Stepper example">
<ul class="progress-indicator-vertical">  
  <li class="progress-indicator-vertical-item completed">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">First step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
  <li class="progress-indicator-vertical-item completed">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">Second step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
  <li class="progress-indicator-vertical-item completed">
    <a class="progress-indicator-vertical-substep-link">
      <span class="progress-indicator-vertical-substep-beforeline"></span>
      <span class="progress-indicator-vertical-substep-icon"></span>
      <span class="progress-indicator-vertical-substep-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">Third step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
  <li class="progress-indicator-vertical-item current">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><use xlink:href="#check-small"/></svg>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
        <span class="progress-indicator-vertical-title">Fourth step</span>
        <p class="progress-indicator-vertical-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>
  </li>
</ul>
</nav>

`;
  