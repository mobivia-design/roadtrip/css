export default {
  title: 'Forms/Range'
}

export const Default = () => `
<div class="form-group">
  <div class="form-range" style="--min:0; --max:10; --value:8">
    <input type="range" class="form-range-input" id="formControlRange" min="0" max="10" value="8" oninput="this.parentNode.style.setProperty('--value',this.value)"/>
    <div class="form-range-progress"></div>
  </div>
</div>
`;

export const Disabled = () => `
<div class="form-group">
  <div class="form-range disabled" style="--min:0; --max:10; --value:8">
    <input type="range" class="form-range-input" id="formControlRange" min="0" max="10" value="8" oninput="this.parentNode.style.setProperty('--value',this.value)" disabled/>
    <div class="form-range-progress disabled"></div>
  </div>
</div>
`;

export const WithValue = () => `
<div class="form-group">
  <div class="form-range" style="--min:0; --max:10; --value:8">
    <output></output>
    <input type="range" class="form-range-input" min="0" max="10" value="8" oninput="this.parentNode.style.setProperty('--value',this.value)"/>
    <div class="form-range-progress"></div>
  </div>
</div>
`;

export const WithTick = () => `
<div class="form-group d-flex align-items-end">
  <svg class="icon-sm mr-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
    <use xlink:href="#navigation-add-less-solid"/>
  </svg>
  <div class="form-range" style="--min:0; --max:10; --value:8">
    <output></output>
    <input type="range" class="form-range-input" min="0" max="10" value="8" oninput="this.parentNode.style.setProperty('--value',this.value)"/>
    <div class="form-range-progress"></div>
  </div>
  <svg class="icon-sm ml-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
    <use xlink:href="#navigation-add-more-solid"/>
  </svg>
</div>
`;


export const WithLabel = () => `
<div class="form-group d-flex align-items-end">
  <svg class="icon-sm mr-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
    <use xlink:href="#navigation-add-less-solid"/>
  </svg>
  <div class="form-range" style="--min:0; --max:10; --value:8">
    <datalist id="tickmarks">
      <option value="0" label="0%"></option>
      <option value="10" label="10%"></option>
      <option value="20" label="20%"></option>
      <option value="30" label="30%"></option>
      <option value="40" label="40%"></option>
      <option value="50" label="50%"></option>
      <option value="60" label="60%"></option>
      <option value="70" label="70%"></option>
      <option value="80" label="80%"></option>
      <option value="90" label="90%"></option>
      <option value="100" label="100%"></option>
    </datalist>
    <input type="range" class="form-range-input" min="0" max="10" value="8" oninput="this.parentNode.style.setProperty('--value',this.value)" list="tickmarks"/>

    <div class="form-range-progress"></div>
    
  </div>
  <svg class="icon-sm ml-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
    <use xlink:href="#navigation-add-more-solid"/>
  </svg>
</div>
`;
