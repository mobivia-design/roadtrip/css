export default {
    title: 'Indicators/Banner'
  }


  export const Banner = () => `
<script>
document.querySelector('.banner-close').addEventListener('click', () => {
  document.querySelector('.banner').parentNode.remove();
});
</script>

<div class="banner align-items-center">
        <span class="banner-label font-weight-bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
        <a href="#" class="link link-default banner-link">See more</a>
        <button type="button" class="banner-close" aria-label="Close">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="close-icon">
                <use xlink:href="#navigation-close"/>
            </svg>
        </button>    
</div>
  `;
