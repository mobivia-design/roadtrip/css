export default {
  title: 'Utilities/Flexbox',
  parameters: {
    layout: 'fullscreen',
  },
};

export const JustifyContent = () => `
<div class="d-flex justify-content-start mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>

<div class="d-flex justify-content-end mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>

<div class="d-flex justify-content-center mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>

<div class="d-flex justify-content-between mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>

<div class="d-flex justify-content-around mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>
`;

export const AlignItems = () => `
  <div class="d-flex align-items-start mt-16 mb-16 bg-light" style="height: 100px">
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  </div>

  <div class="d-flex align-items-end mt-16 mb-16 bg-light" style="height: 100px">
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  </div>

  <div class="d-flex align-items-center mt-16 mb-16 bg-light" style="height: 100px">
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  </div>

  <div class="d-flex align-items-baseline mt-16 mb-16 bg-light" style="height: 100px">
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  </div>

  <div class="d-flex align-items-stretch mt-16 mb-16 bg-light" style="height: 100px">
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
    <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  </div>
`;

export const Grow = () => `
<div class="d-flex justify-content-start mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16 flex-grow-1">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>
`;

export const Shrink = () => `
<div class="d-flex justify-content-start mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16 flex-shrink-1">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>
`;

export const Flex = () => `
<div class="d-flex justify-content-start mt-16 mb-16 bg-light">
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16 flex-1">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
  <div class="bg-secondary text-white pt-16 pb-16 pl-16 pr-16">Flex item</div>
</div>
`;

