export default {
  title: 'Modals/Modal',
  parameters: {
    docs: {
      inlineStories: false,
      iframeHeight: '600px',
    },
  },
}

export const Default = () => `
<div class="modal modal-open" tabindex="-1" role="dialog" aria-label="label of the modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="modal-header">
        <h2 class="modal-title">Modal title</h2>
        <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <h2 class="h2">Headline section</h2>
              <p class="h7">The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps.</p>
              <p class="text-content">
                Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! "Now fax quiz Jack!" my brave ghost pled.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
`;

export const HeaderInverse = () => `
<div class="modal modal-open" tabindex="-1" role="dialog" aria-label="label of the modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <header class="modal-header modal-header-inverse">
        <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <h2 class="h2">Headline section</h2>
              <p class="h7">The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps.</p>
              <p class="text-content">
                Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! "Now fax quiz Jack!" my brave ghost pled.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
`;
