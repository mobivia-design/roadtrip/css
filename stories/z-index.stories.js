export default {
    title: 'Utilities/Z-index',
  };
  
  export const Exemples = () => `

  <div class="my-24">
  <div class="position-absolute z-10" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-warning-surface-inverse);top:0;"></div>

  <div class="position-absolute z-20" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-90);top:1rem;"></div>
  
  <div class="position-absolute z-30" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-danger-surface-inverse);top:2rem;"></div>
  
  <div class="position-absolute z-40" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-success-surface-inverse);top:3rem;"></div>
  
  <div class="position-absolute z-50" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-info-surface-inverse);top:4rem;"></div>
</div>
  `;