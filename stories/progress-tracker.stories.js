export default {
    title: 'Navigation/ProgressTracker'
  }
  
  export const Default = () => `
  <nav aria-label="Progress Tracker example">
    <ul class="progress-tracker">  
      <li class="progress-tracker-item in-progress">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">First step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item completed">
        <div class="progress-tracker-link">
          <span class="progress-tracker-substep-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-substep-title">Title of the sub step</span>
            <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-substep-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-substep-title">Title of the sub step</span>
            <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">Second step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">Third step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">Fourth step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">Fifth step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
    </ul>
  </nav>
  `;
  
  export const OneCompletedStep = () => `
  <nav aria-label="Progress Tracker example">
  <ul class="progress-tracker">  
    <li class="progress-tracker-item completed">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">First step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item completed">
      <div class="progress-tracker-link">
        <span class="progress-tracker-substep-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-substep-title">Title of the sub step</span>
          <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item completed">
      <div class="progress-tracker-link">
        <span class="progress-tracker-substep-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-substep-title">Title of the sub step</span>
          <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item in-progress">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Second step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item current">
      <div class="progress-tracker-link">
        <span class="progress-tracker-substep-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-substep-title">Title of the sub step</span>
          <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Third step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Fourth step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Fifth step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
  </ul>
</nav>
  `;
  
  export const ShowMoreSteps = () => `
  <div class="collapse">
  <nav aria-label="Progress Tracker example">
  <ul class="progress-tracker">  
    <li class="progress-tracker-item completed">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">First step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item completed">
      <div class="progress-tracker-link">
        <span class="progress-tracker-substep-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-substep-title">Title of the sub step</span>
          <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item completed">
      <div class="progress-tracker-link">
        <span class="progress-tracker-substep-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-substep-title">Title of the sub step</span>
          <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item in-progress">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Second step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item current">
      <div class="progress-tracker-link">
        <span class="progress-tracker-substep-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-substep-title">Title of the sub step</span>
          <p class="progress-tracker-substep-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Third step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Fourth step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
    <li class="progress-tracker-item">
      <div class="progress-tracker-link">
        <span class="progress-tracker-circle"></span>
        <span class="progress-tracker-line"></span>
      </div>
      <div class="progress-tracker-item-content">
          <span class="progress-tracker-title">Fifth step</span>
          <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
    </li>
  </ul>

    <div class="collapsed-content">
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">Six step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
      <li class="progress-tracker-item">
        <div class="progress-tracker-link">
          <span class="progress-tracker-circle"></span>
          <span class="progress-tracker-line"></span>
        </div>
        <div class="progress-tracker-item-content">
            <span class="progress-tracker-title">Seven step</span>
            <p class="progress-tracker-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
      </li>
    </div>
    <div class="collapse-btn-wrapper">
      <button class="collapse-btn btn btn-link">Show more</button>
    </div>
 
</nav>
</div>
  
<script>
document.querySelector('.collapse .collapse-btn').addEventListener('click', () => {
  document.querySelector('.collapse .collapsed-content').classList.toggle('collapse-open');
  let btnContent = document.querySelector('.collapse .collapsed-content').classList.contains('collapse-open') ? 'Show less' : 'Show more';
  document.querySelector('.collapse .collapse-btn').innerHTML = btnContent;
});
</script>  
  `;
  