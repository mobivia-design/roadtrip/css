export default {
  title: 'Navigation/Tabs',
  parameters: {
    layout: 'fullscreen',
  },
}

export const Default = () => `
<ul class="nav nav-tabs">
  <li class="nav-item active">
    <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
</ul>

<ul class="nav nav-tabs secondary">
  <li class="nav-item active">
    <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
</ul>
`;

export const Center = () => `
<ul class="nav nav-tabs justify-content-sm-center">
  <li class="nav-item active">
      <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item">
      <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
      <a class="nav-link">Item</a>
  </li>
</ul>

<ul class="nav nav-tabs justify-content-sm-center secondary">
  <li class="nav-item active">
      <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item">
      <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
      <a class="nav-link">Item</a>
  </li>
</ul>
`;

export const Expend = () => `
<ul class="nav nav-tabs">
  <li class="nav-item active col">
      <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item col">
      <a class="nav-link">Item</a>
  </li>
  <li class="nav-item col">
      <a class="nav-link">Item</a>
  </li>
</ul>

<ul class="nav nav-tabs secondary">
  <li class="nav-item active col">
      <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item col">
      <a class="nav-link">Item</a>
  </li>
  <li class="nav-item col">
      <a class="nav-link">Item</a>
  </li>
</ul>

`;

export const leadingIcon = () => `
<ul class="nav nav-tabs">
  <li class="nav-item active">
    <a class="nav-link">
    <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item active
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
</ul>

<ul class="nav nav-tabs secondary">
  <li class="nav-item active">
    <a class="nav-link">
    <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item active
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
</ul>
`;

export const topIcon = () => `

<ul class="nav nav-tabs tab-layout-icon-top">
  <li class="nav-item active">
    <a class="nav-link">
    <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item active
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
</ul>

<ul class="nav nav-tabs tab-layout-icon-top secondary">
  <li class="nav-item active">
    <a class="nav-link">
    <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item active
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link">
      <svg xmlns="http://www.w3.org/2000/svg" class="nav-icon" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
      Item
    </a>
  </li>
</ul>
`;

export const Disabled = () => `
<ul class="nav nav-tabs">
  <li class="nav-item active">
    <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item tab-disabled">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
</ul>

<ul class="nav nav-tabs secondary">
  <li class="nav-item active">
    <a class="nav-link">Item active</a>
  </li>
  <li class="nav-item tab-disabled">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
  <li class="nav-item">
    <a class="nav-link">Item</a>
  </li>
</ul>
`;