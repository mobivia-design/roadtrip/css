export default {
  title: 'Indicators/Alert'
}

export const Info = () => `
<div class="alert alert-info" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-info"/>
  </svg>
  <div>
    <p class="alert-description">Information
      <a href="#" class="alert-link link link-default">Voir les produits similaires</a>
    </p>
  </div>
</div>

<br>

<div class="alert alert-info" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-info"/>
  </svg>
  <div>
    <p class="alert-description">Information</p>
    <button class="btn btn-sm">
      Label
    </button>
  </div>
</div>

<br>

<div class="alert alert-info" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-info"/>
  </svg>
  <div>
    <p class="alert-description">Information
    </p>
  </div>
</div>
`;

export const Success = () => `
<div class="alert alert-success" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-success"/>
  </svg>
  <div>
  <p class="alert-description">Success
    <a href="#" class="alert-link link link-default">Voir les produits similaires</a>
  </p>
  </div>
</div>

<br>

<div class="alert alert-success" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-success"/>
  </svg>
  <div>
    <p class="alert-description">Success</p>
    <button class="btn btn-sm">
      Label
    </button>
  </div>
</div>
`;

export const Warning = () => `
<div class="alert alert-warning" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-warning"/>
  </svg>
  <div>
  <p class="alert-description">Warning
    <a href="#" class="alert-link link link-default">Voir les produits similaires</a>
  </p>
  </div>
</div>

<br>

<div class="alert alert-warning" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-warning"/>
  </svg>
  <div>
    <p class="alert-description">Warning</p>
    <button class="btn btn-sm">
      Label
    </button>
  </div>
</div>
`;

export const Danger = () => `
<div class="alert alert-danger" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-danger"/>
  </svg>
  <div>
  <p class="alert-description">Danger
    <a href="#" class="alert-link link link-default">Voir les produits similaires</a>
  </p>
  </div>
</div>

<br>

<div class="alert alert-danger" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#alert-danger"/>
  </svg>
  <div>
    <p class="alert-description">Danger</p>
    <button class="btn btn-sm">
      Label
    </button>
  </div>
</div>
`;

export const Trick = () => `
<div class="alert alert-warning" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#trick"/>
  </svg>
  <div>
    <span class="alert-title d-block">Title</span>
    <p class="alert-description">Information</p>
  </div>
</div>

<br>

<div class="alert alert-warning" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" class="alert-icon" viewBox="0 0 64 64">
    <use xlink:href="#trick"/>
  </svg>
  <div>
    <p class="alert-description">Trick</p>
    <button class="btn btn-sm">
      Label
    </button>
  </div>
</div>
`;