export default {
    title: 'Utilities/Sizing',
  };
  
  export const Exemples = () => `

  <div class="w-full"
    style="margin: .25rem;background-color: var(--road-warning-surface-inverse);height:1rem"></div>

  <div class="w-screen"
    style="margin: .25rem;background-color: var(--road-grey-90);height:1rem"></div>
  
  <div class="w-auto"
    style="margin: .25rem;background-color: var(--road-danger-surface-inverse);height:1rem"></div>
  
  <div class="h-full"
    style="margin: .25rem;background-color: var(--road-success-surface-inverse);width:1rem;min-height:1rem"></div>

  <div class="h-auto"
    style="margin: .25rem;background-color: var(--road-info-surface-inverse);width:1rem;min-height:1rem"></div>
    
    <div class="h-screen"
    style="margin: .25rem;background-color: var(--road-info-surface-inverse);width:1rem;min-height:1rem"></div>
  `;

  