export default {
  title: 'Forms/Input group'
}

export const Playground = () => `
<div class="input-group input-xl">
  <input type="text" id="cardNumber" class="form-control form-control-xl">
  <div class="input-group-append">
    <label class="input-group-text" for="cardNumber">
      <svg xmlns="http://www.w3.org/2000/svg" class="input-icon icon-gray" viewBox="0 0 64 64"><use href="#payment-card"/></svg>
    </label>
  </div>
  <label class="form-label" for="cardNumber">Card Number</label>
</div>
`;

export const Button = () => `
<div class="input-group input-xl">
  <input type="text" class="form-control form-control-xl" id="newsletter" required>
  <div class="input-group-append">
    <button class="btn btn-xl btn-ghost" type="button">OK</button>
  </div>
  <label class="form-label" for="newsletter">Newsletter</label>
</div>

<div class="input-group input-xl">
  <input type="text" class="form-control form-control-xl" id="fieldWithHelp" required>
  <div class="input-group-append">
    <button class="btn btn-xl btn-outline-default" type="button" aria-label="search button">
      <svg xmlns="http://www.w3.org/2000/svg" class="input-icon" viewBox="0 0 64 64"><use href="#alert-info-outline"/></svg>
    </button>
  </div>
  <label class="form-label" for="fieldWithHelp">Label</label>
</div>
`;

export const Error = () => `
<div class="input-group input-xl was-validated">
  <input type="text" id="inputGroupError" class="form-control form-control-xl" required>
  <div class="input-group-append">
    <label class="input-group-text" for="inputGroupError">
      <svg xmlns="http://www.w3.org/2000/svg" class="input-icon icon-gray" viewBox="0 0 64 64"><use href="#payment-card"/></svg>
    </label>
  </div>
  <label class="form-label" for="inputGroupError">Label</label>
  <p class="invalid-feedback">error message here</p>
</div>
`;

export const Password = () => `



<div class="input-group input-xl">
  <input type="password" class="form-control form-control-xl" id="password" required>
  <div class="input-group-append">
    <button class="btn btn-xl btn-outline-default" type="button" aria-label="search button">
      <svg xmlns="http://www.w3.org/2000/svg" class="input-icon" viewBox="0 0 64 64"><use href="#visibility-outline" class="toggle-icon"/></svg>
    </button>
  </div>
  <label class="form-label" for="fieldWithHelp">Label</label>
</div>
<div class="checklist-password mt-16">
  <span class="checklist-password-label">Low Safety</span>
  <div class="progress mt-8 mb-16">
    <div class="progress-bar bg-danger" role="progressbar" aria-label="label of the progress" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <p class="text-medium mb-8">For optimal safety your password must have at least :</p>
  <ul class="m-0 pl-16">
    <li id="letter" class="invalid mb-8">1 lowercase and 1 uppercase</li>
    <li id="number" class="invalid mb-8">1 digit</li>
    <li id="length" class="invalid">8 characters minimum</li>
  </ul>
</div>

<script>

document.addEventListener("DOMContentLoaded", function () {
  var myInput = document.getElementById('password');
  var letter = document.getElementById('letter');
  var number = document.getElementById('number');
  var length = document.getElementById('length');
  var checklist = document.querySelector('.checklist-password');
  var widthProgress = 0;

  // When the user clicks on the password field, show the message box
  myInput.onfocus = function() {
    checklist.style.display = 'block';
  }

  // When the user clicks outside of the password field, hide the message box
  myInput.onblur = function() {
    checklist.style.display = 'none';
  }

  // When the user starts to type something inside the password field
  myInput.onkeyup = function() {
    // Validate lowercase and capital letters
    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    if(myInput.value.match(lowerCaseLetters) && myInput.value.match(upperCaseLetters)) {  
      letter.classList.remove('invalid');
      letter.classList.add('valid');
    } else {
      letter.classList.remove('valid');
      letter.classList.add('invalid');
    }

    // Validate numbers
    var numbers = /[0-9]/g;
    if(myInput.value.match(numbers)) {  
      number.classList.remove('invalid');
      number.classList.add('valid');
      } else {
      number.classList.remove('valid');
      number.classList.add('invalid');
    }
    
    // Validate length
    if(myInput.value.length >= 8) {
      length.classList.remove('invalid');
      length.classList.add('valid');
    } else {
      length.classList.remove('valid');
      length.classList.add('invalid');
    }
    var letterValid = myInput.value.match(lowerCaseLetters) && myInput.value.match(upperCaseLetters);
    var numbersValid = myInput.value.match(numbers);
    var lengthValid = myInput.value.length >= 8;


    if(letterValid || numbersValid || lengthValid){
      widthProgress = 33;
      document.querySelector('.progress-bar').style.width = widthProgress + '%';
      document.querySelector('.progress-bar').classList.add('bg-danger');
      document.querySelector('.checklist-password-label').textContent = "Low safety";

    } 
    if(letterValid && numbersValid){
      if(widthProgress >= 33){
        widthProgress = widthProgress + 33;
      }
      else{
        widthProgress = 33;
      }
      document.querySelector('.progress-bar').style.width = widthProgress + '%';
      document.querySelector('.progress-bar').classList.replace('bg-danger', 'bg-warning');
      document.querySelector('.checklist-password-label').textContent = "Medium safety";

    } 
    if(letterValid && lengthValid){
      if(widthProgress >= 33){
        widthProgress = widthProgress + 33;
      }
      else{
        widthProgress = 33;
      }
      document.querySelector('.progress-bar').style.width = widthProgress + '%';
      document.querySelector('.progress-bar').classList.replace('bg-danger', 'bg-warning');
      document.querySelector('.checklist-password-label').textContent = "Medium safety";
    }
    if(numbersValid && lengthValid){
      if(widthProgress >= 33){
        widthProgress = widthProgress + 33;
      }
      else{
        widthProgress = 33;
      }
      document.querySelector('.progress-bar').style.width = widthProgress + '%';
      document.querySelector('.progress-bar').classList.replace('bg-danger', 'bg-warning');
      document.querySelector('.checklist-password-label').textContent = "Medium safety";
    }
    if(letterValid && numbersValid && lengthValid){
      widthProgress = 100;
      document.querySelector('.progress-bar').style.width = widthProgress + '%';
      document.querySelector('.progress-bar').classList.replace('bg-warning', 'bg-success');
      document.querySelector('.checklist-password-label').textContent = "Optimal safety";
    } 
  }

  var togglePassword = document.querySelector('.input-icon');
  var toggleIcon = document.querySelector('.toggle-icon');
  var password = document.querySelector('#password');



  // When the user clicks on the icon, show the password and change icon

  document.querySelector('.input-icon').onfocus = function() {
    if (password.type === "password") {
      password.type = "text";
    } else {
      password.type = "password";
    }
    if (toggleIcon.getAttribute("href") === "#visibility-outline") {
      toggleIcon.setAttribute("href", "#visibility-off-outline");
    } else {
      toggleIcon.setAttribute("href", "#visibility-outline");
    }
  }






});

</script>
`;



