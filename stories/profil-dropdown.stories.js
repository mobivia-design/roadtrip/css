export default {
    title: 'Expand/Profil Dropdown',
    parameters: {
      docs: {
        inlineStories: false,
        iframeHeight: '420px',
      },
    },
  }
  
  export const Playground = () => `
  <details class="dropdown profil-dropdown" open>
    <summary>
    <div class="d-flex">
      <div class="dropdown-button mb-0 d-flex align-items-center justify-center">
        <span class="avatar avatar-md">
          A
        </span>
      </div>
    </div>
    </summary>
    <div class="dropdown-menu">
      <div class="dropdown-item">
        <div class="profil-item d-flex pb-16 pt-8 mb-8">
          <span class="avatar avatar-sm">
            A
          </span>
          <div class="d-flex flex-column ml-16">
            <span class="font-weight-bold">First and Last name</span>
            <span>Email</span>
          </div>
        </div>
      </div>
      <button class="dropdown-item" value="fr-FR">
        <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
          <use xlink:href="#edit-outline"/>
        </svg>
        Edit profile
      </button>
      <button class="dropdown-item" value="fr-BE">
        <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
          <use xlink:href="#location-navigation-outline"/>
        </svg>
        Log out
      </button>
    </div>
  </details>
  `;
  
  
  

  
