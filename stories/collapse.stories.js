export default {
  title: 'Expand/Collapse',
};

export const Playground = () => `
<div class="collapse">
  <ul class="m-0">
    <li>Car tire 205/55 R16 91 V</li>
    <li>Good traction on dry and wet soils.</li>
    <li>Reduced risk of aquaplaning.</li>
    <li>Reduced braking distances.</li>
  </ul>
  <div class="collapsed-content">
    <ul class="m-0">
      <li>Specific Homologation : <b>non</b></li>
      <li>XL : <b>non</b></li>
      <li>Runflat : <b>non</b></li>
      <li>Seal : <b>non</b></li>
      <li>Various tire characteristics : <b>MFS</b></li>
      <li>Tire profile : <b>Energy saver</b></li>
    </ul>
  </div>
  <div class="collapse-btn-wrapper">
    <button class="collapse-btn btn btn-link">Show more</button>
  </div>
</div>

<script>
document.querySelector('.collapse .collapse-btn').addEventListener('click', () => {
  document.querySelector('.collapse .collapsed-content').classList.toggle('collapse-open');
  let btnContent = document.querySelector('.collapse .collapsed-content').classList.contains('collapse-open') ? 'Show less' : 'Show more';
  document.querySelector('.collapse .collapse-btn').innerHTML = btnContent;
});
</script>
`;

export const Closed = () => `
<div class="collapse">
  <ul class="m-0">
    <li>Car tire 205/55 R16 91 V</li>
    <li>Good traction on dry and wet soils.</li>
    <li>Reduced risk of aquaplaning.</li>
    <li>Reduced braking distances.</li>
  </ul>
  <div class="collapsed-content">
    <ul class="m-0">
      <li>Specific Homologation : <b>non</b></li>
      <li>XL : <b>non</b></li>
      <li>Runflat : <b>non</b></li>
      <li>Seal : <b>non</b></li>
      <li>Various tire characteristics : <b>MFS</b></li>
      <li>Tire profile : <b>Energy saver</b></li>
    </ul>
  </div>
  <div class="collapse-btn-wrapper">
    <button class="collapse-btn btn btn-link">Show more</button>
  </div>
</div>

<script>
document.querySelector('.collapse .collapse-btn').addEventListener('click', () => {
  document.querySelector('.collapse .collapsed-content').classList.toggle('collapse-open');
  let btnContent = document.querySelector('.collapse .collapsed-content').classList.contains('collapse-open') ? 'Show less' : 'Show more';
  document.querySelector('.collapse .collapse-btn').innerHTML = btnContent;
});
</script>
`;

export const Open = () => `
<div class="collapse">
  <ul class="m-0">
    <li>Car tire 205/55 R16 91 V</li>
    <li>Good traction on dry and wet soils.</li>
    <li>Reduced risk of aquaplaning.</li>
    <li>Reduced braking distances.</li>
  </ul>
  <div class="collapsed-content collapse-open">
    <ul class="m-0">
      <li>Specific Homologation : <b>non</b></li>
      <li>XL : <b>non</b></li>
      <li>Runflat : <b>non</b></li>
      <li>Seal : <b>non</b></li>
      <li>Various tire characteristics : <b>MFS</b></li>
      <li>Tire profile : <b>Energy saver</b></li>
    </ul>
  </div>
  <div class="collapse-btn-wrapper">
    <button class="collapse-btn btn btn-link">Show more</button>
  </div>
</div>

<script>
document.querySelector('.collapse .collapse-btn').addEventListener('click', () => {
  document.querySelector('.collapse .collapsed-content').classList.toggle('collapse-open');
  let btnContent = document.querySelector('.collapse .collapsed-content').classList.contains('collapse-open') ? 'Show less' : 'Show more';
  document.querySelector('.collapse .collapse-btn').innerHTML = btnContent;
});
</script>
`;
