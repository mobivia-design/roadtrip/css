export default {
  title: 'Forms/Input',
  parameters: {
    docs: {
      description: {
        component: 'If the input is not **`required`** , add a class **`.has-value`**  when a value is set to handle Label position.'
      }
    }
  }
}

export const Playground = () => `
<div class="form-group">
  <input class="form-control form-control-xl" type="text" id="inputClassic" required/>
  <label class="form-label" for="inputClassic">Label</label>
</div>
`;

export const WithValue = () => `
<div class="form-group">
  <input class="form-control form-control-xl" type="text" id="inputwithValue" value="Value" required/>
  <label class="form-label" for="inputwithValue">Label</label>
</div>
`;

export const Date = () => `
<div class="form-group">
  <input class="form-control form-control-xl" type="date" min="2021-01-01" max="2021-12-31" value="2021-04-21" id="inputClassic" required/>
  <label class="form-label" for="inputClassic">Date</label>
</div>
`;

export const Hour = () => `
<div class="form-group">
  <input class="form-control form-control-xl" type="time" value="13:30" id="inputClassic" required/>
  <label class="form-label" for="inputClassic">Choose an hour</label>
</div>
`;

export const Disabled = () => `
<div class="form-group">
  <input class="form-control form-control-xl" type="text" id="inputDisabled" disabled/>
  <label class="form-label" for="inputDisabled">Label</label>
</div>
`;

export const Error = () => `
<div class="form-group was-validated">
  <input class="form-control form-control-xl" type="text" id="inputError" required/>
  <label class="form-label" for="inputError">Label</label>
  <p class="invalid-feedback">This field is required</p>
</div>
`;