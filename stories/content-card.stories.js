export default {
  title: 'Listing/ContentCard',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
};

export const Image = () => `
<div class="container-fluid">
  <div class="row">
  <div class="col">
    <div class="content-card">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" width="auto" height="auto" class="p-16 pb-0">
      <div class="content-card-description">
        <p class="content-card-description-title">Lorem ipsum</p>
        <p class="content-card-description-text">Elevate your car's performance with Norauto's all-in-one maintenance kit.</p>
        <div class="content-card-description-buttons">
          <button class="btn btn-primary w-full mt-8 mb-0">
            Add to cart
          </button>
        </div>
      </div>
    </div>
  </div>

    <div class="col">
      <div class="content-card">
        <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" width="auto" height="auto" class="p-16 pb-0">
        <div class="content-card-description">
          <p class="content-card-description-title">Lorem ipsum</p>
          <p class="content-card-description-text">Elevate your car's performance with Norauto's all-in-one maintenance kit.</p>
          <div class="content-card-description-buttons">
            <button class="btn btn-primary w-full mt-8 mb-0">
              Add to cart
            </button>
          </div>
         </div>
      </div>
    </div>
     
   
  </div>
</div>
`;


export const InsetImage = () => `
<div class="container-fluid">
  <div class="row">
  <div class="col">
    <div class="content-card">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" width="auto" height="auto">
      <div class="content-card-description">
        <p class="content-card-description-title">Lorem ipsum</p>
        <p class="content-card-description-text">Elevate your car's performance with Norauto's all-in-one maintenance kit.</p>
        <div class="content-card-description-buttons">
          <button class="btn btn-primary w-full mt-8 mb-0">
            Add to cart
          </button>
        </div>
      </div>
    </div>
  </div>

    <div class="col">
      <div class="content-card">
        <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" width="auto" height="auto" class="p-16 pb-0">
        <div class="content-card-description">
          <p class="content-card-description-title">Lorem ipsum</p>
          <p class="content-card-description-text">Elevate your car's performance with Norauto's all-in-one maintenance kit.</p>
          <div class="content-card-description-buttons">
            <button class="btn btn-primary w-full mt-8 mb-0">
              Add to cart
            </button>
          </div>
         </div>
      </div>
    </div>
     
   
  </div>
</div>
`;

export const SecondaryAction = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <div class="content-card">
        <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" width="auto" height="auto" class="p-16 pb-0">
        <div class="content-card-description">
          <p class="content-card-description-title">Lorem ipsum</p>
          <p class="content-card-description-text">Elevate your car's performance with Norauto's all-in-one maintenance kit.</p>
          <div class="content-card-description-buttons">
            <button class="btn btn-outline-primary w-full mt-8 mb-0">
              Label
            </button>
            <button class="btn btn-primary w-full mt-8 mb-0">
              Add to cart
            </button>
          </div>
         </div>
      </div>
    </div>
    <div class="col">
      <div class="content-card">
        <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" width="auto" height="auto" class="p-16 pb-0">
        <div class="content-card-description">
          <p class="content-card-description-title">Lorem ipsum</p>
          <p class="content-card-description-text">Elevate your car's performance with Norauto's all-in-one maintenance kit.</p>
          <div class="content-card-description-buttons">
            <button class="btn btn-outline-primary w-full mt-8 mb-0">
              Label
            </button>
            <button class="btn btn-primary w-full mt-8 mb-0">
              Add to cart
            </button>
          </div>
         </div>
      </div>
    </div>
     
   
  </div>
</div>
`;