export default {
  title: 'Forms/Toggle'
}

export const Default = () => `
<div class="form-toggle">
  <input class="form-toggle-input" type="checkbox" id="toggleDefault">
  <label class="form-toggle-label" for="toggleDefault">
    <div class="form-toggle-lever" data-off="NO" data-on="YES"></div>
    Label
  </label>
</div>
`;

export const Checked = () => `
<div class="form-toggle">
  <input class="form-toggle-input" type="checkbox" id="toggleChecked" checked>
  <label class="form-toggle-label" for="toggleChecked">
    <div class="form-toggle-lever" data-off="NO" data-on="YES"></div>
    Label
  </label>
</div>
`;

export const Disabled = () => `
<div class="form-toggle">
  <input class="form-toggle-input" type="checkbox" id="toggleDefault">
  <label class="form-toggle-label disabled" for="toggleDefault">
    <div class="form-toggle-lever" data-off="NO" data-on="YES"></div>
    Label
  </label>
</div>
`;

export const Position = () => `
<div class="form-toggle">
  <input class="form-toggle-input" type="checkbox" id="toggleClassicLeft">
  <label class="form-toggle-label" for="toggleClassicLeft">
    <div class="form-toggle-lever" data-off="NO" data-on="YES"></div>
    Label
  </label>
</div>

<div class="form-toggle form-toggle-right">
  <input class="form-toggle-input" type="checkbox" id="toggleClassicRight">
  <label class="form-toggle-label" for="toggleClassicRight">
    Label
    <div class="form-toggle-lever" data-off="NO" data-on="YES"></div>
  </label>
</div>

<div class="form-toggle form-toggle-right">
  <input class="form-toggle-input" type="checkbox" id="toggleClassicAround">
  <label class="form-toggle-label form-toggle-spaced" for="toggleClassicAround">
    Label
    <div class="form-toggle-lever" data-off="NO" data-on="YES"></div>
  </label>
</div>
`;
