export default {
  title: 'Indicators/Badge'
}

export const Default = () => `
<a class="text-content d-inline-flex">
  <svg xmlns="http://www.w3.org/2000/svg" class="icon-lg icon-gray" viewBox="0 0 64 64">
    <use xlink:href="#alert-notification-outline"/>
  </svg>
  <sup class="badge-sup mr-8" style="margin-left: -1.125rem;margin-top: 0.25rem;">
    <span class="badge badge-accent">1</span>
  </sup>
</a>
`;

export const Notification = () => `
<a class="text-content d-inline-flex">
  <svg xmlns="http://www.w3.org/2000/svg" class="icon-lg icon-gray" viewBox="0 0 64 64">
    <use xlink:href="#alert-notification-outline"/>
  </svg>
  <sup class="badge-sup" style="margin-left: -1.125rem;margin-top: 0.25rem;">
    <span class="badge badge-accent">10</span>
  </sup>
</a>
`;

export const Statut = () => `
<a class="text-content d-inline-flex">
  <svg xmlns="http://www.w3.org/2000/svg" class="icon-lg icon-gray" viewBox="0 0 64 64">
    <use xlink:href="#people-outline"/>
  </svg>
  <sup class="badge-sup" style="margin-left: -0.25rem;margin-top: 0.375rem;">
    <span class="badge badge-accent"></span>
  </sup>
</a>
`;
