export default {
  title: 'Listing/Carousel',
  parameters: {
    docs: {
      description: {
        component: 'The carousel is based on [Swiper](https://swiperjs.com/) and is customizable by using the [configuration options](https://swiperjs.com/swiper-api#parameters).'
      },
    },
  },
}

export const Default = () => `
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>

<div class="carousel swiper-container">
  <div class="swiper-wrapper">
    <div class="swiper-slide swiper-zoom-container">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/FR-SLIDE-OP-Pneu-1er-prix-Fev24.png" alt="roof bar diagram"/>
    </div>

    <div class="swiper-slide swiper-zoom-container">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/FR-SLIDE-OP-P1-24-climv2.jpg" alt="roof bar on car"/>
    </div>

    <div class="swiper-slide swiper-zoom-container">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/FR-SLIDE-OP-Pneu-1er-prix-Fev24.png" alt="roof bar diagram mounting first step"/>
    </div>

    <div class="swiper-slide swiper-zoom-container">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/FR-SLIDE-OP-P1-24-climv2.jpg" alt="roof bar diagram mounting second step"/>
    </div>

    <div class="swiper-slide swiper-zoom-container">
      <img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/FR-SLIDE-OP-Pneu-1er-prix-Fev24.png" alt="roof bar example"/>
    </div>
  </div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-prev">
    <svg class="swiper-icon"><use href="#navigation-chevron"></svg>
  </div>
  <div class="swiper-button-next">
    <svg class="swiper-icon"><use href="#navigation-chevron"></svg>
  </div>
</div>

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<!-- Initialize Swiper -->
<script>
  var swiper = new Swiper(".carousel", {
    pagination: {
      el: ".swiper-pagination",
    },
    effect: undefined,
    direction: 'horizontal',
    initialSlide: 0,
    loop: false,
    parallax: false,
    slidesPerView: 1,
    spaceBetween: 0,
    speed: 300,
    grid: {
      rows: 1,
      fill: 'column',
    },
    slidesPerGroup: 1,
    centeredSlides: false,
    slidesOffsetBefore: 0,
    slidesOffsetAfter: 0,
    touchEventsTarget: 'container',
    autoplay: false,
    freeMode: {
      enabled: false,
      momentum: true,
      momentumRatio: 1,
      momentumBounce: true,
      momentumBounceRatio: 1,
      momentumVelocityRatio: 1,
      sticky: false,
      minimumVelocity: 0.02,
    },
    autoHeight: false,
    setWrapperSize: false,
    zoom: {
      maxRatio: 3,
      minRatio: 1,
      toggle: false,
    },
    touchRatio: 1,
    touchAngle: 45,
    simulateTouch: true,
    touchStartPreventDefault: false,
    shortSwipes: true,
    longSwipes: true,
    longSwipesRatio: 0.5,
    longSwipesMs: 300,
    followFinger: true,
    threshold: 0,
    touchMoveStopPropagation: true,
    touchReleaseOnEdges: false,
    resistance: true,
    resistanceRatio: 0.85,
    watchSlidesProgress: false,
    preventClicks: true,
    preventClicksPropagation: true,
    slideToClickedSlide: false,
    loopAdditionalSlides: 0,
    noSwiping: true,
    runCallbacksOnInit: true,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
    flipEffect: {
      slideShadows: true,
      limitRotation: true,
    },
    cubeEffect: {
      slideShadows: true,
      shadow: true,
      shadowOffset: 20,
      shadowScale: 0.94,
    },
    fadeEffect: {
      crossFade: false,
    },
    a11y: {
      prevSlideMessage: 'Previous slide',
      nextSlideMessage: 'Next slide',
      firstSlideMessage: 'This is the first slide',
      lastSlideMessage: 'This is the last slide',
    },
  });
</script>
`;