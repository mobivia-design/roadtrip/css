export default {
    title: 'Utilities/Position',
  };
  
  export const Exemples = () => `

  <div class="position-fixed" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-warning-surface-inverse);"></div>

  <div class="position-relative" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-90);"></div>
  
  <div class="position-absolute" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-danger-surface-inverse);right:0"></div>
  
  <div class="position-sticky" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-success-surface-inverse);"></div>
  
  <div class="position-static" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-info-surface-inverse);"></div>
  `;