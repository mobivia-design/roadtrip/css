export default {
  title: 'Navigation/Pagination',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
}

export const Default = () => `
<nav aria-label="Page navigation example">
  <ul class="pagination">
      <li class="page-item">
        <a class="page-btn disabled" tabindex="-1" aria-disabled="true">
          <svg xmlns="http://www.w3.org/2000/svg" class="page-icon page-icon-prev" viewBox="0 0 64 64"><use xlink:href="#navigation-chevron"/></svg>
        </a>
      </li>
      <li class="page-item active"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item"><a class="page-link" href="#">4</a></li>
      <li class="page-item"><a class="page-link" href="#">5</a></li>
      <li class="page-item">
        <a class="page-btn" aria-label="Next" href="#">
          <svg xmlns="http://www.w3.org/2000/svg" class="page-icon page-icon-next" viewBox="0 0 64 64"><use xlink:href="#navigation-chevron"/></svg>
        </a>
      </li>
  </ul>
</nav>
`;
