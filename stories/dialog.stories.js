export default {
  title: 'Modals/Dialog',
  parameters: {
    docs: {
      inlineStories: false,
      iframeHeight: '400px',
    },
  },
}

export const Playground = () => `
<div class="dialog dialog-open" tabindex="-1" role="dialog" aria-label="label of the dialog">
  <div class="dialog-modal" role="document" tabindex="0">
    <div class="dialog-content">
      <header class="dialog-header">
        <button type="button" class="dialog-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <section class="dialog-body">
        <h2 class="dialog-title">Dialog title</h2>
        <p class="dialog-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
      </section>
      <footer class="dialog-footer">
        <button type="button" class="btn btn-secondary btn-block mb-0">Done</button>
      </footer>
    </div>
  </div>
</div>
`;

export const sideActions = () => `
<div class="dialog dialog-open" tabindex="-1" role="dialog" aria-label="label of the dialog">
  <div class="dialog-modal" role="document" tabindex="0">
    <div class="dialog-content">
      <header class="dialog-header">
        <button type="button" class="dialog-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <section class="dialog-body">
        <h2 class="dialog-title">Dialog title</h2>
        <p class="dialog-description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
      </section>
      <footer class="dialog-footer">
        <button type="button" class="btn btn-outline-default btn-block mb-0" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary ml-16 btn-block mb-0">Done</button>
      </footer>
    </div>
  </div>
</div>
`;

export const Info = () => `
<div class="dialog dialog-open" tabindex="-1" role="dialog" aria-label="label of the dialog">
  <div class="dialog-modal" role="document" tabindex="0">
    <div class="dialog-content">
      <header class="dialog-header">
        <button type="button" class="dialog-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="dialog-body">
        <svg xmlns="http://www.w3.org/2000/svg" class="dialog-icon icon" fill="var(--road-info-icon)" viewBox="0 0 64 64"><use xlink:href="#alert-info-outline"/></svg>
        <h2 class="dialog-title">Information</h2>
        <p class="dialog-description mb-8">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
      </div>
    </div>
  </div>
</div>
`;

export const Success = () => `
<div class="dialog dialog-open" tabindex="-1" role="dialog" aria-label="label of the dialog">
  <div class="dialog-modal" role="document" tabindex="0">
    <div class="dialog-content">
      <header class="dialog-header">
        <button type="button" class="dialog-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="dialog-body">
        <svg xmlns="http://www.w3.org/2000/svg" class="dialog-icon icon" fill="var(--road-success-icon)" viewBox="0 0 64 64"><use xlink:href="#alert-success-outline"/></svg>
        <h2 class="dialog-title">Success</h2>
        <p class="dialog-description mb-8">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
      </div>
    </div>
  </div>
</div>
`;

export const Warning = () => `
<div class="dialog dialog-open" tabindex="-1" role="dialog" aria-label="label of the dialog">
  <div class="dialog-modal" role="document" tabindex="0">
    <div class="dialog-content">
      <header class="dialog-header">
        <button type="button" class="dialog-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="dialog-body">
        <svg xmlns="http://www.w3.org/2000/svg" class="dialog-icon icon" fill="var(--road-warning-icon)" viewBox="0 0 64 64"><use xlink:href="#alert-warning-outline"/></svg>
        <h2 class="dialog-title">Warning</h2>
        <p class="dialog-description mb-8">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
      </div>
    </div>
  </div>
</div>
`;

export const Danger = () => `
<div class="dialog dialog-open" tabindex="-1" role="dialog" aria-label="label of the dialog">
  <div class="dialog-modal" role="document" tabindex="0">
    <div class="dialog-content">
      <header class="dialog-header">
        <button type="button" class="dialog-close" data-dismiss="modal" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="dialog-body">
        <svg xmlns="http://www.w3.org/2000/svg" class="dialog-icon icon" fill="var(--road-danger-icon)" viewBox="0 0 64 64"><use xlink:href="#alert-danger-outline"/></svg>
        <h2 class="dialog-title">Danger</h2>
        <p class="dialog-description mb-8">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
      </div>
    </div>
  </div>
</div>
`;
