export default {
  title: 'Forms/Select'
}

export const Default = () => `
<div class="form-group">
  <select class="form-select form-select-xl" id="selectClassic" required/>
    <option selected disabled hidden style='display: none' value=''></option>
    <option value="value1">Value1</option>
    <option value="value2">Value2</option>
    <option value="value3">Value3</option>
  </select>
  <label class="form-select-label" for="selectClassic">Label</label>
</div>

<div class="form-group">
  <select class="form-select form-select-lg" id="selectClassic" required/>
    <option selected disabled hidden style='display: none' value=''></option>
    <option value="value1">Value1</option>
    <option value="value2">Value2</option>
    <option value="value3">Value3</option>
  </select>
  <label class="form-select-label" for="selectClassic">Label</label>
</div>

<div class="form-group">
  <select class="form-select form-select-md" id="selectClassic" required/>
    <option selected disabled hidden style='display: none' value=''></option>
    <option value="value1">Value1</option>
    <option value="value2">Value2</option>
    <option value="value3">Value3</option>
  </select>
  <label class="form-select-label" for="selectClassic">Label</label>
</div>
`;

export const WithValue = () => `
<div class="form-group">
  <select class="form-select form-select-xl" id="selectWithValue" required/>
    <option selected disabled hidden style='display: none' value=''></option>
    <option value="value1" selected>Value1</option>
    <option value="value2">Value2</option>
    <option value="value3">Value3</option>
  </select>
  <label class="form-select-label" for="selectWithValue">Label</label>
</div>
`;

export const Disabled = () => `
<div class="form-group">
  <select class="form-select form-select-xl" id="selectDisabled" disabled/>
    <option selected disabled hidden style='display: none' value=''></option>
    <option value="value1">Value1</option>
    <option value="value2">Value2</option>
    <option value="value3">Value3</option>
  </select>
  <label class="form-select-label" for="selectDisabled">Label</label>
</div>
`;

export const Error = () => `
<div class="form-group was-validated">
  <select class="form-select form-select-xl" type="text" id="selectError" required/>
    <option selected disabled hidden style='display: none' value=''></option>
    <option value="value1">Value1</option>
    <option value="value2">Value2</option>
    <option value="value3">Value3</option>
  </select>
  <label class="form-select-label" for="selectError">Label</label>
  <p class="invalid-feedback">Please select an element in the list</p>
</div>
`;
