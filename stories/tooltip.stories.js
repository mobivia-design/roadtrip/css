export default {
  title: 'Indicators/Tooltip'
}
export const Default = () => `
<br><br><br><br><br><br><br><br>
<!-- Tooltip top postion -->
<div class="d-inline-block ml-24" tooltip="Fit & small tooltips" tooltip-position="top">
  <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" fill="var(--road-primary-500)" viewBox="0 0 64 64"><use xlink:href="#alert-question"/></svg>
</div>

<div class="d-inline-block ml-24 tooltip-text-left" tooltip="Fit & small tooltips" tooltip-position="top">
  <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" fill="var(--road-primary-500)" viewBox="0 0 64 64"><use xlink:href="#alert-question"/></svg>
</div>

<!-- Tooltip left postion -->
<div class="d-inline-block ml-24" tooltip="Fit & small tooltips" tooltip-position="left">
  <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" fill="var(--road-primary-500)" viewBox="0 0 64 64"><use xlink:href="#alert-question"/></svg>
</div>

<!-- Tooltip right postion -->
<div class="d-inline-block ml-24" tooltip="Fit & small tooltips" tooltip-position="right">
  <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" fill="var(--road-primary-500)" viewBox="0 0 64 64"><use xlink:href="#alert-question"/></svg>
</div>

<!-- Tooltip bottom postion -->
<div class="d-inline-block ml-24" tooltip="Fit & small tooltips" tooltip-position="bottom">
  <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" fill="var(--road-primary-500)" viewBox="0 0 64 64"><use xlink:href="#alert-question"/></svg>
</div>
`;
