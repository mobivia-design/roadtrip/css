export default {
  title: 'Forms/Chip',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
};

export const Playground = () => `
<button class="chip chip-default chip-outline">
  Label
  <svg xmlns="http://www.w3.org/2000/svg" class="chip-close" viewBox="0 0 64 64">
    <use xlink:href="#navigation-close"/>
  </svg>
</button>
`;

export const WithIcon = () => `
<button class="chip chip-default chip-outline">
  <svg class="chip-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
    <use xlink:href="#weather-rain"/>
  </svg>
  B
  <svg xmlns="http://www.w3.org/2000/svg" class="chip-close" viewBox="0 0 64 64">
    <use xlink:href="#navigation-close"/>
  </svg>
</button>
`;

export const Color = () => `
<button class="chip chip-secondary">
  Reset
</button>
`;

export const Size = () => `
<button class="chip chip-default chip-outline chip-lg">
  Revision
</button>
`;


export const Inverse = () => `
<button class="chip chip-outline chip-inverse">
  Label
  <svg xmlns="http://www.w3.org/2000/svg" class="chip-close" viewBox="0 0 64 64">
    <use xlink:href="#navigation-close"/>
  </svg>
</button>
`;