export default {
    title: 'Indicators/Avatar',
  };
  
  
  export const Default = () => `
<span class="avatar avatar-sm">
A
</span>

<span class="avatar avatar-md">
A
</span>

<span class="avatar avatar-lg">
A
</span>
  `;
  
  export const Image = () => `
<span class="avatar avatar-sm">
<img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar" title="Person name"/>
</span>

<span class="avatar avatar-md">
<img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar" title="Person name"/>
</span>

<span class="avatar avatar-lg">
<img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar" title="Person name"/>
</span>
  `;
  