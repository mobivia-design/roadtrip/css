export default {
    title: 'Navigation/Items per page',
    parameters: {
      backgrounds: {
        default: 'grey',
      },
      layout: 'fullscreen',
    },
  }
  
  export const Default = () => `
  <nav aria-label="Page item example">
    <ul class="items-per-page">
        <li class="items-per-page-label mr-16">Show Items</li>
        <li class="active"><a class="page-link" href="#">12</a></li>
        <li><a class="page-link mx-4" href="#">24</a></li>
        <li><a class="page-link" href="#">48</a></li>
    </ul>
  </nav>
  `;
  