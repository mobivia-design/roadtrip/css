export default {
  title: 'Forms/Counter'
}

export const Playground = () => `
<div class="counter input-group">
  <div class="input-group-prepend">
    <button type="button" class="btn btn-number mb-0" data-type="minus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-less"/>
      </svg>
    </button>
  </div>
  <input type="number" name="quant[1]" class="form-control input-number less-label font-weight-bold text-center" value="1" min="1" max="10" aria-label="product quantity">
  <div class="input-group-append">
    <button type="button" class="btn btn-number mb-0" data-type="plus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-more"/>
      </svg>
    </button>
  </div>
</div>
`;

export const Medium = () => `
<div class="counter input-group">
  <div class="input-group-prepend">
    <button type="button" class="btn btn-md btn-number mb-0" data-type="minus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-less"/>
      </svg>
    </button>
  </div>
  <input type="number" name="quant[1]" class="form-control form-control-md input-number less-label font-weight-bold text-center" value="1" min="1" max="10" aria-label="product quantity">
  <div class="input-group-append">
    <button type="button" class="btn btn-md btn-number mb-0" data-type="plus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-more"/>
      </svg>
    </button>
  </div>
</div>
`;

export const Small = () => `
<div class="counter input-group input-group-sm">
  <div class="input-group-prepend">
    <button type="button" class="btn btn-sm btn-number mb-0" data-type="minus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-less"/>
      </svg>
    </button>
  </div>
  <input type="number" name="quant[1]" class="form-control form-control-sm input-number less-label font-weight-bold text-center" value="1" min="1" max="10" aria-label="product quantity">
  <div class="input-group-append">
    <button type="button" class="btn btn-sm btn-number mb-0" data-type="plus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-more"/>
      </svg>
    </button>
  </div>
</div>
`;

export const IconVersion = () => `
<div class="counter input-group">
  <div class="input-group-prepend">
    <button type="button" class="btn btn-default btn-number mb-0" data-type="minus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#delete-forever"/>
      </svg>
    </button>
  </div>
  <input type="number" name="quant[1]" class="form-control input-number less-label font-weight-bold text-center" value="1" min="1" max="10" aria-label="product quantity">
  <div class="input-group-append">
    <button type="button" class="btn btn-default btn-number mb-0" data-type="minus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-more"/>
      </svg>
    </button>
  </div>
</div>
`;

export const Disabled = () => `
<div class="counter input-group">
  <div class="input-group-prepend">
    <button type="button" class="btn btn-md btn-number mb-0" data-type="minus" data-field="quant[1]" disabled>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-less"/>
      </svg>
    </button>
  </div>
  <input type="number" name="quant[1]" class="form-control form-control-md input-number less-label font-weight-bold text-center" value="1" min="1" max="10" aria-label="product quantity">
  <div class="input-group-append">
    <button type="button" class="btn btn-md btn-number mb-0" data-type="plus" data-field="quant[1]" disabled>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-more"/>
      </svg>
    </button>
  </div>
</div>
`;

export const ReadOnly = () => `
<div class="counter input-group">
  <div class="input-group-prepend">
    <button type="button" class="btn btn-md btn-number mb-0" data-type="minus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-less"/>
      </svg>
    </button>
  </div>
  <input type="number" name="quant[1]" readonly class="form-control form-control-md input-number less-label font-weight-bold text-center" value="1" min="1" max="10" aria-label="product quantity">
  <div class="input-group-append">
    <button type="button" class="btn btn-md btn-number mb-0" data-type="plus" data-field="quant[1]">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-add-more"/>
      </svg>
    </button>
  </div>
</div>
`;
