export default {
    title: 'CMS/PictoParagraphs'
  }


  export const Paragraph = () => `

<style>
.pictoParagraphs-list{
    margin-bottom: 2rem;
}
@media (min-width: 768px) {
    .pictoParagraphs-list{
        flex-direction: column;
        align-items:center;
    }
}

.pictoParagraphs-list-icon{
    width: 64px;
    height: 64px;
    background-color: var(--background);
    border-radius: 50%;
    flex:none;
    margin-right: 1rem
}
@media (min-width: 768px) {
    .pictoParagraphs-list-icon{
        margin-right: 0;
        margin-bottom: 1rem
    }
}

.pictoParagraphs-list-icon svg{
    width: 36px;
    height: 36px;
}

.pictoParagraphs-list-title{
    font-size: 1rem
}
@media (min-width: 768px) {
    .pictoParagraphs-list-title{
        text-align: center;
    }
}

@media (min-width: 768px) {
    .pictoParagraphs-list-text{
        text-align: center;
    }
}
</style>

<h2 class="title-underline text-center mb-24">Section title</h2>
<div class="pictoParagraphs">
    <ul class="pl-0 row no-gutters">
        <li class="pictoParagraphs-list d-flex col-md-6 col-xl-4">
            <span class="pictoParagraphs-list-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#battery"/>
                </svg>
            </span>
            <div>
                <p class="mb-4 pictoParagraphs-list-title font-weight-bold mt-0">test</p>
                <p class="pictoParagraphs-list-text mt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
        <li class="pictoParagraphs-list d-flex col-md-6 col-xl-4">
            <span class="pictoParagraphs-list-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#battery"/>
                </svg>
            </span>
            <div>
                <p class="mb-4 pictoParagraphs-list-title font-weight-bold mt-0">test</p>
                <p class="pictoParagraphs-list-text mt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
        <li class="pictoParagraphs-list d-flex col-md-6 col-xl-4">
            <span class="pictoParagraphs-list-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#battery"/>
                </svg>
            </span>
            <div>
                <p class="mb-4 pictoParagraphs-list-title font-weight-bold mt-0">test</p>
                <p class="pictoParagraphs-list-text mt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
        <li class="pictoParagraphs-list d-flex col-md-6 col-xl-4">
            <span class="pictoParagraphs-list-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#battery"/>
                </svg>
            </span>
            <div>
                <p class="mb-4 pictoParagraphs-list-title font-weight-bold mt-0">test</p>
                <p class="pictoParagraphs-list-text mt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
        <li class="pictoParagraphs-list d-flex col-md-6 col-xl-4">
            <span class="pictoParagraphs-list-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#battery"/>
                </svg>
            </span>
            <div>
                <p class="mb-4 pictoParagraphs-list-title font-weight-bold mt-0">test</p>
                <p class="pictoParagraphs-list-text mt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
        <li class="pictoParagraphs-list d-flex col-md-6 col-xl-4">
            <span class="pictoParagraphs-list-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#battery"/>
                </svg>
            </span>
            <div>
                <p class="mb-4 pictoParagraphs-list-title font-weight-bold mt-0">test</p>
                <p class="pictoParagraphs-list-text mt-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
    </ul>
    <button class="btn btn-secondary d-flex col-12 justify-content-center col-md-3 mx-auto mb-24">Label</button>
    <p class="text-center">Show <a href="https://preprod.norauto.pt/storybook/www.google.fr" class="text-secondary">general conditions</a></p>
</div>
  `;