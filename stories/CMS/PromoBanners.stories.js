export default {
    title: 'CMS/PromoBanners.stories.js'
  }


  export const Banners = () => `
  <style>
  .promoBanners{
    flex-direction: column;
  }
  .promoBanners img{
    width: 100%;
  }

  @media (min-width: 1200px) {
    .promoBanners{
      flex-direction: row;
    }
  }

  </style>
<div class="promoBanners d-flex">
  <a href="#" class="col-12 col-xl-6"><img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/es/banner%202%20ruedas.JPG"></a>
  <a href="#" class="col-12 col-xl-6"><img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/es/blog_norauto-01.jpg""></a>
</div>
  `;