export default {
    title: 'CMS/ShowcaseArticlePreview'
  }


  export const ShowcaseArticle = () => `
  <style>
  .showcaseArticle{
    flex-direction: column;
  }

  .showcaseArticle-img{
    flex-direction: column;
    max-height: 12.5rem;
    overflow: hidden;
  }

  .showcaseArticle-img img{
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .showcaseArticle-content{
    justify-content: center;
    padding-top: 2.5rem;
  }

  .showcaseArticle-content-mention{
      text-align: center;
  }

  .showcaseArticle-content-title{
    text-align: center;
}

.showcaseArticle-content-title::after{
    margin: 8px auto 0
}

.showcaseArticle-content-text{
    font-size: 1rem;
    text-align: center;
}

.showcaseArticle-content-btn{
    text-decoration: underline;
}

@media (min-width: 576px) {
    .showcaseArticle-img{
        max-height: 27.5rem;
    }
}

@media (min-width: 768px) {
    .showcaseArticle-content-mention{
        text-align: left;
    }
  
    .showcaseArticle-content-title{
      text-align: left;
  }

    .showcaseArticle-content-title::after{
      margin: 8px 0 0;
    }

    .showcaseArticle-content-text{
        text-align: left;
    }

    .showcaseArticle-content-btn{
        display: contents;
    }
  }

  @media (min-width: 1200px) {
    .showcaseArticle{
        flex-direction: row;
    }

    .showcaseArticle-content{
        padding-left: 3rem
    }
  }

  </style>
<div class="showcaseArticle d-flex">
  <div class="showcaseArticle-img col-xl-6">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-BIGTEASER-Folheto.jpg">
  </div>
  <div class="showcaseArticle-content d-flex flex-column col-xl-6">
    <span class="btn-link text-uppercase font-weight-bold showcaseArticle-content-mention">mention</span>
    <h2 class="title-underline mt-0 showcaseArticle-content-title">Section title</h2>
    <p class="showcaseArticle-content-text mt-0 mb-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    <a class="btn btn-link showcaseArticle-content-btn my-0">Cta Label</a>
  </div>
</div>
  `;

  export const ShowcaseArticleCoverRight = () => `
  <style>
  .showcaseArticle{
    flex-direction: column;
  }

  .showcaseArticle-img{
    flex-direction: column;
    max-height: 12.5rem;
    overflow: hidden;
  }

  .showcaseArticle-img img{
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .showcaseArticle-content{
    justify-content: center;
    padding-top: 2.5rem;
  }

  .showcaseArticle-content-mention{
      text-align: center;
  }

  .showcaseArticle-content-title{
    text-align: center;
}

.showcaseArticle-content-title::after{
    margin: 8px auto 0
}

.showcaseArticle-content-text{
    font-size: 1rem;
    text-align: center;
}

.showcaseArticle-content-btn{
    text-decoration: underline;
}

@media (min-width: 576px) {
    .showcaseArticle-img{
        max-height: 27.5rem;
    }
}

@media (min-width: 768px) {
    .showcaseArticle-content-mention{
        text-align: left;
    }
  
    .showcaseArticle-content-title{
      text-align: left;
  }

    .showcaseArticle-content-title::after{
      margin: 8px 0 0;
    }

    .showcaseArticle-content-text{
        text-align: left;
    }

    .showcaseArticle-content-btn{
        display: contents;
    }
  }

  @media (min-width: 1200px) {
    .showcaseArticle{
        flex-direction: row;
    }

    .showcaseArticle-content{
        padding-right: 3rem
    }

    .showcaseArticle-img{
        order: 2;
    }
  }

  </style>
<div class="showcaseArticle d-flex">
  <div class="showcaseArticle-img col-xl-6">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-BIGTEASER-Folheto.jpg">
  </div>
  <div class="showcaseArticle-content d-flex flex-column col-xl-6">
    <span class="btn-link text-uppercase font-weight-bold showcaseArticle-content-mention">mention</span>
    <h2 class="title-underline mt-0 showcaseArticle-content-title">Section title</h2>
    <p class="showcaseArticle-content-text mt-0 mb-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    <a class="btn btn-link showcaseArticle-content-btn my-0">Cta Label</a>
  </div>
</div>
  `;