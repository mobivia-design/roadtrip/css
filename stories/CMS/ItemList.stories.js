export default {
    title: 'CMS/ItemList'
  }


  export const BulletList = () => `

<ul>
    <li class="mb-8">List elem 1</li>
    <li class="mb-8">List elem 1</li>
    <li class="mb-8">List elem 1</li>
    <li class="mb-8">List elem 1</li>
</ul>
  `;

  export const CheckList = () => `

  <style>
    .checklist-list {
        list-style: none;
        display: flex;
        align-items: center;
    }
    .checkList-list-icon{
        font-size: 1.85rem;
        width: 30px;
        height: 30px;
        color: var(--success-text);
        fill: currentColor;
    }
  </style>

  <ul class="checklist">
      <li class="mb-8 checklist-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="checkList-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 1</span>
      </li>
      <li class="mb-8 checklist-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="checkList-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 2</span>
      </li>
      <li class="mb-8 checklist-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="checkList-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 3</span>
      </li>
      <li class="mb-8 checklist-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="checkList-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 4</span>
      </li>
      <li class="mb-8 checklist-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="checkList-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 5</span>
      </li>
  </ul>
    `;