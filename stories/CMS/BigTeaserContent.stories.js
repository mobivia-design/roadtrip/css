export default {
    title: 'CMS/BigTeaserContent'
  }


  export const BigTeaser = () => `
  <style>
  .bigTeaserContent img{
        height: 100%;
    @media (min-width: 1200px) {
        .bigTeaserContent {
          padding-left: 4rem;
        }
    }
  }
  </style>
<div class="d-lg-flex bigTeaserContent">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-SOUSCAT-xiaomi.jpg">
    <div class="offset-lg-1">
        <p class="text-uppercase text-secondary">Mention here</p>
        <h2 class="title-underline">Title Big Teaser</h2>
        <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </p>
    </div>
</div>
  `;

  export const BigTeaserPictoContent = () => `
  <style>
  .bigTeaserContent img{
        height: 100%;
    @media (min-width: 1200px) {
        .bigTeaserContent {
          padding-left: 4rem;
        }
    }
  }
   .bigTeaserContent-list{
    padding: 0;
    }
    .bigTeaserContent li{
        list-style: none;
    }
    .bigTeaserContent-list-item{
        display:flex
    }
    .bigTeaserContent-list-item-icon{
        border-radius: 50%;
        background: var(--background);
        height: 4rem;
        width: 4rem;
        align-items: center;
        display:flex;
        justify-content: center;
    }
    .bigTeaserContent-list-item-icon svg{
        width:36px
    }
  </style>
<div class="d-lg-flex bigTeaserContent">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-SOUSCAT-xiaomi.jpg">
    <div class="offset-lg-1">
        <p class="text-uppercase text-secondary">Mention here</p>
        <h2 class="title-underline">Title Big Teaser</h2>
        <ul class="bigTeaserContent-list">
            <li class="text-left">
                <div class="bigTeaserContent-list-item">
                    <span class="bigTeaserContent-list-item-icon mr-16 align-items-start">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                        <use xlink:href="#certified-outline"/>
                    </svg>
                    </span>
                    <p class="bigTeaserContent-list-item-text mt-0">
                        <span class="font-weight-bold mb-4">Picto Content Text</span>
                        <span class="d-block">your text</span>
                    </p>
                </div>
            </li>
            <li class="text-left">
                <div class="bigTeaserContent-list-item">
                    <span class="bigTeaserContent-list-item-icon mr-16 align-items-start">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                        <use xlink:href="#certified-outline"/>
                    </svg>
                    </span>
                    <p class="bigTeaserContent-list-item-text mt-0">
                        <span class="font-weight-bold mb-4">Picto Content Text</span>
                        <span class="d-block">your text</span>
                    </p>
                </div>
            </li>
        </ul>
    </div>
</div>
  `;

  export const BigTeaserLegalNotice = () => `
  <style>
  .bigTeaserContent img{
        height: 100%;
    @media (min-width: 1200px) {
        .bigTeaserContent {
          padding-left: 4rem;
        }
    }
  }
  </style>
<div class="d-lg-flex bigTeaserContent">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-SOUSCAT-xiaomi.jpg">
    <div class="offset-lg-1">
        <p class="text-uppercase text-secondary">Mention here</p>
        <h2 class="title-underline">Title Big Teaser</h2>
        <p class="text-left">
            <span class="font-weight-bold d-block">Preços baixos todo o ano</span>
            A Norauto reembolsa-o da diferença se encontrar mais barato.* Garantindo os melhores preços nos pneumáticos, a Norauto compromete-se em proporcionar segurança acessível para todos. Descubra desde já os preços baixos em norauto.pt!
        </p>
    </div>
</div>
<p class="text-small">Legal notice will display here</p>
  `;