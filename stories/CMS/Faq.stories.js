export default {
    title: 'CMS/FAQ'
  }


  export const Faq = () => `
  <style>
  .faq-icon{
      color: var(--warning);
      fill: currentColor;
  }

  .accordion-content {
    border: 0;
    padding: 0 0.5rem;
}

.accordion[open] .accordion-content {
    margin: 0 1rem 1rem;
    padding: 0rem 0rem 0;
}
  </style>

<div class="faq">
    <h2 class="title-underline text-center">Section Title</h2>
    <details class="accordion mb-16">
    <summary class="accordion-trigger" tabindex="0" role="button">
        <div class="accordion-header">
        <h3 class="mt-0 mb-0">First question test</h3>
        <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow faq-icon" viewBox="0 0 64 64">
            <use xlink:href="#navigation-chevron"/>
        </svg>
        </div>
    </summary>
    <div class="accordion-content">
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    </div>
    </details>

    <details class="accordion">
    <summary class="accordion-trigger" tabindex="0" role="button">
        <div class="accordion-header">
        <h3 class="mt-0 mb-0">First question test</h3>
        <svg xmlns="http://www.w3.org/2000/svg" class="accordion-arrow faq-icon" viewBox="0 0 64 64">
            <use xlink:href="#navigation-chevron"/>
        </svg>
        </div>
    </summary>
    <div class="accordion-content">
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    </div>
    </details>
</div>
  `;