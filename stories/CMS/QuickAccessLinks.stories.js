export default {
    title: 'CMS/QuickAccessLinks'
  }


  export const QuickAccess = () => `

<style>

.quickAccessLink{
    padding: 0 16px;
    overflow-x: scroll;
    scroll-snap-type: x mandatory;
}

.quickAccessLink-list{
    list-style: none;
}

.quickAccessLink-list-link{
    display: inline-flex;
    flex-direction: column;
    max-width: 10.625rem;
    align-items: center;
}

.quickAccessLink-list-link-icon{
    width: 80px;
    height: 80px;
    background-color: var(--background);
    border-radius: 50%;
    flex:none;
}


.quickAccessLink-list-link-icon svg{
    width: 36px;
    height: 36px;
}

.quickAccessLink-list-label{
    font-size: 1rem
}

@media (min-width: 640px) {
    .quickAccessLink-list-label{
        font-size: 1rem
    }

    .quickAccessLink-list-link-icon{
        width: 96px;
        height: 96px;
    }

    .quickAccessLink-list-link-icon svg{
        width: 48px;
        height: 48px;
    }
    
}
@media (min-width: 992px) {
    .quickAccessLink-list-label{
        font-size: 3.75rem
    }

    .quickAccessLink-list-link-icon{
        width: 137px;
        height: 137px;
    }

    .quickAccessLink-list-link-icon svg{
        width: 60px;
        height: 60px;
    }
    
}
</style>

<h2 class="title-underline text-center mb-24">Section title</h2>

<ul class="quickAccessLink d-flex">
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    <li class="quickAccessLink-list d-flex col justify-content-center">
        <a href="#" class="text-center text-primary quickAccessLink-list-link">
            <span class="quickAccessLink-list-link-icon d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                    <use xlink:href="#discount-prct-outline-color"/>
                </svg>
            </span>
            <span class="uickAccessLink-list-label font-weight-bold mt-16">Promoções</span>
        </a>
    </li>
    
   
</ul>
   
  `;