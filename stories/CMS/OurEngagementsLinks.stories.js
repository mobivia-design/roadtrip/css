export default {
    title: 'CMS/OurEngagementsLinks'
  }


  export const OurEngagements = () => `
  <style>
  .OurEngagements{
      flex-flow: row wrap;
    }

    .OurEngagements-list{
        flex-direction: column;
    }
    .OurEngagements-list-icons{
        width: 48px;
        color: var(--warning);
        fill: currentColor
    }
</style>

<div class="OurEngagements d-flex">
    <div class="row no-gutters">
    <div class="d-flex align-items-center OurEngagements-list col-6 col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="OurEngagements-list-icons">
            <use xlink:href="#leaf"/>
        </svg>
        <span class="font-weight-bold text-center">Desenvolvimento sustentável</span>
    </div>
    <div class="d-flex align-items-center OurEngagements-list col-6 col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="OurEngagements-list-icons">
            <use xlink:href="#leaf"/>
        </svg>
        <span class="font-weight-bold text-center">Desenvolvimento sustentável</span>
    </div>
    <div class="d-flex align-items-center OurEngagements-list col-6 col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="OurEngagements-list-icons">
            <use xlink:href="#leaf"/>
        </svg>
        <span class="font-weight-bold text-center">Desenvolvimento sustentável</span>
    </div>
    <div class="d-flex align-items-center OurEngagements-list col-6 col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="OurEngagements-list-icons">
            <use xlink:href="#leaf"/>
        </svg>
        <span class="font-weight-bold text-center">Desenvolvimento sustentável</span>
    </div>
    <div class="d-flex align-items-center OurEngagements-list col-6 col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="OurEngagements-list-icons">
            <use xlink:href="#leaf"/>
        </svg>
        <span class="font-weight-bold text-center">Desenvolvimento sustentável</span>
    </div>
    <div class="d-flex align-items-center OurEngagements-list col-6 col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="OurEngagements-list-icons">
            <use xlink:href="#leaf"/>
        </svg>
        <span class="font-weight-bold text-center">Desenvolvimento sustentável</span>
    </div>
    </div>
</div>
  `;