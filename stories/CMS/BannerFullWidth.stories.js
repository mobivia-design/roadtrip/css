export default {
    title: 'CMS/BannerFullWidth'
  }


  export const BannerLeft = () => `
  <style>
.banner{
  position: relative;
  height: 223px;
  overflow: hidden;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url("https://s1.medias-norauto.pt/visuals/desktop/tns/bg-wipers.webp");
}

.banner.bannerFullWidth::before{
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  height: 100%;
  content: "";
  background-color: var(--primary-darken);
  opacity: 0.2;
}

.banner-text{
  position: absolute;
  top: 1rem;
  left: 1rem;
}

.banner-title{
  position: absolute;
  top: 0;
  right: 2rem;
  bottom: 0;
  left: 2rem;
}
  </style>
  <div class="banner bannerFullWidth">
    <h1 class="banner-title text-white d-flex align-items-center">Banner title left</h1>
  </div>
  `;

  export const BannerCenter = () => `
  <style>
.banner{
  position: relative;
  height: 223px;
  overflow: hidden;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url("https://s1.medias-norauto.pt/visuals/desktop/tns/bg-wipers.webp");
}

.banner.bannerFullWidth::before{
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  height: 100%;
  content: "";
  background-color: var(--primary-darken);
  opacity: 0.2;
}

.banner-text{
  position: absolute;
  top: 1rem;
  left: 1rem;
}

.banner-title{
  position: absolute;
  top: 0;
  right: 2rem;
  bottom: 0;
  left: 2rem;
}
  </style>
  <div class="banner bannerFullWidth">
    <h1 class="text-center banner-title text-white d-flex align-items-center justify-content-center">Banner title center</h1>
  </div>
  `;