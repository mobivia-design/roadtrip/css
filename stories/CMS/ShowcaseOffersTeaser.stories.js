export default {
    title: 'CMS/ShowcaseOffersTeaser'
  }


  export const Showcase = () => `
  <style>
  .showcase{
    flex-direction: column;
  }


@media (min-width: 576px) {
    .showcase{
        flex-direction: row;
    }
}

  </style>

  <h2 class="title-underline text-center">Section title</h2>

<div class="showcase d-flex">
  <div class="showcase-img col-xl-6">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-SOUSCAT-xiaomi.jpg">
  </div>
  <div class="showcase-img col-xl-6">
    <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/pt/2021-2022/october/PT-SOUSCAT-xiaomi.jpg">
  </div>
</div>
  `;