export default {
    title: 'CMS/MaintenanceCard'
  }


  export const MaintenanceCard = () => `
  <style>
  .maintenanceCard{
      border-radius: 0.25rem;
      max-width: 380px;
      padding: 2.25rem 1.5rem;
  }
  .maintenanceCard-title{
    font-size: 1.5rem;
}

.maintenanceCard-title-eco{
    color: #b2c42d;
}
  .maintenanceCard-icon--green{
      font-size: 1.85rem;
      width: 30px;
      height: 30px;
      color: var(--success-text);
      fill: currentColor;
  }

  .maintenanceCard-icon{
    width: 24px;
    height: 24px;
    fill: currentColor;
}

  .maintenanceCard-price{
    text-decoration: line-through
}

.maintenanceCard-discount-price{
    font-size: 1.5rem;
    color: var(--danger);
}

.maintenanceCardAttribute-drawer-body{
    background-color: var(--primary-lighten);
}
  </style>

  <script>
document.querySelector('.collapse .collapse-btn').addEventListener('click', () => {
  document.querySelector('.collapse .collapsed-content').classList.toggle('collapse-open');
  let btnContent = document.querySelector('.collapse .collapsed-content').classList.contains('collapse-open') ? 'Menos informações' : 'Mais informaçõe';
  document.querySelector('.collapse .collapse-btn').innerHTML = btnContent;
});

document.querySelector('.maintenanceCard-icon').addEventListener('click', () => {
    document.querySelector('.drawer').classList.toggle('drawer-open');
  });

  
  document.querySelector('.drawer-close').addEventListener('click', () => {
    document.querySelector('.drawer').classList.remove('drawer-open');
  });
  

  document.querySelector('.drawer').addEventListener('click', () => {
    document.querySelector('.drawer').classList.remove('drawer-open');
  });
</script>

<div class="maintenanceCard bg-white">
    <h3 class="maintenanceCard-title"><span class="maintenanceCard-title-eco mr-8">Color title</span>Revisão Oficial</h3>
    

    <div class="collapse">
        <p class="text-gray-second">Garantia do construtor preservada</p>
        <p>Avaliação completotor</p>
        <div class="collapsed-content">
            <div class="pt-4">
                <div class="d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="maintenanceCard-icon--green mr-8">
                        <use xlink:href="#check-wide"/>
                    </svg>
                    <span class="font-weight-bold">Até 70 pontos de controlo</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="maintenanceCard-icon ml-8">
                        <use xlink:href="#alert-info-outline"/>
                    </svg>
                </div>
                <p>Revisão completa até 70 pontos de controlo em função da quilometragem do veículo</p>
            </div>
            <div class="pt-4">
                <div class="d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="maintenanceCard-icon--green mr-8">
                        <use xlink:href="#check-wide"/>
                    </svg>
                    <span class="font-weight-bold">Até 70 pontos de controlo</span>
                </div>
                <p>Inclui mão de obra e filtro de óleo
                * Filtro de ar, filtro de habitáculo e filtro de combustível caso estejam previstas de acordo com o plano de manutenção da marca. Estas peças são substituídas em função da quilometragem e anos da viatura.</p>
            </div>
            <div class="pt-4">
                <div class="d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="maintenanceCard-icon--green mr-8">
                        <use xlink:href="#check-wide"/>
                    </svg>
                    <span class="font-weight-bold">Até 70 pontos de controlo</span>
                </div>
            </div>
            <div class="pt-4">
                <div class="d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="maintenanceCard-icon--green mr-8">
                        <use xlink:href="#check-wide"/>
                    </svg>
                    <span class="font-weight-bold">Até 70 pontos de controlo</span>
                </div>
                <ul>
                    <li>Revisão completa até 70 pontos de controlo em função da quilometragem do veículo</li>
                    <li>Revisão completa até 70 pontos de controlo em função da quilometragem do veículo</li>
                </ul>
            </div>
        </div>
        <div class="collapse-btn-wrapper">
            <button class="collapse-btn btn btn-link pl-0">Mais informações</button>
        </div>
    </div>
    <div class="d-flex justify-content-between align-items-end">
        <div class="d-flex flex-column">
            <span class="text-small mb-0">A partir de</span>
            <span class="maintenanceCard-price">80 €</span>
            <span class="maintenanceCard-discount-price font-weight-bold">85 €</span>
        </div>
        <button class="btn btn-outline-secondary mb-0">Selecione um veículo</button>
    </div>
    <div class="drawer drawer-right" tabindex="-1" role="dialog" aria-label="label of the drawer">
        <div class="drawer-dialog" style="width: 480px" role="document">
        <div class="drawer-content">
            <header class="drawer-header">
            <button type="button" class="drawer-close" aria-label="Close">
                <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
            </button>
            </header>
            <div class="drawer-body p-16 maintenanceCardAttribute-drawer-body">
            <p class="font-weight-bold">Até 70 pontos de controlo</p>
<p>- Diagnóstico anti contaminação (estado das emissões de escape)<br>
- Leitura dos códigos de avaria e centralinas elétrónicas<br>
- Controlo do estado do pára-brisas<br>
- Controlo e atesto dos níveis (liquido limpa-vidros; óleo direcção assistida; líquido de refrigeração; óleo de travões<br>
- Controlo de luzes (imluminação e sinalização)<br>
- Controlo de sinais (sonoros, luminosos, emergência)<br>
- Reset da luz de aviso de revisão<br>
- Reiniciação do indicador de óleo de motor<br>
- Controlo do estado dos equipamentos (escovas, ópticas, luces dianteiras e traseiras, retrovisores)<br>
- Controlo do estado dos pneus FR e TR (pressão) e roda sobresselente<br>
- Controlo visual do alinhamento e rótulas de direcção<br>
- Controlo dos discos e pastilhas de travão FR e TR<br>
- Verificação do estados das placas de matrícula<br>
- Purga em água do filtro de combustivel caso não haja substituição por novo (veículos Diesel)<br>
- Estado visual (correias acessórias, refrigeração, peças de ignição, amortecedores FR e TR)<br>
- Estadodo sistema de transmissão e escape<br>
- Estado das rótulas de suspensão, terminais da barra estabilizadora, foles de suspensão<br>
- Estado dos rolamentos FR e TR, estanquecidade da caixa de velocidades<br>
- Estado silentblocks, tubos de travão FR e TR<br>
- Estado dos tambotes, cilindros de rodas, pastilhas e cabos de travão TR</p>
            </div>
        </div>
        </div>
    </div>
</div>
  `;