export default {
    title: 'CMS/ShowcaseBrands'
  }


  export const Showcase = () => `
  <style>

  .showcase{
      padding: 0 16px;
      overflow-x: scroll;
      scroll-snap-type: x mandatory;
  }
  
  .showcase-list{
      list-style: none;
      background-color: var(--white);
  }
  
  .showcase-list-link{
    width: 14rem;
  }
  
  @media (min-width: 640px) {
    .showcase-list-link{
        width: 14rem;
      }    
  }
  @media (min-width: 1280px) {
    .showcase{
        display: grid;
        grid-template-columns: repeat(6,1fr);
        grid-gap: 2.2857rem;
    }

    .showcase-list{
        flex: 0
      } 

    .showcase-list-link{
        width: 9rem;
      } 
      
  }
  </style>
  
  <h2 class="title-underline text-center mb-24">Section title</h2>
  
  <ul class="showcase d-flex">
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      <li class="showcase-list d-flex col justify-content-center px-16 py-16 border mx-8">
          <a href="#" class="text-center text-primary showcase-list-link d-flex align-items-center">
              <img alt="" src="https://s1.medias-norauto.pt/visuals/desktop/all/goodyear.png"/>
          </a>
      </li>
      
     
  </ul>
  `;