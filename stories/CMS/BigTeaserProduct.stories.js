export default {
    title: 'CMS/BigTeaserProduct'
  }


  export const BigTeaser = () => `
  <style>
  .bigTeaserProduct img{
      border-radius: 0.25rem
  }
  .bigTeaserProduct-title{
    font-size: 1.25rem;
}
  .bigTeaserProduct-price{
      font-size: 1.75rem;
      font-weight: bold;
      display: block;
  }
  </style>
<div class="d-lg-flex align-items-lg-center bigTeaserProduct">
    <img alt="" src="https://s1.medias-norauto.pt/images_produits/400x400/474528.jpg" class="border">
    <div class="offset-lg-1">
        <h3 class="bigTeaserProduct-title">Title Big Teaser</h3>
        <span class="text-left bigTeaserProduct-price mb-24">12,95€</span>
        <button class="btn btn-secondary btn-md d-flex col-12 justify-content-center col-xl-auto mx-auto">Medium button</button>
    </div>
</div>
  `;