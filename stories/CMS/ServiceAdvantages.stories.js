export default {
    title: 'CMS/ServiceAdvantages'
  }


  export const ServiceAdvantagesService = () => `
  <style>
    .serviceAdvantages{
        border-radius: 0.25rem;
        overflow: hidden;
    }
    .serviceAdvantages-title{
        font-size: 1.5rem;
    }
    .serviceAdvantages-price{
        font-size: 1.875rem;
        line-height: 2.25rem;
    }
    .serviceAdvantages-list {
        list-style: none;
        display: flex;
        align-items: center;
    }
    .serviceAdvantages-list-icon{
        font-size: 1.85rem;
        width: 24px;
        height: 24px;
        color: var(--success-text);
        fill: currentColor;
    }

    .serviceAdvantages-btn{
        flex-grow: 1;
        margin-left: 2.75rem;
    }

  </style>

<div class="serviceAdvantages bg-white border py-24 px-16">
    <h3 class="serviceAdvantages-title mb-0 mt-0">Revisão Oficial</h3>
    
    <p class="font-weight-bold mb-0 mt-0">+ Desinfecção do habitáculo</p>
    <p class="font-weight-bold mb-0 mt-0">+ Desinfecção do habitáculo</p>

    <ul class="pl-0">
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 1</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 2</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 3</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 4</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 5</span>
        </li>
    </ul>


    
    <div class="d-flex justify-content-between align-items-end">
        <span class="serviceAdvantages-price font-weight-bold d-flex">80 €</span>
        <button class="btn btn-secondary mb-0 serviceAdvantages-btn">Button label</button>
    </div>
</div>
  `;

  export const ServiceAdvantagesWidthImg = () => `
  <style>
    .serviceAdvantages{
        border-radius: 0.25rem;
        overflow: hidden;
    }
    .serviceAdvantages-title{
        font-size: 1.5rem;
    }
    .serviceAdvantages-price{
        font-size: 1.875rem;
        line-height: 2.25rem;
    }
    .serviceAdvantages-list {
        list-style: none;
        display: flex;
        align-items: center;
    }
    .serviceAdvantages-list-icon{
        font-size: 1.85rem;
        width: 24px;
        height: 24px;
        color: var(--success-text);
        fill: currentColor;
    }

    .serviceAdvantages-btn{
        flex-grow: 1;
        margin-left: 2.75rem;
    }
    .serviceAdvantages-flap{
        font-size: 0.75rem;
        border-radius: 99px;
    }

    .serviceAdvantages-flap--promo{
        border-color: var(--danger-text);
    }

    .serviceAdvantages-flap--exclu{
        border-color: var(--accent-hover);
        color: var(--accent-hover);
    }
  </style>

<div class="serviceAdvantages bg-white border py-24 px-16">
    <div class="d-flex align-items-center">
        <img alt="" src="https://s1.medias-norauto.fr/visuals/desktop/all/Pioneer.jpg" width="40" height="40" class="mr-8">
        <span class="serviceAdvantages-flap serviceAdvantages-flap--promo border px-8 py-4 mr-8 text-danger">Exclu web</span>
        <span class="serviceAdvantages-flap serviceAdvantages-flap--exclu border px-8 py-4">Exclu web</span>
    </div>
    <h3 class="serviceAdvantages-title mb-0 mt-0">Revisão Oficial</h3>
    
    <p class="font-weight-bold mb-0 mt-0">Subtitle</p>

    <ul class="pl-0">
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 1</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 2</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 3</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 4</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 5</span>
        </li>
    </ul>
    <p class="text-gray-second">Mention</p>

    
    <div class="d-flex justify-content-between align-items-end">
        <div class="d-flex flex-column">
            <span class="text-small mb-0 text-gray-second">a partir de</span>
            <span class="serviceAdvantages-price font-weight-bold d-flex text-content mb-0">147 €</span>
        </div>
        <button class="btn btn-secondary mb-0 serviceAdvantages-btn">Button label</button>
    </div>
</div>
  `;

  export const ServiceAdvantagesWidthPicto = () => `
  <style>
    .serviceAdvantages{
        border-radius: 0.25rem;
        overflow: hidden;
    }

    .serviceAdvantages-icon{
        width: 64px;
        height: 64px;
        border-radius: 50%
    }

    .serviceAdvantages-icon svg{
        width: 37px;
    }

    .serviceAdvantages-title{
        font-size: 1.5rem;
    }
    .serviceAdvantages-price{
        text-decoration: line-through;
    }

    .serviceAdvantages-discount-price{
        color: var(--danger);
        font-size: 1.875rem;
        line-height: 2.25rem;
    }

    .serviceAdvantages-list {
        list-style: none;
        display: flex;
        align-items: center;
    }
    .serviceAdvantages-list-icon{
        font-size: 1.85rem;
        width: 24px;
        height: 24px;
        color: var(--success-text);
        fill: currentColor;
    }

    .serviceAdvantages-btn{
        flex-grow: 1;
        margin-left: 2.75rem;
    }
    .serviceAdvantages-flap{
        font-size: 0.75rem;
        border-radius: 99px;
    }

    .serviceAdvantages-flap--promo{
        border-color: var(--danger-text);
    }

    .serviceAdvantages-flap--exclu{
        border-color: var(--accent-hover);
        color: var(--accent-hover);
    }
  </style>

<div class="serviceAdvantages bg-white border py-24 px-16">
    <div class="d-flex align-items-center mb-16">
        <span class="mr-8 d-flex align-items-center justify-content-center serviceAdvantages-icon bg-light mr-16">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                <use xlink:href="#battery"></use>
            </svg>
        </span>
        <span class="serviceAdvantages-flap serviceAdvantages-flap--exclu border px-8 py-4">Exclu web</span>
    </div>
    <h3 class="serviceAdvantages-title mb-0 mt-0">Revisão Oficial</h3>
    
    <p class="font-weight-bold mb-0 mt-0">Subtitle</p>

    <ul class="pl-0">
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
            <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 1</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 2</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 3</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 4</span>
        </li>
        <li class="mb-8 serviceAdvantages-list">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="serviceAdvantages-list-icon mr-8">
        <use xlink:href="#check-wide"/>
        </svg>
        <span>Advantage 5</span>
        </li>
    </ul>
    <p class="text-gray-second">Mention</p>

    
    <div class="d-flex justify-content-between align-items-end">
        <div class="d-flex flex-column">
            <span class="text-small mb-0 text-gray-second">a partir de</span>
            <span class="serviceAdvantages-price d-flex text-content mb-0">80 €</span>
            <span class="serviceAdvantages-discount-price d-flex text-danger font-weight-bold">99 €</span>
        </div>
        <button class="btn btn-secondary mb-0 serviceAdvantages-btn">Button label</button>
    </div>
</div>
  `;