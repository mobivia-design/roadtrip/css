export default {
    title: 'CMS/Steps'
  }


  export const Steps = () => `

  <style>

  .step-list{
      flex-direction:column
  }

  @media (min-width: 768px) {
    .step-list{
        flex-direction:row;
        justify-content: space-around;
    }
  }
  .steps-list-item{
      list-style: none;
}

@media (min-width: 768px) {
    .steps-list-item{
        flex-direction: column;
        flex: 1 1 0%;
    }
  }

  .steps-list-item-number{
    color: var(--accent-hover);
    font-size: 1.5rem;
    width: 48px;
    border-radius: 50%;
    height: 48px;
    border-color: var(--accent-hover);
    flex:none;
    margin-right: 1rem;
  }

  @media (min-width: 768px) {
    .steps-list-item-number{
        margin-bottom: 1rem;
        width: 64px;
        height: 64px;
        font-size: 2rem
    }
  }

  @media (min-width: 1200px) {
    .steps-list-item-number{
        font-size: 2.7143rem;
        width: 96px;
        height: 96px;
    }
  }

  .steps-list-item-title{
    font-size: 1rem;
  }

  @media (min-width: 768px) {
    .steps-list-item-title{
        text-align: center;
    }
  }

  </style>

<h2 class="title-underline text-center mb-24">Section title</h2>

<ul class="py-16 pl-0 d-flex step-list">
  <li class="steps-list-item py-16 px-16 d-flex align-items-center">
    <span class="steps-list-item-number border font-weight-bold d-flex justify-content-center align-items-center mr-16">1</span>
    <span class="steps-list-item-title font-weight-bold">Controlo e diagnóstico por parte da NORAUTO</span>
  </li>
  <li class="steps-list-item py-16 px-16 d-flex align-items-center">
    <span class="steps-list-item-number border font-weight-bold d-flex justify-content-center align-items-center">2</span>
    <span class="steps-list-item-title font-weight-bold">Transferência de dados e o cliente fica ao cuidado da Carglass®</span>
  </li>
  <li class="steps-list-item py-16 px-16 d-flex align-items-center">
    <span class="steps-list-item-number border font-weight-bold d-flex justify-content-center align-items-center mr-16">3</span>
    <span class="steps-list-item-title font-weight-bold">Contacto com o cliente e envio GRATUITO do orçamento por parte da Carglass®</span>
  </li>
</ul>



  `;