export default {
    title: 'CMS/PictoContentCollection'
  }


  export const Exemple1 = () => `
  <style>

  .pictoContentWithoutMedia{
    align-items: center;
    flex-direction: column;
    display: flex
}

  .pictoContent{
      flex-direction: column;
}
.pictoContent-img{
    width:100%;
    max-width: 144px;
}

    .pictoContent-icon{
        width: 9rem;
        height: 9rem;
        background-color: var(--background);
        border-radius: 50%;
        flex:none;
    }

    .pictoContent-icon svg{
        width: 36px;
        height: 36px;
    }

    .pictoContent-title{
        text-align: center
    }

    .pictoContent-text{
        font-size: 1rem;
    }

    @media (min-width: 768px) {

        .pictoContentWithoutMedia{
            align-items: start;
            flex-direction: row;
            display: initial
        }

        .pictoContent{
            flex-direction: row;
        }  

        .pictoContent-icon{
            margin-right: 3rem;
        } 

        .pictoContent-img{
            margin-right: 3rem;
            flex: none;
        } 

        .pictoContent-title{
            text-align: left
        }
    
        .pictoContent-text{
            text-align: left
        }
    }

  </style>

  <h2 class="title-underline text-center mb-16">Picto content collection title</h2>

    <div class="pictoContent d-flex align-items-center pt-24 pb-24">

        <span class="pictoContent-icon d-flex align-items-center justify-content-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                <use xlink:href="#battery"/>
            </svg>
        </span>
        <div>
            <h3 class="mb-0 pictoContent-title">PictoContent with picto</h3>
            <p class="text-center pictoContent-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a mi in erat pharetra ultrices non a elit. Donec nunc erat, tincidunt in mauris ut, molestie viverra elit. Nam feugiat odio et urna auctor, vitae accumsan arcu pretium. In tellus leo, dictum eget bibendum vitae, congue malesuada felis. Pellentesque et dolor urna. Donec a sapien quis dui fringilla egestas id sed ante. Nullam lacinia metus nisl, vitae facilisis diam eleifend dapibus. In eu enim eu massa tincidunt facilisis id id tortor. Nam at volutpat sem. Maecenas nisi lacus, tristique vitae velit mattis, dictum ultrices est. Nam ornare tellus nec felis fringilla condimentum. Sed sed dui condimentum, pulvinar diam ut, elementum massa. Mauris malesuada non est ut feugiat.</p>
        </div>
    </div>

    <div class="pictoContent d-flex align-items-center pt-24 pb-24">
        <span class="pictoContent-img d-flex align-items-center justify-content-center">
            <img alt="" src="https://s1.medias-norauto.pt/visuals/mobile/all/Michelin.png"/>
        </span>
        <div>
            <h3 class="mb-0 pictoContent-title">PictoContent with image</h3>
            <p class="text-center pictoContent-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a mi in erat pharetra ultrices non a elit. Donec nunc erat, tincidunt in mauris ut, molestie viverra elit. Nam feugiat odio et urna auctor, vitae accumsan arcu pretium. In tellus leo, dictum eget bibendum vitae, congue malesuada felis. Pellentesque et dolor urna. Donec a sapien quis dui fringilla egestas id sed ante. Nullam lacinia metus nisl, vitae facilisis diam eleifend dapibus. In eu enim eu massa tincidunt facilisis id id tortor. Nam at volutpat sem. Maecenas nisi lacus, tristique vitae velit mattis, dictum ultrices est. Nam ornare tellus nec felis fringilla condimentum. Sed sed dui condimentum, pulvinar diam ut, elementum massa. Mauris malesuada non est ut feugiat.</p>
        </div>
    </div>


    <div class="pictoContentWithoutMedia pt-24 pb-24">
        <h3 class="mb-0 pictoContent-title">PictoContent without media</h3>
        <p class="text-center pictoContent-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a mi in erat pharetra ultrices non a elit. Donec nunc erat, tincidunt in mauris ut, molestie viverra elit. Nam feugiat odio et urna auctor, vitae accumsan arcu pretium. In tellus leo, dictum eget bibendum vitae, congue malesuada felis. Pellentesque et dolor urna. Donec a sapien quis dui fringilla egestas id sed ante. Nullam lacinia metus nisl, vitae facilisis diam eleifend dapibus. In eu enim eu massa tincidunt facilisis id id tortor. Nam at volutpat sem. Maecenas nisi lacus, tristique vitae velit mattis, dictum ultrices est. Nam ornare tellus nec felis fringilla condimentum. Sed sed dui condimentum, pulvinar diam ut, elementum massa. Mauris malesuada non est ut feugiat.</p>
    </div>


  `;


