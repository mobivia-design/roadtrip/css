export default {
    title: 'CMS/PdfCollection'
  }


  export const Pdf = () => `
  <style>
  .pdfCollection{
      height:75vh;
      width:100%;
  }
  @media (min-width: 1200px) {
    .pdfCollection{
        max-height: 70%;
    }  
}
  </style>

  <h2 class="title-underline text-center mb-16">Section Title</h2>
  <embed src="https://medias-norauto.fr/pdf/PT/2020-2021/setembro/Saldos_Norauto.pdf" type="application/pdf" class="pdfCollection mx-auto" />

  `;
