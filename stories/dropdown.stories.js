export default {
  title: 'Expand/Dropdown',
  parameters: {
    docs: {
      inlineStories: false,
      iframeHeight: '420px',
    },
  },
}

export const Playground = () => `
<details class="dropdown" open>
  <summary>
  <div class="d-flex">
    <div class="dropdown-button btn-xl btn-default bg-white pl-8 pr-8 mb-0 d-flex align-items-center justify-center">
      <svg class="icon-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-more"/>
      </svg>
    </div>
  </div>
  </summary>
  <div class="dropdown-menu">
    <button class="dropdown-item" value="fr-FR">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Français - FR
    </button>
    <button class="dropdown-item" value="fr-BE">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Français - BE
    </button>
    <button class="dropdown-item" value="nl-BE">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Néerlandais - BE
    </button>
    <button class="dropdown-item" value="it-IT">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Italien - IT
    </button>
    <button class="dropdown-item" value="es-ES">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Espagnol - ES
    </button>
    <button class="dropdown-item" value="pt-PT">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Portugais - PT
    </button>
  </div>
</details>
`;

export const Medium = () => `
<details class="dropdown" open>
  <summary>
  <div class="d-flex">
    <div class="dropdown-md-button btn-default bg-white mb-0 d-flex align-items-center justify-content-center">
      <svg class="icon-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-more"/>
      </svg>
    </div>
  </div>
  </summary>
  <div class="dropdown-menu">
    <button class="dropdown-item" value="fr-FR">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Français - FR
    </button>
    <button class="dropdown-item" value="fr-BE">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Français - BE
    </button>
    <button class="dropdown-item" value="nl-BE">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Néerlandais - BE
    </button>
    <button class="dropdown-item" value="it-IT">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Italien - IT
    </button>
    <button class="dropdown-item" value="es-ES">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Espagnol - ES
    </button>
    <button class="dropdown-item" value="pt-PT">
      <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#picture"/>
      </svg>
      Portugais - PT
    </button>
  </div>
</details>
`;

export const dropdownRight = () => `
<details class="dropdown" open style="width: 300px">
  <summary>
  <div class="d-flex dropdown-header-top-right">
    <div class="dropdown-button btn-xl btn-default bg-white pl-8 pr-8 mb-0 d-flex align-items-center justify-center">
      <svg class="icon-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-more"/>
      </svg>
    </div>
  </div>
  </summary>
  <div class="dropdown-menu">
    <button class="dropdown-item" value="fr-FR">Français - FR</button>
    <button class="dropdown-item" value="fr-BE">Français - BE</button>
    <button class="dropdown-item" value="nl-BE">Néerlandais - BE</button>
    <button class="dropdown-item" value="it-IT">Italien - IT</button>
    <button class="dropdown-item" value="es-ES">Espagnol - ES</button>
    <button class="dropdown-item" value="pt-PT">Portugais - PT</button>
  </div>
</details>
`;

export const dropdownWidthSeparator = () => `
<details class="dropdown" open style="width: 300px">
  <summary>
  <div class="d-flex">
    <div class="dropdown-button btn-xl btn-default bg-white pl-8 pr-8 mb-0 d-flex align-items-center justify-center">
      <svg class="icon-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-more"/>
      </svg>
    </div>
  </div>
  </summary>
  <div class="dropdown-menu">
    <button class="dropdown-item dropdown-item-border" value="fr-FR">Français - FR</button>
    <button class="dropdown-item dropdown-item-border" value="fr-BE">Français - BE</button>
    <button class="dropdown-item dropdown-item-border" value="nl-BE">Néerlandais - BE</button>
    <button class="dropdown-item dropdown-item-border" value="it-IT">Italien - IT</button>
    <button class="dropdown-item dropdown-item-border" value="es-ES">Espagnol - ES</button>
    <button class="dropdown-item" value="pt-PT">Portugais - PT</button>
  </div>
</details>
`;

export const Dropup = () => `
<style>
#root{
  padding: 17rem 0 0
}
</style>
<details class="dropdown" open style="width: 300px">
  <summary>
  <div class="d-flex">
    <div class="dropdown-button btn-xl btn-default bg-white pl-8 pr-8 mb-0 d-flex align-items-center justify-center">
      <svg class="icon-lg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
        <use xlink:href="#navigation-more"/>
      </svg>
    </div>
  </div>
  </summary>
  <div class="dropdown-menu dropup">
    <button class="dropdown-item" value="fr-FR">Français - FR</button>
    <button class="dropdown-item" value="fr-BE">Français - BE</button>
    <button class="dropdown-item" value="nl-BE">Néerlandais - BE</button>
    <button class="dropdown-item" value="it-IT">Italien - IT</button>
    <button class="dropdown-item" value="es-ES">Espagnol - ES</button>
    <button class="dropdown-item" value="pt-PT">Portugais - PT</button>
  </div>
</details>
`;