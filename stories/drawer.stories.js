export default {
  title: 'Modals/Drawer',
  parameters: {
    docs: {
      inlineStories: false,
      iframeHeight: '600px',
    },
  },
}

export const Default = () => `
<div class="drawer drawer-open drawer-left" tabindex="-1" role="dialog" aria-label="label of the drawer">
  <div class="drawer-dialog" style="width: 360px" role="document">
    <div class="drawer-content">
      <header class="drawer-header">
        <button type="button" class="drawer-close" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="drawer-body"></div>
    </div>
  </div>
</div>
`;

export const WithFooter = () => `
<div class="drawer drawer-open drawer-left" tabindex="-1" role="dialog" aria-label="label of the drawer">
  <div class="drawer-dialog" style="width: 360px" role="document">
    <div class="drawer-content">
      <header class="drawer-header">
        <button type="button" class="drawer-close" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="drawer-body"></div>
      <footer class="drawer-footer">
        <button class="btn btn-primary btn-block">Label</button>
        <button class="btn btn-outline-primary btn-block mb-0">Label</button>
      </footer>
    </div>
  </div>
</div>
`;

export const HeaderInverse = () => `
  <div class="drawer drawer-open drawer-right" tabindex="-1" role="dialog" aria-label="label of the drawer">
  <div class="drawer-dialog" style="width: 480px" role="document">
    <div class="drawer-content">
      <header class="drawer-header drawer-header-inverse">
        <h2 class="drawer-title">Drawer title</h2>
        <button type="button" class="drawer-close" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="drawer-body"></div>
    </div>
  </div>
</div>
`;

export const WithBackButton = () => `
  <div class="drawer drawer-open drawer-right" tabindex="-1" role="dialog" aria-label="label of the drawer">
  <div class="drawer-dialog" style="width: 480px" role="document">
    <div class="drawer-content">
      <header class="drawer-header drawer-header-inverse">
        <button type="button" class="drawer-action" aria-label="Back">
          <svg xmlns="http://www.w3.org/2000/svg" class="action-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-chevron"/></svg>
        </button>
        <h2 class="drawer-title">Drawer title</h2>
        <button type="button" class="drawer-close" aria-label="Close">
          <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
        </button>
      </header>
      <div class="drawer-body"></div>
    </div>
  </div>
</div>
`;
