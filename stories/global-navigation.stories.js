export default {
    title: 'Navigation/Global Navigation',
    parameters: {
      docs: {
        inlineStories: false,
        iframeHeight: '200px',
      },
      backgrounds: {
        default: 'grey',
      },
      layout: 'fullscreen',
    },
  }
  
  export const Default = () => `

  <div class="toolbar">
    <div class="toolbar-container">
        <div class="logo d-flex">
        <svg class="icon-lg rotate-180"><use href="#pass-maintain-logo-color"></svg>
        <span class="app-name">App Name</span>
        </div>
        <div class="d-flex justify-content-between">
        <button class="btn align-items-center border-0">
            <svg class="btn-icon"><use href="#alert-question-outline"></svg>
            <span class="d-none d-xl-block">Help<span>
        </button>
        <button class="btn d-none d-xl-flex align-items-center">
            <svg class="btn-icon"><use href="#speak-advice-outline"></svg>
            Feedback
        </button>
        </div>
    </div>
    </div>

  <ul class="nav-bottom">
    <li class="nav-bottom-item d-none d-xl-flex m-24">
      <details class="dropdown profil-dropdown">
      <summary>
      <div class="d-flex">
        <div class="dropdown-button pl-8 pr-8 mb-0 d-flex align-items-center justify-center">
          <span class="avatar avatar-md">
            A
          </span>
        </div>
      </div>
      </summary>
      <div class="dropdown-menu">
        <div class="dropdown-item">
          <div class="profil-item d-flex pb-16 pt-8 mb-8">
            <span class="avatar avatar-sm">
              A
            </span>
            <div class="d-flex flex-column ml-16">
              <span class="font-weight-bold">First and Last name</span>
              <span>Email</span>
            </div>
          </div>
        </div>
        <button class="dropdown-item" value="fr-FR">
          <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
            <use xlink:href="#edit-outline"/>
          </svg>
          Edit profile
        </button>
        <button class="dropdown-item" value="fr-BE">
          <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
            <use xlink:href="#location-navigation-outline"/>
          </svg>
          Log out
        </button>
      </div>
    </details>
    </li>
    <li class="nav-bottom-item active" tooltip="Home" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-home-outline"/></svg>
        Home
      </a>
    </li>
    <li class="nav-bottom-item" tooltip="Search" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#search"/></svg>
        Search
      </a>
    </li>
    <li class="nav-bottom-item" tooltip="Print" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#print-outline"/></svg>
        Print
      </a>
    </li>
    <li class="nav-bottom-item" tooltip="Notifications" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#alert-notification-outline"/></svg>
        Notifications
      </a>
    </li>
    <li class="nav-bottom-item menu d-xl-none">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-menu"/></svg>
        Menu
      </a>
    </li>
  </ul>
  
  <div class="drawer drawer-right" tabindex="-1" role="dialog" aria-label="label of the drawer">
    <div class="drawer-dialog" style="width: 480px" role="document">
      <div class="drawer-content">
        <header class="drawer-header">
          <button type="button" class="drawer-action" aria-label="Back">
            <svg xmlns="http://www.w3.org/2000/svg" class="action-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-chevron"/></svg>Back
          </button>
          <h2 class="drawer-title">Menu</h2>
          <button type="button" class="drawer-close" aria-label="Close">
            <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
          </button>
        </header>
        <div class="drawer-body"></div>
      </div>
    </div>
  </div>
  
  
  <script>
  document.querySelector('.nav-bottom .menu').addEventListener('click', () => {
    document.querySelector('.drawer').classList.toggle('drawer-open');
  });
  document.querySelector('.drawer').addEventListener('click', () => {
    document.querySelector('.drawer').classList.toggle('drawer-open');
  });
  </script>
  `;
  
  export const Page = () => `
    <div class="toolbar">
        <div class="toolbar-container">
            <button class="btn btn-back border-left-0">
            <svg class="icon-lg rotate-180"><use href="#navigation-chevron"></svg>
            </button>
            <div class="toolbar-title">Search</div>
            <div class="d-flex justify-content-between">
            <button class="btn align-items-center border-0">
                <svg class="btn-icon"><use href="#alert-question-outline"></svg>
                <span class="d-none d-xl-block">Help<span>
            </button>
            <button class="btn d-none d-xl-flex align-items-center">
                <svg class="btn-icon"><use href="#speak-advice-outline"></svg>
                Feedback
            </button>
            </div>
        </div>
    </div>

  <ul class="nav-bottom">
    <li class="nav-bottom-item d-none d-xl-flex m-24">
      <details class="dropdown profil-dropdown">
      <summary>
      <div class="d-flex">
        <div class="dropdown-button pl-8 pr-8 mb-0 d-flex align-items-center justify-center">
          <span class="avatar avatar-md">
            A
          </span>
        </div>
      </div>
      </summary>
      <div class="dropdown-menu">
        <div class="dropdown-item">
          <div class="profil-item d-flex pb-16 pt-8 mb-8">
            <span class="avatar avatar-sm">
              A
            </span>
            <div class="d-flex flex-column ml-16">
              <span class="font-weight-bold">First and Last name</span>
              <span>Email</span>
            </div>
          </div>
        </div>
        <button class="dropdown-item" value="fr-FR">
          <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
            <use xlink:href="#edit-outline"/>
          </svg>
          Edit profile
        </button>
        <button class="dropdown-item" value="fr-BE">
          <svg class="icon-md" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
            <use xlink:href="#location-navigation-outline"/>
          </svg>
          Log out
        </button>
      </div>
    </details>
    </li>
    <li class="nav-bottom-item" tooltip="Home" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-home-outline"/></svg>
        Home
      </a>
    </li>
    <li class="nav-bottom-item active" tooltip="Search" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#search"/></svg>
        Search
      </a>
    </li>
    <li class="nav-bottom-item" tooltip="Print" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#print-outline"/></svg>
        Print
      </a>
    </li>
    <li class="nav-bottom-item" tooltip="Notifications" tooltip-position="right">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#alert-notification-outline"/></svg>
        Notifications
      </a>
    </li>
    <li class="nav-bottom-item menu d-xl-none">
      <a class="nav-bottom-link">
        <svg xmlns="http://www.w3.org/2000/svg" class="nav-bottom-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-menu"/></svg>
        Menu
      </a>
    </li>
  </ul>
  
  <div class="drawer drawer-right" tabindex="-1" role="dialog" aria-label="label of the drawer">
    <div class="drawer-dialog" style="width: 480px" role="document">
      <div class="drawer-content">
        <header class="drawer-header">
          <button type="button" class="drawer-action" aria-label="Back">
            <svg xmlns="http://www.w3.org/2000/svg" class="action-icon" viewBox="0 0 64 64"><use xlink:href="#navigation-chevron"/></svg>Back
          </button>
          <h2 class="drawer-title">Menu</h2>
          <button type="button" class="drawer-close" aria-label="Close">
            <svg xmlns="http://www.w3.org/2000/svg" class="close-icon" viewBox="0 0 64 64" aria-hidden="true"><use xlink:href="#navigation-close"/></svg>
          </button>
        </header>
        <div class="drawer-body"></div>
      </div>
    </div>
  </div>
  
  
  <script>
  document.querySelector('.nav-bottom .menu').addEventListener('click', () => {
    document.querySelector('.drawer').classList.toggle('drawer-open');
  });
  document.querySelector('.drawer').addEventListener('click', () => {
    document.querySelector('.drawer').classList.toggle('drawer-open');
  });
  </script>
  
  `;
  