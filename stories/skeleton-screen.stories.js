export default {
  title: 'Indicators/Skeleton'
}

export const List = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-3 mb-16">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-sm-9">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>

  <div class="row mt-16">
    <div class="col-sm-3 mb-16">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-sm-9">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>

  <div class="row mt-16">
    <div class="col-sm-3 mb-16">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-sm-9">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>

  <div class="row mt-16">
    <div class="col-sm-3 mb-16">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-sm-9">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>
</div>

<div class="container-fluid mt-16">
  <div class="row">
    <div class="col-6">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-6">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>

  <div class="row mt-16">
    <div class="col-6">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-6">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>

  <div class="row mt-16">
    <div class="col-6">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-6">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>

  <div class="row mt-16">
    <div class="col-6">
      <div class="skeleton" style="height: 156px"></div>
    </div>
    <div class="col-6">
      <p class="h2 skeleton" style="width: 60%"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
      <p class="text-content skeleton"></p>
    </div>
  </div>
</div>
`;

export const Grid = () => `
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>

    <div class="col-sm-6 col-md-3 mb-24">
      <div class="skeleton" style="height: 156px"></div>
      <p class="h2 mt-16 skeleton"></p>
      <p class="text-content skeleton" style="width: 80%;"></p>
      <p class="text-content mb-24 skeleton" style="width: 60%;"></p>
    </div>
  </div>
</div>
`;
