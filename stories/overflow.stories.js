export default {
    title: 'Utilities/Overflow',
  };
  
  export const Exemples = () => `

  <div class="overflow-auto" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-warning-surface-inverse);"></div>

  <div class="overflow-hidden" 
    style="width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-90);"></div>
  
  `;