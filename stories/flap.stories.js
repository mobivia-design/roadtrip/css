export default {
  title: 'Indicators/Flap'
}

export const Playground = () => `
<div class="flap flap-promo">
  Promo
</div>
`;

export const Colors = () => `
<div class="flap flap-promo">
  Promo
</div>

<div class="flap flap-exclu">
  Web Only
</div>

<div class="flap flap-info">
  New
</div>

<div class="flap flap-ecology">
  Ecology
</div>

<div class="flap flap-blackfriday">
  Black Friday
</div>

`;

export const Sizes = () => `
<div class="flap flap-md flap-promo">
  Promo
</div>

<div class="flap flap-md flap-exclu">
  Web Only
</div>

<div class="flap flap-md flap-info">
  New
</div>

<div class="flap flap-md flap-ecology">
  Ecology
</div>

<div class="flap flap-md flap-blackfriday">
  Black Friday
</div>
`;

export const Filled = () => `
<div class="flap flap-md flap-promo-filled">
  Promo
</div>

<div class="flap flap-md flap-exclu-filled">
  Web Only
</div>

<div class="flap flap-md flap-info-filled">
  New
</div>

<div class="flap flap-md flap-ecology-filled">
  Ecology
</div>

<div class="flap flap-md flap-blackfriday-filled">
  Black Friday
</div>
`;