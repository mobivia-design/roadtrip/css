export default {
  title: 'Forms/Button'
}

export const Playground = () => `
<button class="btn btn-primary">
  <svg xmlns="http://www.w3.org/2000/svg" class="btn-icon" viewBox="0 0 64 64">
    <use xlink:href="#shopping-cart-add"/>
  </svg>
  Add to cart
</button>
`;

export const Default = () => `
<button class="btn btn-primary">Label</button>
`;

export const Destructive = () => `
<button class="btn btn-destructive">Label</button>
`;

export const Outline = () => `
<button class="btn btn-outline-primary">Label</button>
<button class="btn btn-outline-destructive">Label</button>
`;

export const OutlineDefault = () => `
<button class="btn btn-outline-default">Label</button>
`;

export const Ghost = () => `
<button class="btn btn-ghost">Label</button>
`;

export const IconOnly = () => `
<button class="btn btn-primary btn-icon-only">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
    <use xlink:href="#shopping-cart-add"/>
  </svg>
</button>`;

export const Disabled = () => `
<button class="btn btn-secondary" disabled>Label</button>

<br>

<button class="btn btn-outline-secondary" disabled>Label</button>

<br>

<button class="btn btn-outline-default" disabled>Label</button>

<br>

<button class="btn btn-ghost" disabled>Label</button>

`;

export const Sizes = () => `
<div>
  <button class="btn btn-outline-default btn-xl">Large button</button>
  <button class="btn btn-ghost btn-xl">Large button</button>
  <button class="btn btn-primary btn-xl">Large button</button>
</div>

<div>
  <button class="btn btn-outline-default btn-md">Medium button</button>
  <button class="btn btn-ghost btn-md">Medium button</button>
  <button class="btn btn-primary btn-md">Medium button</button>
</div>

<div>
  <button class="btn btn-outline-default btn-sm">Small button</button>
  <button class="btn btn-ghost btn-sm">Small button</button>
  <button class="btn btn-primary btn-sm">Small button</button>
</div>

<div>
  <button class="btn btn-primary btn-icon-only btn-xl">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
      <use xlink:href="#shopping-cart-add"/>
    </svg>
  </button>
  <button class="btn btn-primary btn-icon-only btn-md">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
      <use xlink:href="#shopping-cart-add"/>
    </svg>
  </button>
  <button class="btn btn-primary btn-icon-only btn-sm">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
      <use xlink:href="#shopping-cart-add"/>
    </svg>
  </button>
  </div>

<button class="btn btn-outline-default btn-block">Block button</button>
<button class="btn btn-ghost btn-block">Block button</button>
<button class="btn btn-primary btn-block">Block button</button>
`;
