export default {
  title: 'Navigation/Toolbar',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
};

export const Default = () => `
<div class="toolbar">
  <div class="toolbar-container">
    <div class="toolbar-title-container">
      <div class="toolbar-title">Title</div>
    </div>
  </div>
</div>
`;

export const withButtons = () => `
<div class="toolbar">
  <div class="toolbar-container">
    <button class="btn border-left-0 border-right">
      <svg class="icon-lg rotate-180"><use href="#navigation-chevron"></svg>
    </button>
    <div class="toolbar-title-container">
      <div class="toolbar-title">Title</div>
    </div>
    <button class="btn">
    <svg class="icon-lg"><use href="#more-horizontal"></svg>
    </button>
  </div>
</div>
`;

export const Home = () => `
<div class="toolbar">
  <div class="toolbar-container">
    <div class="logo d-flex">
      <svg class="icon-lg rotate-180"><use href="#pass-maintain-logo-color"></svg>
      <span class="app-name">App Name</span>
    </div>
    <div class="d-flex justify-content-between">
      <button class="btn align-items-center border-0">
        <svg class="btn-icon"><use href="#alert-question-outline"></svg>
        <span class="d-none d-xl-block">Help<span>
      </button>
      <button class="btn d-none d-xl-flex align-items-center">
        <svg class="btn-icon"><use href="#speak-advice-outline"></svg>
        Feedback
      </button>
    </div>
  </div>
</div>
`;

export const headerApp = () => `
<div class="toolbar">
  <div class="toolbar-container">
    <button class="btn btn-back border-left-0">
      <svg class="icon-lg rotate-180 "><use href="#navigation-chevron"></svg>
    </button>
    <div class="toolbar-title">Page Title</div>
    <div class="d-flex justify-content-between">
      <button class="btn align-items-center border-0">
        <svg class="btn-icon"><use href="#alert-question-outline"></svg>
        <span class="d-none d-xl-block">Help<span>
      </button>
      <button class="btn d-none d-xl-flex align-items-center">
        <svg class="btn-icon"><use href="#speak-advice-outline"></svg>
        Feedback
      </button>
    </div>
  </div>
</div>
`;