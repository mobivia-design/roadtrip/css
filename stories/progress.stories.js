export default {
  title: 'Indicators/Progress'
}

export const Default = () => 
`
<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" aria-label="label of the progress" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info">
    <span class="progress-element-label">Label 0</span>
    <span class="progress-element-step">0/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" aria-label="label of the progress" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info">
    <span class="progress-element-label">Label 1</span>
    <span class="progress-element-step">1/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 40%" aria-label="label of the progress" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info">
    <span class="progress-element-label">Label 2</span>
    <span class="progress-element-step">2/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 60%" aria-label="label of the progress" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info">
    <span class="progress-element-label">Label 3</span>
    <span class="progress-element-step">3/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info">
    <span class="progress-element-label">Label 4</span>
    <span class="progress-element-step">4/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 100%" aria-label="label of the progress" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info">
    <span class="progress-element-label">Label 5</span>
    <span class="progress-element-step">5/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" aria-label="label of the progress" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info px-16">
    <span class="progress-element-label">Label 0</span>
    <span class="progress-element-step">0/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" aria-label="label of the progress" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info px-16">
    <span class="progress-element-label">Label 1</span>
    <span class="progress-element-step">1/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 40%" aria-label="label of the progress" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info px-16">
    <span class="progress-element-label">Label 2</span>
    <span class="progress-element-step">2/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 60%" aria-label="label of the progress" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info px-16">
    <span class="progress-element-label">Label 3</span>
    <span class="progress-element-step">3/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info px-16">
    <span class="progress-element-label">Label 4</span>
    <span class="progress-element-step">4/5</span>
  </div>
</div>

<div class="progress-element">
  <div class="progress mt-24">
    <div class="progress-bar" role="progressbar" style="width: 100%" aria-label="label of the progress" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <div class="progress-element-info px-16">
    <span class="progress-element-label">Label 5</span>
    <span class="progress-element-step">5/5</span>
  </div>
</div>



<div class="progress mt-24">
  <div class="progress-bar" role="progressbar" aria-label="label of the progress" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar" role="progressbar" aria-label="label of the progress" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar" role="progressbar" style="width: 50%" aria-label="label of the progress" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar" role="progressbar" style="width: 75%" aria-label="label of the progress" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar" role="progressbar" style="width: 100%" aria-label="label of the progress" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>
`;

export const Feedback = () => `
<div class="progress mt-24">
  <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar bg-warning" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar bg-danger" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress mt-24">
  <div class="progress-bar bg-rating" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>




<div class="progress progress-light mt-24">
  <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress progress-light mt-24">
  <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress progress-light mt-24">
  <div class="progress-bar bg-warning" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress progress-light mt-24">
  <div class="progress-bar bg-danger" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress progress-light mt-24">
  <div class="progress-bar bg-rating" role="progressbar" style="width: 80%" aria-label="label of the progress" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
</div>
`;